require 'rails_helper'

RSpec.describe "metabolomic_categories/edit", type: :view do
  before(:each) do
    @metabolomic_category = assign(:metabolomic_category, MetabolomicCategory.create!())
  end

  it "renders the edit metabolomic_category form" do
    render

    assert_select "form[action=?][method=?]", metabolomic_category_path(@metabolomic_category), "post" do
    end
  end
end
