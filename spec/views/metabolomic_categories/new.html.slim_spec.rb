require 'rails_helper'

RSpec.describe "metabolomic_categories/new", type: :view do
  before(:each) do
    assign(:metabolomic_category, MetabolomicCategory.new())
  end

  it "renders new metabolomic_category form" do
    render

    assert_select "form[action=?][method=?]", metabolomic_categories_path, "post" do
    end
  end
end
