require 'spec_helper'

describe Synonym do
  it { should belong_to(:metabolite) }

  it { should validate_presence_of(:synonym) }
  it { should validate_presence_of(:metabolite) }
end