require 'spec_helper'

describe PubmedReference do
  # Relationships
  it { should have_many(:protein_references).dependent(:destroy) }
  it { should have_many(:metabolite_references).dependent(:destroy) }
  it { should have_many(:concentration_references).dependent(:destroy) }
  it { should have_many(:metabolite_protein_references).dependent(:destroy) }

  # Validations
  it { should validate_presence_of(:pubmed_id) }
  it { should validate_numericality_of(:pubmed_id).only_integer }

  it 'should validate pubmed ID is unique for pubmed references' do
    FactoryGirl.create(:pubmed_reference)
    should validate_uniqueness_of(:pubmed_id) 
  end
end
