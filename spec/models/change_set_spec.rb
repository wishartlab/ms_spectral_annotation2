require 'spec_helper'

describe ChangeSet do
  it { should validate_presence_of(:source) }  
  it { should validate_presence_of(:description) }  
  it { should validate_presence_of(:user) }  
  
  it { should ensure_length_of(:source).is_at_most(255) }
end
