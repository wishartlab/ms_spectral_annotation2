require 'spec_helper'

describe MetaboliteDescription do
  it { should belong_to(:metabolite) }

  it { should validate_presence_of(:metabolite_id) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:source) }
end
