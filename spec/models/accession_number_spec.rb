require 'spec_helper'

describe AccessionNumber do
  it { should belong_to(:element) }
  
  it { should validate_presence_of(:number) }

  it 'should validate number is unique' do
    FactoryGirl.create(:metabolite_accession_number)
    should validate_uniqueness_of(:number).scoped_to(:element_type)
  end

end
