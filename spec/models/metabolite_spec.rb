require 'spec_helper'

describe Metabolite do
  # Relationships
  it { should have_one(:link_set).dependent(:destroy) }
  it { should have_one(:taxonomy).dependent(:destroy) }
  it { should have_one(:ontology).dependent(:destroy) }

  it { should have_many(:synonyms).dependent(:destroy) }
  it { should have_many(:accession_numbers).dependent(:destroy) }
  it { should have_many(:tissues).dependent(:destroy) }
  it { should have_many(:metabolite_protein_links).dependent(:destroy) }
  it { should have_many(:proteins).through(:metabolite_protein_links) }
  it { should have_many(:concentrations).dependent(:destroy) }
  it { should have_many(:concentration_disease_links).through(:concentrations) }
  it { should have_many(:diseases).through(:concentration_disease_links) }
  it { should have_many(:pathway_links) }
  it { should have_many(:pathways).through(:pathway_links) }
  it { should have_many(:metabolite_references).dependent(:destroy) }
  it { should have_many(:references).through(:metabolite_references) }
  it { should have_many(:metabolite_descriptions).dependent(:destroy)}
  it { should have_and_belong_to_many(:change_sets) }

  # Validations
  it { should validate_presence_of(:name) }
  it { should ensure_length_of(:name).is_at_most(255) }
  it { should ensure_length_of(:melting_point).is_at_most(255) }
  it { should ensure_length_of(:experimental_water_solubility).is_at_most(255) }
  it { should ensure_length_of(:experimental_logp).is_at_most(255) }
  it { should ensure_length_of(:cell_loc).is_at_most(255) }
  it { should ensure_length_of(:tissue_loc).is_at_most(255) }
  it { should ensure_inclusion_of(:state).in_array([ 'Solid', 'Liquid', 'Gas' ]) }

  it 'should validate name is unique' do
    FactoryGirl.create(:metabolite)
    should validate_uniqueness_of(:name) 
  end

  it 'should validate HMDB id is the correct format' do
    should_not allow_value(1).for(:hmdb_id)
    should_not allow_value("BMDB00001").for(:hmdb_id)
    should_not allow_value("HMDBA0001").for(:hmdb_id)
    should allow_value('HMDB00001').for(:hmdb_id)
  end

  it 'should validate the CAS number is the correct format' do
    should_not allow_value('103-90-A').for(:cas)
    should_not allow_value('103-90-3').for(:cas)
    should_not allow_value('103.90.2').for(:cas)
    should allow_value('103-90-2').for(:cas)
  end

  describe 'revoking a metabolite' do
    it 'should create a revocation and delete the metabolite' do
      comment = "Revoked because it sucks"
      metabolite = FactoryGirl.create(:metabolite)
      Revocation.where(:hmdb_id => metabolite.hmdb_id).first.should be_nil
      metabolite.revoke!(comment).should be_true
      metabolite.destroyed?.should be_true
      revocation = Revocation.where(:hmdb_id => metabolite.hmdb_id).first
      revocation.should be_present
      revocation.comment.should == comment
      revocation.name.should == metabolite.name
    end

    it 'should store accession numbers' do
      metabolite = FactoryGirl.create(:metabolite)
      metabolite.accession_numbers << AccessionNumber.create(:number => 'HMDB99998')
      metabolite.accession_numbers << AccessionNumber.create(:number => 'HMDB99999')
      metabolite.revoke!('Lame').should be_true
      revocation = Revocation.where(:hmdb_id => metabolite.hmdb_id).first
      revocation.accession_numbers.should == 'HMDB99998; HMDB99999'
    end

    it 'should store inchikey' do
      metabolite = FactoryGirl.create(:metabolite_with_structure)
      metabolite.revoke!('Lame').should be_true
      revocation = Revocation.where(:hmdb_id => metabolite.hmdb_id).first
      revocation.inchikey.should == metabolite.inchikey
    end
  end
end
