require 'spec_helper'

describe Protein do
  # Relationships
  it { should have_one(:polypeptide_sequence).dependent(:destroy) }
  it { should have_one(:gene_sequence).dependent(:destroy) }

  it { should have_many(:accession_numbers).dependent(:destroy) }
  it { should have_many(:metabolite_protein_links).dependent(:destroy) }
  it { should have_many(:metabolites).through(:metabolite_protein_links) }
  it { should have_many(:synonyms).dependent(:destroy).class_name("ProteinSynonym") }
  it { should have_many(:metabolite_references).through(:metabolite_protein_links) }
  it { should have_many(:enzyme_classes).dependent(:destroy) }
  it { should have_many(:protein_references).dependent(:destroy) }
  it { should have_many(:references).through(:protein_references) }
  it { should have_many(:pathway_links).dependent(:destroy) }
  it { should have_many(:pathways).through(:pathway_links) }
  it { should have_many(:pfam_memberships).dependent(:destroy) }
  it { should have_many(:pfams).through(:pfam_memberships) }
  it { should have_many(:protein_subcellular_locations).dependent(:destroy) }
  it { should have_many(:subcellular_locations).through(:protein_subcellular_locations) }
  it { should have_many(:protein_go_classes).dependent(:destroy) }
  it { should have_many(:go_classes).through(:protein_go_classes) }
  it { should have_many(:sequences).dependent(:destroy) }

  # Validations
  it { should validate_presence_of(:name) }
  it { should ensure_length_of(:name).is_at_most(255) }

  # it 'should validate name is unique' do
  #   FactoryGirl.create(:metabolite)
  #   should validate_uniqueness_of(:name) 
  # end

  # it 'should validate HMDB id is the correct format' do
  #   should_not allow_value(1).for(:hmdb_id)
  #   should_not allow_value("BMDB00001").for(:hmdb_id)
  #   should_not allow_value("HMDBA0001").for(:hmdb_id)
  #   should allow_value('HMDB00001').for(:hmdb_id)
  # end  
end
