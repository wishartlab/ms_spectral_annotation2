require 'spec_helper'

describe LinkSet do
  it { should belong_to(:metabolite) }

  it { should ensure_length_of(:kegg_id).is_equal_to(6) }
  it { should ensure_length_of(:drugbank_id).is_equal_to(7) }
  it { should ensure_length_of(:foodb_id).is_equal_to(9) }
  it { should ensure_length_of(:knapsack_id).is_equal_to(9) }
  it { should ensure_length_of(:wiki_id).is_at_most(255) }

  it { should validate_numericality_of(:chebi_id).only_integer }  
  it { should validate_numericality_of(:chemspider_id).only_integer }  
  it { should validate_numericality_of(:phenol_metabolite_id).only_integer }  
  it { should validate_numericality_of(:phenol_compound_id).only_integer }  
  it { should validate_numericality_of(:metlin_id).only_integer }

  it { should validate_uniqueness_of(:chebi_id) }  
  it { should validate_uniqueness_of(:chemspider_id) }  
  it { should validate_uniqueness_of(:phenol_metabolite_id) }  
  it { should validate_uniqueness_of(:phenol_compound_id) }  
  it { should validate_uniqueness_of(:metlin_id) }  

  it { should allow_value('').for(:chebi_id) }
  it { should allow_value('').for(:chemspider_id) }
  it { should allow_value('').for(:phenol_metabolite_id) }
  it { should allow_value('').for(:phenol_compound_id) }
  it { should allow_value('').for(:metlin_id) }
  it { should allow_value('').for(:kegg_id) }
  it { should allow_value('').for(:drugbank_id) }
  it { should allow_value('').for(:foodb_id) }
  it { should allow_value('').for(:knapsack_id) }
  it { should allow_value('').for(:wiki_id) }

  it { should allow_value("C12345").for(:kegg_id) }
  it { should_not allow_value("D12345").for(:kegg_id) }

  it { should allow_value("FDB123456").for(:foodb_id) }
  it { should_not allow_value("FDC123456").for(:foodb_id) }
 
  it { should allow_value("DB12345").for(:drugbank_id) }
  it { should_not allow_value("DZ12345").for(:drugbank_id) }

  it { should allow_value("C12345678").for(:knapsack_id) }
  it { should_not allow_value("A12345678").for(:knapsack_id) }
end