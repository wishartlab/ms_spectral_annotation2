require "rails_helper"

RSpec.describe MetabolomicCategoriesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/metabolomic_categories").to route_to("metabolomic_categories#index")
    end

    it "routes to #new" do
      expect(:get => "/metabolomic_categories/new").to route_to("metabolomic_categories#new")
    end

    it "routes to #show" do
      expect(:get => "/metabolomic_categories/1").to route_to("metabolomic_categories#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/metabolomic_categories/1/edit").to route_to("metabolomic_categories#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/metabolomic_categories").to route_to("metabolomic_categories#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/metabolomic_categories/1").to route_to("metabolomic_categories#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/metabolomic_categories/1").to route_to("metabolomic_categories#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/metabolomic_categories/1").to route_to("metabolomic_categories#destroy", :id => "1")
    end

  end
end
