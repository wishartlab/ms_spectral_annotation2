FactoryGirl.define do
  factory :link_set

  factory :metabolite do |metabolite|
    sequence(:hmdb_id) { |n| 'HMDB' << n.to_s.rjust(5, '0') }
    name "Funyl Crazydite"
    link_set

    factory :metabolite_with_structure do |metabolite|
       metabolite.after(:build) do |m|  
        m.stubs(:inchikey).returns('BRMWTNUJHUMWMS-LURJTMIESA-N')
      end
    end

  end

end