FactoryGirl.define do
  factory :metabolite_accession_number, class: AccessionNumber do
    number 'HMDB00100'
    association :element, :factory => :metabolite
  end

  # factory :protein_accession_number, class: AccessionNumber do
  #   number '42'
  #   association :element, :factory => :protein
  # end
end