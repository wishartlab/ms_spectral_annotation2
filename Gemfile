source 'http://rubygems.org'
ruby '2.3.0'

gem 'rails', '~> 4.2.0'
gem 'mysql2'
gem 'sass-rails', '~> 5.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'uglifier', '>= 1.3.0'
gem 'jquery-rails'
gem 'turbolinks'
gem 'jquery-turbolinks'
gem 'jbuilder', '~> 2.0'
gem 'bootstrap-sass'
gem 'bootstrap-kaminari-views'
gem 'slim-rails'
gem 'progress_bar'
gem 'kaminari'
gem 'safe_attributes'
gem 'bio'
gem 'rubyzip'
gem 'whenever', require: false
gem 'paperclip'
gem 'active_sanity'
gem 'parallel'
gem 'google-analytics-rails'
gem 'activeadmin', '~> 1.0.0.pre5'
gem 'active_admin_theme'
gem 'devise'
gem 'oink'
gem 'daemons'
gem 'redis-rails'
gem 'sidekiq'
gem 'inherited_resources'
gem 'select2-rails', '~> 3.5.9'
gem 'deep_cloneable'
# Locking to 2.x for now - don't have time to fix for version 3.
# http://stackoverflow.com/questions/34344094/after-gem-update-test-fail-with-asset-was-not-declared-to-be-precompiled-in-pr
gem 'sprockets-rails', '~> 2.3.3'
gem 'elasticsearch', '~> 2.0.0'
gem 'active_record_union'
gem 'rename'

# Wishartlab Gems
gem 'admin_mailer', git: 'git@bitbucket.org:wishartlab/admin-mailer'
# gem 'moldbi', path: '/Users/knox/wishart/gems/moldbi'
gem 'moldbi', git: 'git@bitbucket.org:wishartlab/moldbi'
# gem 'moldbi', path: '/Users/AnaMarcu/Documents/moldbi'
#gem 'moldbi', path: '~/BioinformaticsApplications/moldbi' 
# gem 'wishart', path: '/Users/knox/wishart/gems/wishart'
gem 'wishart', git: 'git@bitbucket.org:wishartlab/wishart'
# gem 'specdb', path: '/Users/jason/workspace/wishartlab/specdbi'
# gem 'specdb', path: '/Users/allisonpon/Development/specdbi'
# gem 'specdb', path: '/Users/tanvirsajed/Documents/wishartlab-gems/specdbi'
# gem 'specdb', path: '/Users/darndt/work/SpecDB/specdbi'
gem 'specdb', git: 'git@bitbucket.org:wishartlab/specdbi'
# gem 'specdb', path: '/Users/AnaMarcu/Documents/specdbi'
#gem 'specdb', path: '~/BioinformaticsApplications/specdbi'
# gem 'unearth', path: '/Users/knox/wishart/gems/unearth'
gem 'unearth', git: 'git@bitbucket.org:wishartlab/unearth', branch: 'v5'
gem 'seq_search', git: 'git@bitbucket.org:wishartlab/seqsearch'
# gem 'cite_this', path: '/Users/knox/wishart/gems/cite_this'
gem 'cite_this', git: 'git@bitbucket.org:wishartlab/cite_this'
# gem "tmic_banner", path: "../tmic_banner"
gem 'tmic_banner', git: 'git@bitbucket.org:wishartlab/tmic_banner'

gem 'nokogiri', '~> 1.6.1'
gem 'metbuilder', git: "git@bitbucket.org:wishartlab/metbuilder.git"
# gem 'chemoSummarizer', git: 'git@bitbucket.org:wishartlab/chemosummarizer.git', :branch => 'master'
# gem 'data-wrangler', git: "git@bitbucket.org:wishartlab/datawrangler.git"
gem 'synonym_cleaner',
  git: 'git@bitbucket.org:wishartlab/synonym-cleaner.git',
  require: false

gem 'crack'
gem 'trollop'
gem 'sqlite3'

group :development do
  gem 'awesome_print'
  gem "capistrano"
  gem 'capistrano-rails'
  gem 'capistrano-bundler'
  gem 'capistrano-chruby'
  gem 'capistrano-sidekiq'
  gem 'quiet_assets'
  gem 'thin'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'syncfast', git: 'git@bitbucket.org:wishartlab/syncfast', require: false
  gem 'spring'
end

group :development, :test do
  gem 'rspec-rails'
  gem 'spork-rails'
end

group :test do
  gem 'shoulda-matchers'
  gem "guard-rspec"
  gem "factory_girl_rails", "~> 4.0"
  gem 'turn', require: false # Pretty printed test output
  gem 'minitest'
  gem "mocha"
  gem 'ruby-prof'
end

group :production do
  gem 'newrelic_rpm'
  gem 'execjs'
  gem 'therubyracer', require: 'v8'
  gem 'puma'
  gem 'puma_worker_killer'
end