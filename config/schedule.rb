# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

set :output, { error: '/apps/hmdb/project/shared/log/whenever-error.log',
               standard: '/apps/hmdb/project/shared/log/whenever.log' }

# Update the indexes daily (only covers updates/new records)
every :day, at: '3pm' do
  rake "unearth:index:update"
end

# Rebuild the entire index weekly (this handles all changes). Note this will
# not bring down searching on the site while it's indexing.
every 7.days, at: '8pm' do
  rake "unearth:index:rebuild"
end

# Cleanup unearth extractor sessions and files
every 1.days, at: '12am' do
  rake 'unearth:extractor:cleanup'
end

# Rebuild the BLAST database every 4 days
every 4.days, at: '12am', roles: [:app] do
  rake 'seq_search:build_blast_db'
end

every :saturday, at: '11pm', roles: [:app] do
  rake 'hmdb:export:all NEWRELIC_ENABLE=false'
end

# Cron job for synchronizing the tms derivatives here
every :sunday, at: '12am', roles: [:app] do
	rake 'moldb:sync_tms_derivative'
end