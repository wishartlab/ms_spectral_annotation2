set :application, 'hmdb'

set :deploy_via, :copy
set :repo_url, "git@bitbucket.org:wishartlab/hmdb.git"
set :local_repository, "."


set :deploy_to, '/apps/hmdb/project'
set :use_sudo, false
set :linked_files, ['config/database.yml', 'config/moldb.yml', 
                    'config/seq_search.yml', 'public/robots.txt',
                    'config/unearth.yml']
set :linked_dirs, %w{public/downloads index log public/system public/extractions}
set :keep_releases, 5
set :branch, ENV["REVISION"] || ENV["BRANCH_NAME"] || "master"

namespace :deploy do
  desc 'Start application'
  task :start do
    on roles(:web) do
      within release_path do
        execute "script/puma.sh", "start"
      end
    end
  end

  desc 'Stop application'
  task :stop do
    on roles(:web) do
      within release_path do
        execute "script/puma.sh", "stop"
      end
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:web) do
      within release_path do
        execute "script/puma.sh", "restart"
      end
    end
  end

  after :publishing, :restart, :cleanup
end

# Flush all redis caches. Not necessary unless large update to database
namespace :redis do
  desc "Flushes all Redis data"
  task :flushall do
    on roles(:web) do
      execute "redis-cli", "flushall"
    end
  end
end

namespace :redis_2 do
  namespace :flush do
    desc "Flushes all redis cached moldbi views"
    task :moldbi do
      on roles(:db) do
        execute "redis-cli KEYS \"moldbi/*\" | xargs redis-cli DEL"
      end
    end

    desc "Flushes all redis cached moldbi resources"
    task :moldbi_resources do
      on roles(:db) do
        execute "redis-cli KEYS \"moldbi/*/resource\" | xargs redis-cli DEL"
      end
    end
  end

end