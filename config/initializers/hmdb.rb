Rails.application.configure do
  config.version = 1.0
  config.contactor = 'MSSA'
  config.site_name = 'MS Spectral Annotation'
  config.site_abbreviation = 'MSSA'
  config.url = "http://www.msspectralannotation.ca"
end
