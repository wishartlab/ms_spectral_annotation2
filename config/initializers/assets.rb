# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '2.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w( demo.css )

# Precompile TMIC banner assets
Rails.application.config.assets.precompile += %w( tmic-banner-*-back.jpg )
Rails.application.config.assets.precompile += %w( tmic-banner-wide-logo.png )
Rails.application.config.assets.precompile += %w( MetaSciBanners-01.png )
Rails.application.config.assets.precompile += %w( MetaSciBanners-02.png )
Rails.application.config.assets.precompile += %w( MetaSciBanners-03.png )
