require "biofluid_routes_helper"

Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  devise_for :users, ActiveAdmin::Devise.config

  mount Wishart::Engine => "/w" , as: 'wishart'
  mount Moldbi::Engine => "/structures"
  mount Specdb::Engine => "/spectra"
  mount SeqSearch::Engine => "/seq_search"
  mount Unearth::Engine => "/unearth", as: "unearth"
  mount CiteThis::Engine => "/cite_this", as: "cite_this"

  # Human Metabolome Library (HML)
  namespace :hml do
    root to: 'simple#home'
    resources :metabolites, only: :index
  end

  # Named routes
  get 'about' => 'simple#about', as: :about
  get 'release-notes' => 'simple#release_notes', as: :release_notes
  get 'citing' => 'simple#citing', as: :citing
  get 'news' => 'simple#news', as: :news
  get 'statistics' => 'simple#statistics', as: :statistics
  get 'sources' => 'simple#sources', as: :sources
  get 'downloads' => 'simple#downloads', as: :downloads
  get 'help' => 'simple#help', as: :help
  get 'textquery' => 'simple#textquery', as: :textquery

  controller :demo do
    get "/demo/ad1"
  end

  # Extractor routes
  extractable_collections :metabolites
  extractable_collections :metabolite_protein_links

  # Resources
  resources :metabolites, only: [ :index, :show ] do
    resources :concentrations, only: :index
    resources :predicted_concentrations, only: :index
    resources :metabolite_protein_links, only: :index
    member do
      get 'structure'
    end
  end
  resources :proteins, only: [:show, :index] do
    resources :metabolite_protein_links, only: :index
    resource :polypeptide_sequence, only: :show
    resource :gene_sequence, only: :show
  end
  resources :concentrations, only: [:show, :index] do
    collection do
      post 'stats'
    end
  end

  resources :metabolomic_categories
  resources :bmi_metabolomics, controller: 'metabolomic_categories', type: 'BmiLink' 
  resources :diurnal_metabolomics, controller: 'metabolomic_categories', type: 'DiurnalLink'
  resources :age_metabolomics, controller: 'metabolomic_categories', type: 'AgeLink' 
  resources :gender_metabolomics, controller: 'metabolomic_categories', type: 'GenderLink'
  resources :geno_metabolomics, controller: 'metabolomic_categories', type: 'GenoLink'
  resources :pharmaco_metabolomics, controller: 'metabolomic_categories', type: 'PharmacoLink'

  resources :diseases, only: [:index]
  resources :pathways, only: [:index]
  resources :reactions, only: [:index, :show]
  resources :biofluids, only: [:index]
  post "biofluids" => "biofluids#index"
  get 'classyfication',
    to: 'classyfication#index',
    as: :classyfication

  # Set up the routes for the biofluid sub-applications
  draw_biofluid_routes :urine, :serum, :csf, :saliva, :feces, :sweat
  
  get "/biofluids/urine/system/downloads/current/urine_metabolites.zip" => redirect("http://hmdb.ca/system/downloads/current/urine_metabolites.zip"), :as => :urine_download
  get "/biofluids/urine/system/downloads/current/urine_metabolites_structures.zip" => redirect("http://hmdb.ca/system/downloads/current/urine_metabolites_structures.zip"), :as => :urine_download_structures
  get "/biofluids/serum/system/downloads/current/serum_metabolites.zip" => redirect("http://hmdb.ca/system/downloads/current/serum_metabolites.zip"), :as => :serum_download
  get "/biofluids/serum/system/downloads/current/serum_metabolites_structures.zip" => redirect("http://hmdb.ca/system/downloads/current/serum_metabolites_structures.zip"), :as => :serum_download_structures
  get "/biofluids/feces/system/downloads/current/feces_metabolites.zip" => redirect("http://hmdb.ca/system/downloads/current/feces_metabolites.zip"), :as => :feces_download
  get "/biofluids/feces/system/downloads/current/feces_metabolites_structures.zip" => redirect("http://hmdb.ca/system/downloads/current/feces_metabolites_structures.zip"), :as => :feces_download_structures
  get "/biofluids/csf/system/downloads/current/csf_metabolites.zip" => redirect("http://hmdb.ca/system/downloads/current/csf_metabolites.zip"), :as => :csf_download
  get "/biofluids/csf/system/downloads/current/csf_metabolites_structures.zip" => redirect("http://hmdb.ca/system/downloads/current/csf_metabolites_structures.zip"), :as => :csf_download_structures
  get "/biofluids/saliva/system/downloads/current/saliva_metabolites.zip" => redirect("http://hmdb.ca/system/downloads/current/saliva_metabolites.zip"), :as => :saliva_download
  get "/biofluids/saliva/system/downloads/current/saliva_metabolites_structures.zip" => redirect("http://hmdb.ca/system/downloads/current/saliva_metabolites_structures.zip"), :as => :saliva_download_structures
  get "/biofluids/sweat/system/downloads/current/sweat_metabolites.zip" => redirect("http://hmdb.ca/system/downloads/current/sweat_metabolites.zip"), :as => :sweat_download
  get "/biofluids/sweat/system/downloads/current/sweat_metabolites_structures.zip" => redirect("http://hmdb.ca/system/downloads/current/sweat_metabolites_structures.zip"), :as => :sweat_download_structures

  root to: 'specdb/ms#search'
end
