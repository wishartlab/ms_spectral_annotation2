module ReactionsHelper
  def arrow(str)
    if str == ">"
      return "&rarr;".html_safe
    elsif str == "<"
      return "&larr;".html_safe
    elsif str == "<>"
      return "&harr;".html_safe
    end
    return "=".html_safe
  end
end