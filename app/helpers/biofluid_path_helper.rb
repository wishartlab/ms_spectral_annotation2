module BiofluidPathHelper
  # Load the variables needed get all the data for a given biofluid - this
  # is called from controller or the view depending on where the layout is
  # being loaded from. In unearth, we can't do a before action so we load it 
  # in at the top of the layout for a biofluid (sigh...)
  def load_biofluid
    @biofluid = params[:biofluid].to_s
    raise "Invalid biofluid database '#{@biofluid}'" if BIOFLUID_CONFIG[@biofluid].nil?
    
    @biofluid_name   = BIOFLUID_CONFIG[@biofluid]['name']
    @biofluid_source = BIOFLUID_CONFIG[@biofluid]['source']

    if @biofluid_name.blank? || @biofluid_source.blank?
      raise "Missing configuration in config/biofluid_apps.yml for #{@biofluid}!"
    end
  end

  def biofluid_app
    @path_builder ||= PathBuilder.new(params,url_options)
  end

  class PathBuilder
    def self.get(path_name)
      define_method "#{path_name}_url" do
        self.host + "/#{path_name}"
      end
    end

    def self.to_hmdb(path_name)
      define_method "#{path_name}_url" do
        Rails.application.config.url + "/#{path_name}"
      end
    end

    get :metabolites
    get :concentrations
    get :diseases
    get :explain
    get :databases
    get :about
    get :downloads
    get :statistics

    to_hmdb :chemquery
    to_hmdb :seqsearch
    to_hmdb :advanced_search

    def home_url
      self.host
    end

    def metabolite_url(metabolite)
      self.metabolites_url + "/#{metabolite.to_param}"
    end
    
    def download_link(biofluid_abbrev)
      "/system/downloads/current/#{biofluid_abbrev}_metabolites.zip"
    end

    def download_link_bio_structures(biofluid_abbrev)
      "/system/downloads/current/#{biofluid_abbrev}_metabolites_structures.zip"
    end
    
    def initialize(params,url_options)
      @params      = params
      @url_options = url_options
    end

    def host
      segments = [ @url_options[:protocol] ]
      if Rails.env.production?
        # In production use the URL defined
        # in /config/biofluid_apps.yml
        segments << BIOFLUID_CONFIG[ @params[:biofluid] ]["url"]
      else
        # In development use the localhost
        segments << @url_options[:host]
        segments << ":#{@url_options[:port]}" unless @url_options[:port].blank?      
        segments << "/biofluids/#{@params[:biofluid]}" 
      end
      segments.join
    end
  end
end

