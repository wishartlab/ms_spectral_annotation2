module MetabolomicCategoriesHelper

  def metabolomic_category_table_references(metabolomic_category)
    render '/metabolomic_categories/references', metabolomic_category: metabolomic_category
  end

  def metabolomic_category_details_modal
    render '/metabolomic_categories/metabolomic_category_details_modal'
  end

  def link_to_metabolomic_category_details(metabolomic_category)
    link_to "#{glyphicon('eye-open')} details".html_safe,
      main_app.metabolomic_category_path(metabolomic_category),
      data: { toggle: 'modal', target: '#metabolomic_category-details' },
      class: 'btn-concentration-details'
  end

end