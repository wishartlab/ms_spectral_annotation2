module MetabolitesHelper
  def metabolite_type_class(filter)
    return 'selected' if params[filter] == '1'
  end

  def class_path(taxon)
    "/classes/#{taxon.read_attribute(:class)}"
  end
  
  def ref_pubmed_link(concentration)
    ref = concentration.references[0].pubmed_id
    if(ref)
      (bio_url(:pubmed,concentration.references[0].pubmed_id)).html_safe
    else
      "none"
    end
  end

  # Link ontology term descriptions
  def linked_term(ontology_link)
    term = ontology_link.ontology_term
    definition = term[:definition] ||= ""
    link_to(term[:term], 'javascript:;',
                title:  definition + "\n" + "#{render '/metabolite_ontology_terms/references', :metabolite_ontology_term => ontology_link}".html_safe,
                class: 'term-ontnode')
  end

   # Link ontology term descriptions
  def linked_category(category, type)
    if type == "parent"
      link_to(category[:term], 'javascript:;',
                  title: category[:definition],
                  class: 'main-ontnode')
    else
      link_to(category[:term]+":", 'javascript:;',
                title: category[:definition],
                class: 'category-ontnode')
    end
  end

  # Return the value of a concentration based on whether it is detected, quantified or expected.
  def humanized_concentration_value(concentration)
    "#{concentration.conc_value} #{concentration.conc_unit}"
  end

  # grep the compound concentrations and convert them to uM for blood and umol/mmol_creatinine for urine
  # this function is used for plotting concentrations
  # NOTE: because both the number formats and units are arbitary the code does not always recognize
  # them. In this case "" is returned.
  def plot_data(c)
    return {} unless c.detected_and_quantified?

    mean = 0
    error = 0
   
    # the following regx parse the various concentration values used in the database
    case c.conc_value.to_s.strip
    when /^([\+\-\d\.Ee]+)$/ # basic single value i.e. 0.01   2.0e2
      mean = $1.to_f
      error = 0
    when /^<?([\d\.]+)\s*\-\s*([\d\.]+)$/ # basic range i.e. 2.0-9.0  <2.5-8.9>
      mean = ($1.to_f + $2.to_f)/2
      error = ($2.to_f - $1.to_f)/2
    when /^([\d\.]+)\s*\+\/\-\s*([\d\.]+)\s*/ # number +/- another number e.g. 2.0+/-0.4
      mean = $1.to_f
      error = $2.to_f
    when /^\>=?\s*([\d\.]+)$/ # greater or equal to a value e.g. >= 2.5
      mean = $1.to_f
      error = 0
    when /^\<=?\s*([\d\.]+)$/ # less or equal to a value e.g. <= 2.5
      mean = $1.to_f/2
      error = $1.to_f/2
    when /^([\+\-\d\.Ee]+)\s+\+\/\-\s*([\+\-\d\.Ee]+)$/ # a scientific number +/- another scientific number
      mean = $1.to_f
      error = $2.to_f
    when /^([\d\.]+)\s*\(\s*([\d\.]+)\s*[^\s\d\.\+]\s*([\d\.]+)\s*\)$/ # mean and range e.g. 2.0 (1.0 - 3.0)
      mean = $1.to_f
      error = ($3.to_f - $2.to_f)/2
    else
      return {}
    end
    
    # Convert nmol/mmol creatinine to umol/mmol creatinine
    if c.biofluid_source == "Urine" &&  c.units == "nmol/mmol creatinine" 
      mean = mean * 1000
      error = error * 1000
    end
    
    { mean: mean, error: error }
  end

  # Sort spectra into a hash, by SpecDB type
  def sorted_spectra(spectra)
    sorted_spectra = {}
    sorted_spectra[:gcms] = []
    sorted_spectra[:msms] = []
    sorted_spectra[:lcms] = []
    sorted_spectra[:nmr1d] = []
    sorted_spectra[:nmr2d] = []

    spectra.each do |spectrum|
      if spectrum.instance_of?(Specdb::CMs)
        sorted_spectra[:gcms] << spectrum
      elsif spectrum.instance_of?(Specdb::MsMs)
        if spectrum.spectrum_type =~ /^LC-MS/
          sorted_spectra[:lcms] << spectrum
        else
          sorted_spectra[:msms] << spectrum
        end
      elsif spectrum.instance_of?(Specdb::NmrOneD)
        sorted_spectra[:nmr1d] << spectrum
      elsif spectrum.instance_of?(Specdb::NmrTwoD)
        sorted_spectra[:nmr2d] << spectrum
      end
    end

    return sorted_spectra
  end
 
end

