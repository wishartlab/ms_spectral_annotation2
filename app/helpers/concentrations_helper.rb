module ConcentrationsHelper
  def concentration_table_references(concentration)
    render '/concentrations/references', concentration: concentration
  end

  def concentration_details_modal
    render '/concentrations/concentration_details_modal'
  end

  def link_to_concentration_details(concentration)
    link_to "#{glyphicon('eye-open')} details".html_safe,
      main_app.concentration_path(concentration),
      data: { toggle: 'modal', target: '#concentration-details' },
      class: 'btn-concentration-details'
  end
end
