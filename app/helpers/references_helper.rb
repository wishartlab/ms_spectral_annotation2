module ReferencesHelper
  # Return the given text with links to different references.
  def text_with_linked_references(text)
    # Please don't convert to REGEX. It is a big one and not future-self friendly.
    # Sometimes some verbosity goes a long way...

    return text if text.blank?

    begin # Rescue errors to ensure this never fails and kills the page
      linked_text = text.to_s
      linkers = { pubmed: 'PMID', omim: 'OMIM' }

      linkers.each do |link_type, matcher|
        matches = text.scan(/#{matcher}:?\s*([\d\,\s]+)/i)
        replaced = Set.new

        if matches.present?
          matches.each do |match|
            match.first.split(/,\s*/).each do |id|
              next if id.blank? || replaced.include?(id)
              replaced << id
              linked_text.gsub!(id, bio_link_out(link_type, id))
            end
          end
        end
      end

      linked_text
    rescue Exception => e
      logger.error "Couldn't link refernces in description (#{text}): #{e.message}"
    end
  end
end