class DiseaseSearcher
  include Unearth::Searcher::Searcher

  search_index :disease
  search_with :fuzzy_like_this, :basic, fuzzy_fields: [:name, :synonyms]
 
  def self.exact_match_redirect_path(query)
    iems = ["iem", "inborn error of metabolism", "metabolic disease", "genetic metabolic disease",
      "inborn errors of metabolism", "iems", "metabolic diseases", "genetic metabolic diseases"]
    if iems.include? query.downcase.strip
      query = "inborn errors of metabolism"
      #Rails.application.routes.url_helpers.diseases_path(query)
      diseases = Disease.exported.where(inborn_error_disease: 1)
      #@diseases = relation.where(diseases: { inborn_error_disease: 1 })
      Rails.application.routes.url_helpers.diseases_path("?inborn_error=1&filter=true").sub(".%3F", "?")
    else
      nil
    end
  end

  def self.highlights
    {}
  end

  def self.suggestions(query)
    candidates = [
      {
        field: "name",
        suggest_mode: "popular",
        min_word_len: 1
      },
      {
        field: "synonyms",
        suggest_mode: "popular",
        min_word_len: 2
      }
    ]
    self.index_class.suggestions(query, 'name', candidates)
  end
end
