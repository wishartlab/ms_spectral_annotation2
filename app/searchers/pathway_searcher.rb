class PathwaySearcher
  include Unearth::Searcher::Searcher

  search_index :pathway
  search_with :basic
 
  def self.exact_match_redirect_path(query)
    nil
  end

  def self.highlights
    {}
  end

  def self.suggestions(query)
    candidates = [
      {
        field: "metabolite_name",
        suggest_mode: "popular",
        min_word_len: 1
      }
    ]

    self.index_class.suggestions(query, 'name', candidates)
  end
end
