class ClassyficationSearcher
  include Unearth::Searcher::Searcher

  filter_by_groups :metabolite_status, :metabolite_biofluid

  search_index :metabolite
  search_with :name, :basic, :fuzzy_like_this,
    name_fields: [:name, :synonyms],
    fuzzy_fields: [:name, :synonyms]

  def self.prefilter(params)
    { classyfied: ['T'] }
  end

  def self.highlights
    MetaboliteSearcher.highlights
  end

  def self.suggestions(query)
    MetaboliteSearcher.suggestions(query)
  end
end
