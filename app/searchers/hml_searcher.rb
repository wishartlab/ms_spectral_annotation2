class HmlSearcher
  include Unearth::Searcher::Searcher
  
  filter_by_groups :metabolite_status, :metabolite_biofluid
  
  search_index :metabolite
  search_with :name, name_fields: [:name, :synonyms]
  search_layout :hml

  def self.exact_match_redirect_path(query)
    if /\AHMDB\d+\z/.match(query)
      Rails.application.routes.url_helpers.compound_path(Metabolite.exported.hml_compound.from_param(query))
    elsif CasValidator.cas_valid?(query)
      Rails.application.routes.url_helpers.compound_path(Metabolite.exported.hml_compound.find_by(cas: query))
    else
      nil
    end
  end

  def self.prefilter(params)
    { hml_compound: ['T'] }
  end

  def self.highlights
    MetaboliteSearcher.highlights
  end

  def self.suggestions(query)
    MetaboliteSearcher.suggestions(query)    
  end
end