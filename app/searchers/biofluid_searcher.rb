class BiofluidSearcher
  include Unearth::Searcher::Searcher
  
  filter_by_groups :metabolite_status
  
  search_index :metabolite
  search_with :name, name_fields: [:name, :synonyms]
  search_layout :biofluid

  def self.exact_match_redirect_path(query)
    if /\AHMDB\d+\z/.match(query)
      Rails.application.routes.url_helpers.compound_path(Metabolite.exported.from_param(query))
    elsif CasValidator.cas_valid?(query)
      Rails.application.routes.url_helpers.compound_path(Metabolite.exported.find_by(cas: query))
    else
      nil
    end
  end

  def self.prefilter(params)
    biofluid = params[:biofluid]
    biofluid = 'blood' if biofluid == 'serum'
    { biofluid_source: [biofluid] }
  end

  def self.highlights
    MetaboliteSearcher.highlights
  end

  def self.suggestions(query)
    MetaboliteSearcher.suggestions(query)    
  end
end