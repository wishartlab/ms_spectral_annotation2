class MetaboliteSearcher
  include Unearth::Searcher::Searcher
  
  filter_by_groups :metabolite_status, :metabolite_biofluid
  
  search_index :metabolite
  search_with :name, :basic, :fuzzy_like_this, 
    name_fields: [:name, :synonyms],
    fuzzy_fields: [:name, :synonyms]
  
  def self.exact_match_redirect_path(query)
    if /\AHMDB\d+\z/.match(query)
      Rails.application.routes.url_helpers.metabolite_path(query)
    elsif CasValidator.cas_valid?(query)
      metabolite = Metabolite.exported.where(cas: query)
      # CAS is not necessarily unique, only jump to entry if only 1 hit
      if metabolite.count == 1 
        Rails.application.routes.url_helpers.metabolite_path(metabolite.take.to_param)
      else
        nil
      end
    else
      nil
    end
  end 

  def self.highlights
    { hmdb_id: {},
      name: {},
      synonyms: { number_of_fragments: 3 },
      cas: {},
      iupac: {},
      description: { number_of_fragments: 3 } }
  end

  def self.suggestions(query)
    candidates = [
      {
        field: "synonyms",
        suggest_mode: "popular",
        min_word_len: 1
      },
      {
        field: "description",
        suggest_mode: "popular",
        min_word_len: 2
      }
    ]

    self.index_class.suggestions(query, 'name', candidates)
  end
end
