class ReactionSearcher
  include Unearth::Searcher::Searcher

  search_index :reaction
  search_with :basic
 
  def self.exact_match_redirect_path(query)
    nil
  end

  def self.highlights
    {}
  end

  def self.suggestions(query)
    self.index_class.suggestions(query, 'elements')
  end
end
