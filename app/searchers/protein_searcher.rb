class ProteinSearcher
  include Unearth::Searcher::Searcher

  search_index :protein
  search_with :basic
 
  def self.exact_match_redirect_path(query)
    if /\AHMDBP\d+\z/.match(query)
      Rails.application.routes.url_helpers.protein_path(query)
    else
      nil
    end
  end

  def self.highlights
    { hmdbp_id: {},
      name: {},
      synonyms: { number_of_fragments: 3 },
      gene_name: {},
      general_function: { number_of_fragments: 3 } }
  end

  def self.suggestions(query)
    candidates = [
      {
        field: "synonyms",
        suggest_mode: "popular",
        min_word_len: 1
      },
      {
        field: "general_function",
        suggest_mode: "popular",
        min_word_len: 2
      }
    ]

    self.index_class.suggestions(query, 'name', candidates)
  end
end
