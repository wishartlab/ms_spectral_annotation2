class OntologyExtractor < Unearth::Extractor::Base
  definition do
    target Ontology

    # This sort of wacky..
    extract(:status).with do |target|
      target.metabolite.status
    end.as(Metabolite.human_attribute_name(:status))

    # Use translations to get the attribute column names
    [:origin, :biofunction].each do |attr|
      extract(attr).as( Ontology.human_attribute_name(attr) )
    end

    extract(:cellular_locations).with do |target|
      target.metabolite.cellular_locations
    end.as(Metabolite.human_attribute_name(:cellular_locations))
  end
end


