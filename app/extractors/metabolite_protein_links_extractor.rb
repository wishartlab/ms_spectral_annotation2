class MetaboliteProteinLinksExtractor < Unearth::Extractor::Base

  definition do
    target MetaboliteProteinLink
    
    all_attributes_extractable
    
    # includes these by default
    includes :metabolite, :protein

    # Composite Children
    extract_children :metabolites, MetabolitesExtractor
    extract_children :proteins, ProteinsExtractor
  end

end
