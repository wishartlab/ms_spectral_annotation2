class LinkSetExtractor < Unearth::Extractor::Base
  definition do
    target LinkSet
    # all_attributes_extractable

    # Use translations to get the attribute column names
    LinkSet.attribute_names.each do |attr|
      extract(attr).as( LinkSet.human_attribute_name(attr) )
    end

    not_extractable :id, :omim, :pubchem_sid,
      :created_at, :updated_at, :metabolite_id
    # extract(:omim_ids)
    # extract(:pubchem_sids)
  end
end


