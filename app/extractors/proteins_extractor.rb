class ProteinsExtractor < Unearth::Extractor::Base
  definition do
    target Protein
    target_name :proteins
    # all_attributes_extractable

    # Use translations to get the attribute column names
    Protein.attribute_names.each do |attr|
      extract(attr).as( Protein.human_attribute_name(attr) )
    end

    not_extractable :id, :export, :metabolite_id,
      :comment, :created_at, :updated_at
  end
end
