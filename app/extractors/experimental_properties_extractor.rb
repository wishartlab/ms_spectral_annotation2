class ExperimentalPropertiesExtractor < Unearth::Extractor::Base
  definition do
    target ExperimentalPropertySet
    # all_attributes_extractable

    # Use translations to get the attribute column names
    ExperimentalPropertySet.attribute_names.each do |attr|
      extract(attr).as( ExperimentalPropertySet.human_attribute_name(attr) )
    end
    
    not_extractable :id, :metabolite_id, :created_at, 
      :updated_at
  end
end
