class ConcentrationMetaboliteExtractor < Unearth::Extractor::Base

  definition do
    target Metabolite

    extract :hmdb_id
    extract :name
    extract(:synonyms).with do |target|
      target.synonyms.map(&:synonym)
    end
    # includes these by default
    includes :synonyms

    all_attributes_extractable

    # Set the attributes that shouldn't be extracted
    not_extractable :id, :msds_file_name, :msds_content_type, 
      :msds_file_size, :msds_updated_at, :export_to_hmdb,
      :old_synonyms, :created_at, :updated_at, :comment

    # Rename all the moldb attributes except moldb_id
    target_class.attribute_names.grep(/moldb_/).each do |attr|
      extract(attr).as( attr.sub(/moldb_(?!id)/,"") )
    end

    # Rename the predicted properties from moldb
    extract(:moldb_alogps_solubility).as(:predicted_solubility)
    extract(:moldb_alogps_logp).as(:predicted_logp)
    extract(:moldb_alogps_logs).as(:predicted_logs)
  end

end
