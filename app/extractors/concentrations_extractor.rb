class ConcentrationsExtractor < Unearth::Extractor::Base

  definition do
    target Concentration
    
    all_attributes_extractable

    not_extractable :created_at, :updated_at, :comment, 
      :export_comment, :export_hmdb, :id, :metabolite_id

    extract_child :metabolite, ConcentrationMetaboliteExtractor
  end

end
