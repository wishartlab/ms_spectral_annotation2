class MetabolitesExtractor < Unearth::Extractor::Base

  definition do
    target Metabolite

    extract :hmdb_id
    extract :name
    extract(:synonyms).with do |target|
      target.synonyms.map(&:synonym)
    end.as('Synonyms')
    # includes these by default
    includes :synonyms

    all_attributes_extractable

    # Use translations to get the attribute column names
    Metabolite.attribute_names.each do |attr|
      extract(attr).as( Metabolite.human_attribute_name(attr) )
    end

    # Set the attributes that shouldn't be extracted
    not_extractable :id, :msds_file_name, :msds_content_type,
      :msds_file_size, :msds_updated_at, :export_to_hmdb,
      :created_at, :updated_at, :comment, :moldb_id, :moldb_pka

    # Composite Children
    # TODO add an as method
    extract_child :link_set, LinkSetExtractor
    extract_child :ontology, OntologyExtractor
    extract_child :experimental_property_set, ExperimentalPropertiesExtractor

    # Aggregate Children
    extract_children :proteins, ProteinsExtractor
  end
end
