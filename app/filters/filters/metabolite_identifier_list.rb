module Filters
  class MetaboliteIdentifierList < Unearth::Filters::Filters::List
    PLACEHOLDER = 'Provide a list of HMDB IDs and/or metabolite names, once per line.'
    EXAMPLE = ['HMDB00001', 'Glucose', 'HMDB00042']
    TITLE = 'Metabolite'

    def initialize
      super(:hmdb_id_or_name, TITLE, example: EXAMPLE, placeholder: PLACEHOLDER)
    end

    def apply_to_relation(relation)
      return relation if self.list.blank?

      hmdb_ids = []
      names = []
      self.list.each do |identifier|
        if /HMDB\d+{7}/ =~ identifier
          hmdb_ids << identifier
        else
          names << identifier
        end
      end

      hmdb_ids_from_name = find_ids_from_names(names)
      hmdb_ids = (hmdb_ids + hmdb_ids_from_name).uniq

      if relation.name == "Concentration"
        relation.joins(:metabolite).where(metabolites: { hmdb_id: hmdb_ids })
      else
        relation.merge(Concentration.joins(:metabolite).where(metabolites: { hmdb_id: hmdb_ids })).uniq
      end
    end

    private

    def find_ids_from_names(names)
      hits = MetaboliteSearcher.index_class.search(:fuzzy, names, fields: %w[name synonyms])
      hits.map(&:hmdb_id)
    end
  end
end
