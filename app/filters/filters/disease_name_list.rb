module Filters
  class DiseaseNameList < Unearth::Filters::Filters::List
    PLACEHOLDER = 'Provide a list of disease names, once per line.'
    EXAMPLE = ["Alzheimer's", 'Aciduria', 'Tuberculosis', 'Dengue', 'Heart']
    TITLE = 'Disease'

    def initialize
      super(:name, TITLE, example: EXAMPLE, placeholder: PLACEHOLDER)
    end

    def apply_to_relation(relation)
      return relation if self.list.blank?

      ids = find_ids_from_names(self.list).uniq

      if relation.name == "Concentration"
        relation.joins(:diseases).where(diseases: { id: ids })
      else
        relation.merge(Concentration.joins(:diseases).where(diseases: { id: ids })).uniq
      end
    end

    private

    def find_ids_from_names(names)
      hits = DiseaseSearcher.index_class.search(:fuzzy, names, fields: %w[name])
      hits.map(&:id)
    end
  end
end