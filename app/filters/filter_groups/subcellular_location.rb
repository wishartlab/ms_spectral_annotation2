module FilterGroups
	class SubcellularLocation < Unearth::Filters::FilterGroup

    # was done manually (from cellular_locations table in hmdb):
    # Cell Membrane (membrane) id -> 3
    # Cytoplasm id -> 1
    # Nucleus id -> 5
    # Mitochondria -> 4 

		FILTERS = {
      cell_membrane: "Cell Membrane",
      cytoplasm: "Cytoplasm",
      nucleus: "Nucleus",
      mitochondria: "Mitochondria"
    }.freeze

    def initialize
     	super('subcellular-location-filter', 'Filter by subcellular location', FILTERS)
    end

    def get_id(params)
      id = nil
      if params == :cell_membrane
        id = 3
      elsif params == :cytoplasm
        id = 1
      elsif params == :nucleus
        id = 5
      elsif params == :mitochondria
        id = 4
      end
      return id
    end

  	def apply_to_relation(relation, params)
  		applicable_filters = self.parse(params)
    		if applicable_filters.empty?
      		relation
    		else
          location_id = get_id(applicable_filters[0])
          relation.joins(:metabolite_cellular_locations).where("cellular_location_id = #{location_id}")
        end
  	end

  	def apply_to_searcher(terms={}, params)
    		applicable_filters = self.parse(params)
    		unless applicable_filters.empty?
      		terms[:name] = applicable_filters
    		end
    		terms
  	end

  	def parse(params)
    		self.filters.keys.select do |filter_param|
      		params[filter_param] == '1'
    		end
  	end
  end
end