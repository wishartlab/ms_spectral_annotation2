module FilterGroups
  class MetaboliteIdentifier < Unearth::Filters::FilterGroup
    def initialize()
      filters = {
        metabolite: Filters::MetaboliteIdentifierList.new,
        disease: Filters::DiseaseNameList.new,
      }
      super('metabolite-identifier-filter', 'Filter by identifiers', filters)
    end

    def apply_to_relation(relation, params)
      self.parse(params)
      self.filters.values.reduce(relation) do |relation, filter|
        filter.apply_to_relation(relation)
      end
    end

    def parse(params)
      self.filters.each_pair do |key, filter|
        filter.reset!
        if params[key] == '1' && params["#{key}_list"].present?
          filter.list = params["#{key}_list"].split("\n").map(&:strip)
        end
      end
    end
  end
end