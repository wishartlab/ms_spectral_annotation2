module FilterGroups
  class MetaboliteBiofluid < Unearth::Filters::FilterGroup
    FILTERS = {
      blood:        'Blood',
      urine:        'Urine',
      saliva:       'Saliva',
      csf:          'Cerebrospinal Fluid',
      feces:        'Feces',
      sweat:        'Sweat',
      breast_milk:  'Breast Milk',
      bile:         'Bile',
      amniotic_fluid: 'Amniotic Fluid',
      other_fluids: 'Other Fluids'   
    }.freeze

    def initialize()
      super('biofluid-filter', 'Filter by biofluid', FILTERS)
    end

    def apply_to_relation(relation, params)
      applicable_filters = self.parse(params)
      if applicable_filters.empty?
        relation
      else
        relation = relation.joins(:concentrations) unless relation.name == "Concentration"
        relation.
          merge(Concentration.filter_by_biofluids(applicable_filters)).
          uniq
      end
    end

    def apply_to_searcher(terms={}, params)
      applicable_filters = self.parse(params)
      unless applicable_filters.empty?
        terms[:biofluid_source] = applicable_filters
      end
      terms
    end

    def parse(params)
      self.filters.keys.select do |filter_param|
        params[filter_param] == '1'
      end
    end
  end
end