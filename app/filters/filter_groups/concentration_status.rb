module FilterGroups
  class ConcentrationStatus < Unearth::Filters::FilterGroup
    FILTERS = {
      concentration_quantified: "Detected and quantified",
      concentration_detected:   "Detected but not quantified",
      concentration_expected:   "Expected but not quantified"
    }.freeze

    def initialize
      super('concentration-status-filter', 'Filter by concentration type', FILTERS)
    end

    def apply_to_relation(relation, params)
      applicable_filters = self.parse(params)
      if applicable_filters.empty?
        relation
      else
        relation.merge(Concentration.filter_by_status(applicable_filters))
      end
    end

    def apply_to_searcher(terms={}, params)
      applicable_filters = self.parse(params)
      unless applicable_filters.empty?
        terms[:status] = applicable_filters
      end
      terms
    end

    def parse(params)
      self.filters.keys.select do |filter_param|
        params[filter_param] == '1'
      end.map { |f| f.to_s.sub(/\Aconcentration_/, '').to_sym }
    end

    def self.statuses
      FILTERS
    end
  end
end