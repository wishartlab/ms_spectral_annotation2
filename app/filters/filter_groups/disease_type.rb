module FilterGroups
	class DiseaseType < Unearth::Filters::FilterGroup
		FILTERS = {
      		inborn_error: 'Inborn Errors of Metabolism',
    	}.freeze

    	def initialize
     		super('disease-filter', 'Filter by disease type', FILTERS)
    	end

    	def apply_to_relation(relation, params)
    		applicable_filters = self.parse(params)
      		if applicable_filters.empty?
        		relation
      		else
        		relation.where(diseases: { inborn_error_disease: 1 })
      		end
    	end

    	def apply_to_searcher(terms={}, params)
      		applicable_filters = self.parse(params)
      		unless applicable_filters.empty?
        		terms[:inborn_error_disease] = applicable_filters
      		end
      		terms
    	end

    	def parse(params)
      		self.filters.keys.select do |filter_param|
        		params[filter_param] == '1'
      		end
    	end

		def self.statuses
      		FILTERS
    	end
    end
end