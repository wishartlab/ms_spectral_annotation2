json.extract! metabolomic_category, :id, :created_at, :updated_at
json.url metabolomic_category_url(metabolomic_category, format: :json)
