json.array! @concentrations.each do |concentration|
  json.id concentration.id
  json.hmdb_id concentration.metabolite.to_param
  json.compound concentration.metabolite.name
  json.kind concentration.patient_status
  json.biofluid concentration.biofluid
  json.status concentration.status
  json.concentration humanized_concentration_value(concentration)
  json.age concentration.age
  json.sex concentration.sex
  json.comment (concentration.export_comment? ? concentration.comment : nil)
  json.condition concentration.conc_condition
  json.source concentration.source
  json.articles concentration.articles do |article|
    json.pubmed_id article.pubmed_id
    json.citation article.citation
  end
  json.textbooks concentration.textbooks do |textbook|
    json.isbn textbook.isbn
    json.title textbook.title
  end
  json.external_links concentration.external_links do |external_link|
    json.name external_link.name
    json.url external_link.url
  end
end
