json.total @total
json.total_normal @total_normal
json.total_abnormal @total_abnormal
json.total_with_concentration @total_with_concentration
json.total_with_normal @total_with_normal
json.total_with_abnormal @total_with_abnormal
json.list_concentration @list_concentration
json.list_normal @list_normal
json.list_abnormal @list_abnormal
