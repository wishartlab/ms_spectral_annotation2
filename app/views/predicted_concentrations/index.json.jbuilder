json.array! @predicted_concentrations.each do |concentration|
  json.id concentration.id
  json.hmdb_id concentration.metabolite.to_param
  json.compound concentration.metabolite.name
  json.biofluid concentration.biofluid
  json.concentration "#{concentration.value} #{concentration.units}"
  json.age concentration.original_age
  json.sex concentration.original_sex
  json.comment concentration.comments
  json.condition concentration.original_conditions
end
