class DemoController < ApplicationController
  include Shared::MetaboliteLoader
  include Unearth::Extractor::Extractable
  include Unearth::Filters::Filterable

  before_action :load_metabolites

  def ad1
    render layout: "demo/ad1"
  end

  private

  def load_metabolites
    @metabolites = Metabolite.exported.
      page(params[:page]).
      order("metabolites.hmdb_id")

    extract_with MetabolitesExtractor
    extract_from @metabolites
    #render "demoes/index"
  end
end
