module Shared::MetaboliteLoader
  extend ActiveSupport::Concern

  METABOLITE_SHOW_INCLUDES = [
    :articles, :textbooks, :external_links, :link_set, :ontology,
    :accession_numbers, :synonyms, :tissues, :pathways,
    :predicted_concentrations,
    diseases: [:articles, :textbooks, :external_links],
    normal_concentrations: [:articles, :textbooks, :external_links],
    abnormal_concentrations: [:articles, :textbooks, :external_links],
    metabolite_cellular_locations: [ :cellular_location ]
  ].freeze

  def load_metabolite_show_objects!
    load_metabolite!
    if @metabolite.present? # Don't run if metabolite revoked
      load_spectra
      load_structure
    end
  end

  def load_metabolite!(metabolite_id=nil)
    metabolite_id ||= params[:id]
    @metabolite = Metabolite.exported.
      includes(METABOLITE_SHOW_INCLUDES).where(hmdb_id: metabolite_id).take

    # Metabolite may have been merged or revoked
    if @metabolite.nil?
      if accession_number = AccessionNumber.for_metabolites.where(number: metabolite_id).take
        @metabolite = Metabolite.exported.
                                 includes(METABOLITE_SHOW_INCLUDES).
                                 where(id: accession_number.element_id).
                                 take
      elsif @revocation = Revocation.find_by_metabolite_id(metabolite_id)
        return
      end
    end

    raise ActiveRecord::RecordNotFound if @metabolite.nil?
  end

  def load_spectra
    @spectra = @metabolite.spectra
  rescue Errno::ECONNREFUSED, SocketError, ActiveResource::TimeoutError => e
    @spectra = []
    @no_spectra_connection = true
  end

  def load_structure
    @structure_resource = @metabolite.structure_resource
  rescue Errno::ECONNREFUSED, SocketError, ActiveResource::TimeoutError => e
    @structure_resource = nil
    @no_moldb_connection = true
  end

  def load_metabolite_index_objects
    @metabolites = Metabolite.exported.
      includes(:concentrations).
      page(params[:page]).
      order(@sorted_column => @sorted_order)
  end
end
