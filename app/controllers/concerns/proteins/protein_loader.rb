module Proteins
  module ProteinLoader
    extend ActiveSupport::Concern

    PROTEIN_SHOW_INCLUDES = [:enzyme_classes, :go_classes, :pfams,
                             :gene_sequence, :polypeptide_sequence,
                             :articles, :textbooks, :external_links, :pathways,
                             :subcellular_locations, :synonyms,
                             exported_metabolite_protein_links: [:metabolite, :articles, :textbooks, :external_links]].freeze

    def load_protein_objects!
      # Here we load a protein object, to check if the cache exists, as well as
      # a protein scope that has the required includes to make the page fast
      # the first time.
      @protein = Protein.exported.find_by_hmdbp_id(params[:id])

      # Protein may have been merged or revoked
      if @protein.nil?
        if accession_number = AccessionNumber.for_proteins.where(number: params[:id]).take
          @protein = Protein.exported.find_by(id: accession_number.element_id)
        elsif @revocation = ProteinRevocation.find_by_protein(params[:id])
          return
        end
      end

      raise ActiveRecord::RecordNotFound if @protein.nil?
      @protein_scope = Protein.where(id: @protein.id).includes(PROTEIN_SHOW_INCLUDES)
    end
  end
end
