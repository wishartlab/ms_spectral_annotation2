class MetabolomicCategoriesController < ApplicationController
  before_action :set_metabolomic_category, only: [:show, :edit, :update, :destroy]
  before_action :set_type, only: [:index]

  # GET /metabolomic_categories
  # GET /metabolomic_categories.json
  def index
   @metabolomic_categories = Kaminari.paginate_array(type_class.all).page(params[:page]).per(25)
  end

  # GET /metabolomic_categories/1
  # GET /metabolomic_categories/1.json
  def show
    render layout: false
  end

  # GET /metabolomic_categories/new
  def new
    @metabolomic_category = MetabolomicCategory.new
  end

  # GET /metabolomic_categories/1/edit
  def edit
  end

  # POST /metabolomic_categories
  # POST /metabolomic_categories.json
  def create
    @metabolomic_category = MetabolomicCategory.new(metabolomic_category_params)

    respond_to do |format|
      if @metabolomic_category.save
        format.html { redirect_to @metabolomic_category, notice: 'Metabolomic category was successfully created.' }
        format.json { render :show, status: :created, location: @metabolomic_category }
      else
        format.html { render :new }
        format.json { render json: @metabolomic_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /metabolomic_categories/1
  # PATCH/PUT /metabolomic_categories/1.json
  def update
    respond_to do |format|
      if @metabolomic_category.update(metabolomic_category_params)
        format.html { redirect_to @metabolomic_category, notice: 'Metabolomic category was successfully updated.' }
        format.json { render :show, status: :ok, location: @metabolomic_category }
      else
        format.html { render :edit }
        format.json { render json: @metabolomic_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /metabolomic_categories/1
  # DELETE /metabolomic_categories/1.json
  def destroy
    @metabolomic_category.destroy
    respond_to do |format|
      format.html { redirect_to metabolomic_categories_url, notice: 'Metabolomic category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.

    def set_metabolomic_category
      @metabolomic_category = MetabolomicCategory.find(params[:id])
    end

    def set_type
      @type = type
    end

    def type
      params[:type]
      # MetabolomicCategory.types.include?(params[:type]) ? params[:type] : "MetabolomicCategory"
    end

    def type_class 
      type.constantize 
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def metabolomic_category_params
      params.fetch(:metabolomic_category, {})
    end

end