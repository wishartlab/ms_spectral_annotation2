class ClassyficationController < ApplicationController
  include Unearth::Filters::Filterable

  filter_by_groups *ClassyficationSearcher.original_filters

  def index
    @search = ClassyficationSearcher.search(setup_query, setup_search_options)

    respond_to do |format|
      format.html
    end
  end

  private
    def setup_search_options
      search_options =
        {
          page: params[:page],
          sort: ['hmdb_id']
        }
      prefilter = ClassyficationSearcher.prefilter(params)
      filter_options = prefilter.merge(apply_searcher_filters(ClassyficationSearcher))
      search_options.merge!(filter: filter_options)
    end

    def setup_query
      queries = []
      params[:q] ||= {}

      return '*' if params[:q].blank?

      params[:q].each do |field, value|
        next if value.blank?
        queries << "#{field}:#{value}"
      end

      if queries.present?
        queries.join(" AND ")
      else
        "*"
      end
    end
end
