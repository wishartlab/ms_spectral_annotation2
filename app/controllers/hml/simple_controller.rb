module Hml
  class SimpleController < HmlController
    respond_to :html

    def home
      render :template => "/hml/simple/home"
    end
  end
end

