module Hml
  class MetabolitesController < HmlController
    include Shared::MetaboliteLoader

    SORTABLE_COLUMNS = [:hmdb_id, :name, :moldb_formula, :moldb_average_mass, :moldb_mono_mass]
    PER_PAGE = 30

    respond_to :html

    filter_by_groups :metabolite_status
    parse_sort_params(SORTABLE_COLUMNS, default_column: 'hmdb_id', only: [:index]) 

    def index
      @pagination_scope = apply_relation_filters Metabolite.exported.hml_compound.
        select("metabolites.id").
        page(params[:page]).per(20).
        order("metabolites.#{@sorted_column} #{@sorted_order}").
        joins(:concentrations).uniq

      @table_scope = Metabolite.
        where(id: @pagination_scope.map(&:id) ).
        includes(:concentrations)

      respond_with(@table_scope) do |format|
        format.html { render :template => '/hml/metabolites/index' }
      end
    end
  end
end
