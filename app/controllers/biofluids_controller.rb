class BiofluidsController < ApplicationController
  include Unearth::Extractor::Extractable
  include Unearth::Filters::Filterable

  filter_by_groups \
    :metabolite_status,
    :metabolite_biofluid,
    :metabolite_identifier

  def index
    @metabolites = Metabolite.exported.
      includes(concentrations: [:metabolite, :articles, :textbooks, :external_links]).
      order('metabolites.hmdb_id').
      page(params[:page])
    @metabolites = apply_relation_filters(@metabolites)

    # extract_with ConcentrationsExtractor
    # extract_from apply_relation_filters(Concentration.exported)
    render 'biofluids/shared/concentrations' unless params.include?(:extract)
  end
end
