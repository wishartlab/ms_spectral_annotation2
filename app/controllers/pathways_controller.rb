class PathwaysController < ApplicationController
  
  def index
    @pathways = Pathway.includes(:metabolites).page(params[:page])
  end
  
  
end