module Biofluids
  class SimpleController < BiofluidController
    respond_to :html

    def home
      render template: "/biofluids/#@biofluid/home"
    end
  end
end