class Biofluids::BiofluidController < ApplicationController
  include Unearth::Filters::Filterable
  include BiofluidPathHelper

  self.asset_host = Rails.application.config.url if Rails.env.production?

  # Set the layout based on the class name
  layout "biofluid"
  PER_PAGE = 25
 
  before_filter :load_biofluid # In BiofluidPathHelper

  filter_by_groups :metabolite_status

  def concentrations
    @metabolites = Metabolite.
      exported.
      includes(:concentrations).
      joins(:concentrations).
      where(concentrations: { export_hmdb: 'Yes', biofluid_source: @biofluid_source }).
      page(params[:page]).
      uniq
    @metabolites = apply_relation_filters(@metabolites)
    render template: '/biofluids/shared/concentrations'
  end

  def diseases
    @diseases = Disease.
      select("diseases.id, name, omim_id").
      includes(:concentrations).
      where(concentrations: { export_hmdb: 'Yes', biofluid_source: @biofluid_source }).
      page(params[:page]).
      uniq
    @diseases = apply_relation_filters(@diseases)

    render template: '/biofluids/shared/diseases'
  end

  def downloads
    render template: '/biofluids/shared/downloads'
  end

  def stats
    @stats = BiofluidStats.new(@biofluid)
    render template: '/biofluids/shared/statistics'
  end
  
end

