module Biofluids
  class MetabolitesController < BiofluidController
    include Shared::MetaboliteLoader

    SORTABLE_COLUMNS = [:hmdb_id, :name, :moldb_formula, :moldb_average_mass, :moldb_mono_mass]

    respond_to :html

    filter_by_groups :metabolite_status
    parse_sort_params(SORTABLE_COLUMNS, default_column: 'hmdb_id', only: [:index])

    def index
      # using params to get metabolite status and apply to biofluid status
      metabolite_status = ["quantified", "detected", "expected", "predicted"]
      biofluid_status = Array.new
      metabolite_status.each do |p|
        if params.include? p
          if p == "quantified"
            biofluid_status << "Detected and Quantified"
          elsif p == "detected"
            biofluid_status << "Detected but not Quantified"
          elsif p == "expected"
            biofluid_status << "Expected but not Quantified"
          elsif p == "predicted"
            biofluid_status << "Predicted but not Quantified"
          end
        end
      end

      biofluid_concentrations_condition1 = nil
      selection_array = Array.new
      biofluid_status.each do |s|
        selection_array << "concentrations.status = '#{s}'"
      end

      if not selection_array.empty?
        biofluid_concentrations_condition1 = selection_array.to_sentence(:words_connector => ' or ',
         :last_word_connector => ' or ', :two_words_connector => ' or ')
      end

      biofluid_concentrations_condition2 = {
        concentrations: {
          export_hmdb: 'Yes',
          biofluid_source: @biofluid_source
        }
      }

      if biofluid_concentrations_condition1.nil?
        @pagination_scope = apply_relation_filters Metabolite.exported.
          select("metabolites.id").
          page(params[:page]).per(20).
          where(biofluid_concentrations_condition2).
          order("metabolites.#{@sorted_column} #{@sorted_order}").
          joins(:concentrations).uniq
      else
        @pagination_scope = apply_relation_filters Metabolite.exported.
          select("metabolites.id").
          page(params[:page]).per(20).
          where("concentrations.export_hmdb = 'Yes' and concentrations.biofluid_source = 'Feces' and (#{biofluid_concentrations_condition1})").
          order("metabolites.#{@sorted_column} #{@sorted_order}").
          joins(:concentrations).uniq
      end

      @table_scope = Metabolite.
        where(id: @pagination_scope.map(&:id) ).
        includes(:concentrations)

      respond_with(@table_scope) do |format|
        format.html { render template: '/biofluids/shared/metabolites' }
      end
    end
  end
end

=begin
module Biofluids
  class MetabolitesController < BiofluidController
    include Shared::MetaboliteLoader

    SORTABLE_COLUMNS = [:hmdb_id, :name, :moldb_formula, :moldb_average_mass, :moldb_mono_mass]

    respond_to :html

    filter_by_groups :metabolite_status
    parse_sort_params(SORTABLE_COLUMNS, default_column: 'hmdb_id', only: [:index]) 

    def index
      biofluid_concentrations_condition = {
        concentrations: {
          export_hmdb: 'Yes',
          biofluid_source: @biofluid_source
        }
      }

      @pagination_scope = apply_relation_filters Metabolite.exported.
        select("metabolites.id").
        page(params[:page]).per(20).
        where(biofluid_concentrations_condition).
        order("metabolites.#{@sorted_column} #{@sorted_order}").
        joins(:concentrations).uniq

      @table_scope = Metabolite.
        where(id: @pagination_scope.map(&:id) ).
        includes(:concentrations)

      respond_with(@table_scope) do |format|
        format.html { render template: '/biofluids/shared/metabolites' }
      end
    end
  end
end
=end