class SimpleController < ApplicationController
  def home
  end

  def about
  end

  def release_notes
  end

  def citing
  end

  def news
  end
  
  def statistics
     @specdb_stats = Specdb::Statistic.all( :params => { :database => 'HMDB' } ).first
  end

  def sources
  end

  def help
  end

  def databases
  end

  def contact
  end

  def textquery
  end

  def downloads
  end

  def help
  end
end
