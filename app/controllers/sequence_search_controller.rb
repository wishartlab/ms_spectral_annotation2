class SequenceSearchController < ApplicationController
  def index
  end
  
  def search
     puts "ENTER SEQUENCE BLAST"
     @query = Bio::FastaFormat.new( params[:sequence] )
     puts @query
     @sequence = @query.to_seq
     @sequence.auto # Guess the type of sequence. Changes the class of @sequence.
     query_sequence_type = @sequence.seq.class == Bio::Sequence::AA ? 'protein' : 'gene'
     puts query_sequence_type
     program = @sequence.seq.class == Bio::Sequence::AA ? 'blastp' : 'blastn'
     puts program
     database = "#{query_sequence_type}"
     puts database
     blast_options = { 'e' => params[:evalue].to_f, 'G' => params[:gap_cost].to_i,
                 			'E' => params[:extend_cost].to_i, 'q' => params[:mismatch_penalty].to_i, 'r' => params[:match_reward].to_i,
                 			'U' => (params[:lower_case_filtering].present? ? 'T' : 'F'), 
                 			'F' => (params[:filter_query_sequence].present? ? 'T' : 'F'),
                 			'g' => (params[:gapped_alignment].present? ? 'T' : 'F') }
     puts blast_options
     blast_options = blast_options.collect { |key, value| value.present? ? "-#{key} #{value}" : nil }.compact.join(" ")

     blaster = Bio::Blast.local( program, "index/blast/#{database}", blast_options, BLAST_CONFIG[:blastall] )
     report = blaster.query( @sequence.seq )

     puts report
     @var = ''
     @hits = Hash.new
     
     report.each do |hit|
       @var = hit.target_def
       if hit.target_def =~ /hmdb_(\d+)/
         @hits[$1.to_i] = hit
         
       end
     end
     
     @proteins = Protein.exported.find(@hits.keys)
     
     @proteins.sort! { |a, b| @hits[b.id].bit_score <=> @hits[a.id].bit_score }

     respond_to do |format|
       format.html { render :results }
     end

   end


   private
   def validate_blast_args
     redirect_on_bad_param("must enter a sequence") if params[:sequence].blank?
   end
end
