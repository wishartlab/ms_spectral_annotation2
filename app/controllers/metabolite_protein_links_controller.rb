class MetaboliteProteinLinksController < ApplicationController
  include Wishart::SortParamsParser
  include Unearth::Extractor::Extractable
  respond_to :html

  SORTABLE_COLUMNS = [:hmdb_id, :name, :moldb_formula, :moldb_average_mass, :moldb_mono_mass]

  parse_sort_params(SORTABLE_COLUMNS, default_column: 'hmdb_id', only: [:index]) 

  def index
    @metabolite = Metabolite.exported.find_by_hmdb_id(params[:metabolite_id])
    @protein = Protein.exported.find_by_hmdbp_id(params[:protein_id])

    if @metabolite.present?
      @metabolite_protein_links = @metabolite.metabolite_protein_links.all
    elsif @protein.present?
      @metabolite_protein_links = @protein.metabolite_protein_links.all
    else
      raise ActiveRecord::RecordNotFound
    end

    @metabolite_protein_links = 
      @metabolite_protein_links.where(metabolites: { export_to_hmdb: true }, proteins: { export: true }).
                                includes(:metabolite, :protein, :articles, :textbooks, :external_links).
                                order("metabolites.#{@sorted_column} #{@sorted_order}").
                                page(params[:page])
    
    extract_with MetaboliteProteinLinksExtractor
    extract_from @metabolite_protein_links

    respond_with(@metabolite_protein_links)
  end
end
