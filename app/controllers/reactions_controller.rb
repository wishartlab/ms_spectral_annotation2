class ReactionsController < ApplicationController

  def index
    @reactions = Reaction.exported.page(params[:page]).per(15)    
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @reactions }
    end
  end

  def show
    @reaction = Reaction.where(id: params[:id]).first
  end
end