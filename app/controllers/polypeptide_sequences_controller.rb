class PolypeptideSequenceController < ApplicationController
  def show
    protein = Protein.exported.find_by_hmdbp_id(params[:protein_id])
    if protein.nil?
      if accession_number = AccessionNumber.for_proteins.where(number: params[:protein_id]).first
        protein = Protein.exported.where(id: accession_number.element_id).first
      end
    end
    raise ActiveRecord::RecordNotFound if protein.nil?

    respond_to do |format|
      format.fasta do
        render text: protein.polypeptide_sequence.try(:fasta_sequence), content_type: 'text/plain' 
      end
    end
  end
end
