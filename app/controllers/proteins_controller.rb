class ProteinsController < ApplicationController
  include Wishart::SortParamsParser
  # includes the load_protein_show_objects! method
  include Proteins::ProteinLoader

  respond_to :html
  respond_to :xml, only: [:show]

  SORTABLE_COLUMNS = [:hmdbp_id, :gene_name, :locus, :name, :protein_type].freeze

  parse_sort_params(SORTABLE_COLUMNS, default_column: 'name', only: [:index])

  def index
    @proteins = Protein.exported.
      order("#{@sorted_column} #{@sorted_order}").
      includes(:exported_metabolites).
      page(params[:page])
  end

  def show
    load_protein_objects!

    if @revocation.present?
      respond_to do |format|
        format.html { render action: 'revoked' }
        format.xml { render xml: @revocation, status: 404 }
        format.any { raise ActiveRecord::RecordNotFound }
      end
      return
    end
  end
end
