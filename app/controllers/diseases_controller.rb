class DiseasesController < ApplicationController
  include Unearth::Filters::Filterable

  filter_by_groups \
    :metabolite_status,
    :metabolite_biofluid,
    :metabolite_identifier,
    :disease_type
    

  def index
    @diseases = Disease.exported.
                        order('diseases.name').
                        includes(:articles, :textbooks, :external_links,
                                 concentrations: [:metabolite]).
                        page(params[:page])                 
    @diseases = apply_relation_filters(@diseases)
  end
end
