class MetabolitesController < ApplicationController
  include Shared::MetaboliteLoader
  include Unearth::Extractor::Extractable
  include Unearth::Filters::Filterable

  MAX_PROTEINS = 10

  SORTABLE_COLUMNS = [:hmdb_id, :name, :moldb_formula, :moldb_average_mass, :moldb_mono_mass]
  parse_sort_params(SORTABLE_COLUMNS, default_column: 'hmdb_id', only: [:index])

  filter_by_groups :metabolite_status, :metabolite_biofluid, :metabolite_origin, :subcellular_location

  def index
    # Get the metabolites, based on any applied filters
    load_metabolite_index_objects
    @metabolites = apply_relation_filters(@metabolites)

    extract_with MetabolitesExtractor
    extract_from @metabolites
  end

  def show
    load_metabolite_show_objects!
    @pathways = Kaminari.paginate_array(@metabolite.pathways.sort_by(&:name)).page(params[:pathways]).per(5)
    @ontology_terms = @metabolite.get_ontology_terms

    if @revocation.nil?
      respond_to do |format|

        format.html {}
        format.xml {}
        format.js {}
      end
    else
      respond_to do |format|
        format.html { render action: 'revoked' }
        format.xml { render xml: @revocation, status: 404 }
        format.any { raise ActiveRecord::RecordNotFound }
      end
    end
  end
end
