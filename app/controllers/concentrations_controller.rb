class ConcentrationsController < ApplicationController
  inherit_resources
  belongs_to :metabolite, optional: true, finder: :from_param
  respond_to :html, :json
  actions :show, :index, :stats
  layout false

  def stats
    ids = params[:ids]
    hmdb_ids = ids.to_s.split(/,/)
    # This only count concentrations that are Detected and Quantified
    concentrations = Concentration.exported.
      joins(:metabolite).
      where(status: "Detected and Quantified").
      where("metabolites.hmdb_id IN (?)", hmdb_ids)
    
    @total = concentrations.count
    @total_normal = concentrations.normal.count
    @total_abnormal = concentrations.abnormal.count
    @total_with_concentration = concentrations.pluck('metabolites.id').uniq.length
    @total_with_normal = concentrations.normal.pluck('metabolites.id').uniq.length
    @total_with_abnormal = concentrations.abnormal.pluck('metabolites.id').uniq.length

    @list_concentration = concentrations.pluck('metabolites.hmdb_id').uniq
    @list_normal = concentrations.normal.pluck('metabolites.hmdb_id').uniq
    @list_abnormal = concentrations.abnormal.pluck('metabolites.hmdb_id').uniq
  end

  private
  
  def collection
    @concentrations ||= \
      end_of_association_chain.
        includes(:metabolite, :articles, :textbooks, :external_links).
        order("concentrations.patient_status, concentrations.biofluid_source")
  end
end
