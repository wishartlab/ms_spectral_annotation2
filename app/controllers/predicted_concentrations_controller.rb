class PredictedConcentrationsController < ApplicationController
  inherit_resources
  belongs_to :metabolite, optional: true, finder: :from_param
  respond_to :json
  actions :index
  layout false
end
