ActiveAdmin.register CiteThis::Textbook, as: "Textbook" do
  menu parent: "References", label: "Textbooks"

  filter :ref_id
  filter :title
  filter :isbn

  index do
    selectable_column
    column :ref_id
    column :title
    column :isbn
    actions
  end

  config.sort_order = ', priority: 5id_asc'
  config.per_page = 20

  show title: :ref_id do |t|
    panel "Reference Details" do
      attributes_table_for t do
        row :created_at
        row :updated_at
        row :ref_id
        row :title
        row :isbn
      end
    end
  end

  # show :title => :id do |m|
  #   panel "Identification" do
  #     attributes_table_for m, :created_at, :updated_at, :isbn, :title
  #   end

  #   if m.metabolites.count > 0
  #     panel "Metabolites" do
  #       table_for(m.metabolites) do
  #         column "HMDB ID" do |metabolite|
  #           link_to(metabolite.hmdb_id, admin_metabolite_path(metabolite))
  #         end
  #         column "Name" do |metabolite|
  #           metabolite.name
  #         end
  #       end
  #     end
  #   end

  #   if m.proteins.count > 0
  #     panel "Proteins" do
  #       table_for(m.proteins) do
  #         column "HMDB ID" do |protein|
  #           link_to(protein.hmdbp_id, admin_protein_path(protein))
  #         end
  #         column "Name" do |protein|
  #           protein.name
  #         end
  #       end
  #     end
  #   end

  #   if m.concentrations.count > 0
  #     panel "Concentrations" do
  #       table_for(m.concentrations.includes(:metabolite)) do
  #         column "Metabolite HMDB ID" do |concentration|
  #           link_to(concentration.metabolite.hmdb_id, admin_concentration_path(concentration))
  #         end
  #         column "Metabolite Name" do |concentration|
  #           concentration.metabolite.name
  #         end
  #       end
  #     end
  #   end

  #   if m.metabolite_protein_links.count > 0
  #     panel "Metabolite-Protein Associations" do
  #       table_for(m.metabolite_protein_links.includes(:metabolite, :protein)) do
  #         column "Metabolite HMDB ID" do |mpl|
  #           link_to(mpl.metabolite.hmdb_id, admin_metabolite_path(mpl.metabolite))
  #         end
  #         column "Metabolite Name" do |mpl|
  #           mpl.metabolite.name
  #         end
  #         column "Protein HMDB ID" do |mpl|
  #           link_to(mpl.protein.hmdbp_id, admin_protein_path(mpl.protein))
  #         end
  #         column "Protein Name" do |mpl|
  #           mpl.protein.name
  #         end
  #         column "Show" do |mpl|
  #           link_to('Show', admin_protein_association_path(mpl))
  #         end
  #       end
  #     end
  #   end

  #   if m.metabolite_tissues.count > 0
  #     panel "Metabolite Tissues" do
  #       table_for(m.metabolite_tissues.includes(:metabolite, :tissue)) do
  #         column "Metabolite HMDB ID" do |mt|
  #           link_to(mt.metabolite.hmdb_id, admin_concentration_path(mt))
  #         end
  #         column "Metabolite Name" do |mt|
  #           mt.metabolite.name
  #         end
  #         column "Tissue" do |mt|
  #           mt.tissue.name
  #         end
  #       end
  #     end
  #   end
  # end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.actions
    f.inputs "Reference Details" do
      f.input :ref_id, input_html: { disabled: true }
      f.input :title, :input_html => { :rows => 5 }
      f.input :isbn
    end
    f.actions
  end

end
