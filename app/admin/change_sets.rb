ActiveAdmin.register ChangeSet do
  
  filter :user
  filter :description
  filter :source

  index do
    selectable_column
    column :user
    column :source
    column :description
    column :created_at
    column :updated_at
    actions
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    
    f.inputs do 
      f.input :user
      f.input :source
      f.input :description
      f.input :data
    end    

    f.actions
  end

  show do |cs|
    panel "Details" do
      attributes_table_for cs, :user, :source, :description, :created_at, :updated_at do
        row :data do
          link_to "Download (#{number_to_human_size(cs.data_file_size)})", cs.data.url
        end
        row :metabolites_changed do
          cs.metabolites.count
        end
      end
    end
  end
end