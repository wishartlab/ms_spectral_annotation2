ActiveAdmin.register_page "Dashboard" do

  menu :priority => 1, :label => proc{ I18n.t("active_admin.dashboard") }

  content :title => proc{ I18n.t("active_admin.dashboard") } do
    columns do
      column do
        panel "Recently Updated Metabolites" do
          ul do
            Metabolite.recently_updated(5).map do |metabolite|
              span do
                li link_to(metabolite.hmdb_id, admin_metabolite_path(metabolite)) << " - " <<
                metabolite.name
              end
            end
          end
        end
      end
      column do
        panel "Recently Created Metabolites" do
          ul do
            Metabolite.recently_created(5).map do |metabolite|
              span do
                li link_to(metabolite.hmdb_id, admin_metabolite_path(metabolite)) << " - " <<
                metabolite.name
              end
            end
          end
        end
      end
    end
  end
end