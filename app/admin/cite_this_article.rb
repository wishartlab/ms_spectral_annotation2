ActiveAdmin.register CiteThis::Article, as: "Article" do
  menu parent: "References", label: "Articles"

  filter :ref_id
  filter :citation
  filter :pubmed_id

  index do
    selectable_column
    column :ref_id
    column :citation
    column :pubmed_id
    actions
  end

  config.sort_order = 'id_asc'
  config.per_page = 20

  show title: :ref_id do |t|
    panel "Reference Details" do
      attributes_table_for t do
        row :created_at
        row :updated_at
        row :ref_id
        row :citation
        row :pubmed_id
      end
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.actions
    f.inputs "Reference Details" do
      f.input :pubmed_id
    end
    f.actions
  end

end
