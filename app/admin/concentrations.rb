ActiveAdmin.register Concentration do
  menu priority: 3
  config.sort_order = 'metabolites.hmdb_id'

  controller do
    after_action :bust_cache, only: [:update, :create, :destroy]

    def scoped_collection
      Concentration.joins(:metabolite).select("metabolites.hmdb_id, concentrations.*")
    end

    private

    def bust_cache
      expire_fragment(@concentration.metabolite)
    end
  end

  scope :detected_and_quantified
  scope :detected_not_quantified
  scope :expected_not_quantified

  filter :metabolite_hmdb_id, as: :string, label: 'HMDB ID'
  filter :biofluid_source, as: :select, collection: Concentration.biofluids
  filter :source, as: :select, collection: Concentration.sources
  filter :conc_condition, label: "Condition"

  index do
    selectable_column
    column('HMDB ID') do |concentration|
      link_to concentration.hmdb_id, admin_metabolite_path(id: concentration.hmdb_id)
    end
    column :biofluid_source
    column :status
    column :source
    column :patient_status
    column("Concentration") do |concentration|
      humanized_concentration_value(concentration)
    end
    column :age
    column :sex
    column :conc_condition
    column("Exported") do |concentration|
      status_tag (concentration.export_to_hmdb? ? "Exported" : "Not Exported"),
        (concentration.export_to_hmdb? ? :ok : :error)
    end

    actions
  end

  show title: :to_s do |c|
    attributes_table do
      row :metabolite do
        link_to "#{c.metabolite.hmdb_id} - #{c.metabolite.name}", admin_metabolite_path(c.metabolite)
      end
      row :status
      if c.expected_not_quantified?
        row :expected_source
      end
      row :source
      row :patient_status
      row :biofluid_source
      row :sex
      row :age
      row :conc_condition
      row :value do
        humanized_concentration_value(c)
      end
      row(:export_hmdb) do
        status_tag (c.export_to_hmdb? ? "Exported" : "Not Exported"),
          (c.export_to_hmdb? ? :ok : :error)
      end
      row :export_comment
      row :comment
    end

    panel "Diseases" do
      table_for(c.diseases) do
        column "Name" do |disease|
          disease.name
        end
        column "Synonyms" do |disease|
          disease.synonyms
        end
        column "OMIM ID" do |disease|
          bio_link_out(:omim, disease.omim_id)
        end
      end
    end

    panel "References" do
      table_for(c.articles) do
        column "PubMed ID" do |article|
          bio_link_out(:pubmed, article.pubmed_id)
        end
        column "Citation" do |article|
          article.citation
        end
      end
      table_for(c.textbooks) do
        column "ISBN" do |textbook|
          bio_link_out(:open_isbn, textbook.isbn)
        end
        column "Title" do |textbook|
          textbook.title
        end
      end
      table_for(c.external_links) do
        column "Link" do |external_link|
          link_to(external_link.name, external_link.url, target: '_blank')
        end
      end
    end

    active_admin_comments
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.input :metabolite_hmdb_id, label: 'Metabolite HMDB ID', as: :string,
              input_html: { readonly: f.object.persisted? }
      f.input :status, collection: Concentration.statuses
      f.input :expected_source
      f.input :source, as: :radio, collection: Concentration.sources
      f.input :patient_status, as: :radio, collection: Concentration.patient_statuses
      f.input :biofluid_source, collection: Concentration.biofluids
      f.input :sex, collection: Concentration.sexes
      f.input :age, collection: Concentration.ages
      f.input :conc_condition
      f.input :conc_value, input_html: { placeholder: 'e.g. 10.0 +/- 35.0' }
      f.input :conc_unit, collection: Concentration.units
      f.input :export_hmdb, collection: ['Yes', 'No']
      f.input :export_comment, collection: ['Yes', 'No']
      f.input :comment, input_html: { rows: 2 }
      f.has_many :concentration_disease_links,
        heading: "Diseases",
        new_record: "Add Disease",
        allow_destroy: true do |cdl_f|
          cdl_f.input :disease_id, as: :string, placeholder: 'Start typing to search',
          input_html: { class: 'remote-selector formatted-select-wide',
                        data: { source: search_admin_diseases_path,
                                label: cdl_f.object.try(:disease).try(:label) } }
      end
      f.input :article_text_references, as: :text, label: 'Pubmed References', input_html: { rows: 3 }
      f.has_many :textbook_referencings, heading: "Textbooks", new_record: "Add Textbook", allow_destroy: true do |textbook_f|
        textbook_f.input :textbook_id, as: :string, placeholder: 'Start typing to search',
        input_html: { class: 'remote-selector formatted-select-wide',
                      data: { source: cite_this.textbooks_path,
                              label: textbook_f.object.try(:textbook).try(:to_label) } }
      end
      f.has_many :external_link_referencings, heading: "External Links", new_record: "Add External Link", allow_destroy: true do |link_f|
        link_f.input :external_link_id, as: :string, placeholder: 'Start typing to search',
        input_html: { class: 'remote-selector formatted-select-wide',
                      data: { source: cite_this.external_links_path,
                              label: link_f.object.try(:external_link).try(:to_label) } }
      end
    end

    f.actions
  end
end
