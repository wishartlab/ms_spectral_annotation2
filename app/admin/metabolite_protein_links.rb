ActiveAdmin.register MetaboliteProteinLink, as: "Protein Associations" do
  menu priority: 4

  controller do
    def scoped_collection
      MetaboliteProteinLink.includes(:metabolite, :protein, :articles)
    end
  end

  filter :metabolite_hmdb_id, as: :string, label: 'Metabolite HMDB ID'
  filter :metabolite_name, as: :string
  filter :protein_hmdbp_id, as: :string, label: 'Protein HMDB ID'
  filter :protein_name, as: :string
  filter :protein_uniprot_id, as: :string, label: 'Protein UniProt ID'

  index do
    selectable_column
    column("HMDB ID") do |mpl|
      if mpl.metabolite.present?
        link_to mpl.metabolite.hmdb_id, admin_metabolite_path(mpl.metabolite)
      else
        'Not available'
      end
    end
    column("Metabolite") do |mpl|
      if mpl.metabolite.present?
        mpl.metabolite.name
      else
        'Not available'
      end
    end
    column("Protein ID") do |mpl|
      link_to mpl.protein.hmdbp_id, admin_protein_path(mpl.protein)
    end
    column("Protein Name") do |mpl|
      mpl.protein.name
    end
    column("UniProt ID") do |mpl|
      mpl.protein.uniprot_id
    end
    column("PubMed References") do |mpl|
      mpl.articles.length
    end

    actions
  end

  show do |mpl|
    panel "Details" do
      attributes_table_for mpl, :metabolite, :protein, :created_at, :updated_at
      table_for(mpl.articles) do
        column "PubMed ID" do |article|
          bio_link_out(:pubmed, article.pubmed_id)
        end
        column "Citation" do |article|
          article.citation
        end
      end
      table_for(mpl.textbooks) do
        column "ISBN" do |textbook|
          bio_link_out(:open_isbn, textbook.isbn)
        end
        column "Title" do |textbook|
          textbook.title
        end
      end
      table_for(mpl.external_links) do
        column "Link" do |external_link|
          link_to(external_link.name, external_link.url, target: '_blank')
        end
      end
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs do
      f.input :metabolite_id, label: 'Metabolite', as: :string,
                placeholder: 'Start typing to search',
                input_html: { readonly: f.object.persisted?,
                              class: 'remote-selector formatted-select-wide',
                              data: { source: search_admin_metabolites_path,
                                      label: f.object.try(:metabolite).try(:label) }
                            }
      f.input :protein_id, label: 'Protein', as: :string,
                placeholder: 'Start typing to search',
                input_html: { readonly: f.object.persisted?,
                              class: 'remote-selector formatted-select-wide',
                              data: { source: search_admin_proteins_path,
                                      label: f.object.try(:protein).try(:label) }
                            }
      f.input :article_text_references, as: :text, label: 'Pubmed References', input_html: { rows: 3 }
      f.has_many :textbook_referencings, heading: "Textbooks", new_record: "Add Textbook", allow_destroy: true do |textbook_f|
        textbook_f.input :textbook_id, as: :string, placeholder: 'Start typing to search',
        input_html: { class: 'remote-selector formatted-select-wide',
                      data: { source: cite_this.textbooks_path,
                              label: textbook_f.object.try(:textbook).try(:to_label) } }
      end
      f.has_many :external_link_referencings, heading: "External Links", new_record: "Add External Link", allow_destroy: true do |link_f|
        link_f.input :external_link_id, as: :string, placeholder: 'Start typing to search',
        input_html: { class: 'remote-selector formatted-select-wide',
                      data: { source: cite_this.external_links_path,
                              label: link_f.object.try(:external_link).try(:to_label) } }
      end
    end

    f.actions
  end
end
