ActiveAdmin.register Metabolite do
  menu priority: 2

  config.sort_order = 'hmdb_id'
  config.batch_actions = true

  controller do
    after_action :bust_cache, only: [:update, :create, :destroy]

    helper Moldbi::MarvinJsHelper
    defaults finder: :find_by_hmdb_id

    private

    def bust_cache
      expire_fragment(@metabolite)
    end
  end

  # Used to search for metabolites from select2
  collection_action :search, method: :get do
    @metabolites = Metabolite.limit(20)
    params[:q].to_s.split.each do |word|
      @metabolites =
        @metabolites.where("name LIKE :q OR hmdb_id LIKE :q", q: "%#{word}%")
    end
    respond_to do |format|
      format.json do
        render json: @metabolites.as_json(only: [:id, :name], methods: [:label])
      end
    end
  end

  action_item(:more, only: [:show, :edit]) do
    link_to('Proteins', admin_protein_associations_path(q: { metabolite_hmdb_id_contains: metabolite.hmdb_id })) << " " <<
    link_to('Concentrations', admin_concentrations_path(q: { metabolite_hmdb_id_contains: metabolite.hmdb_id }))
  end

  scope :exported
  scope :not_exported
  scope :drug

  filter :hmdb_id
  filter :name
  filter :cas

  index do
    selectable_column
    column :hmdb_id
    column :name
    column :cas
    column("Exported") do |metabolite|
      status_tag (metabolite.export_to_hmdb? ? "Exported" : "Not Exported"),
        (metabolite.export_to_hmdb? ? :ok : :error)
    end
    actions
  end

  show title: :label do |m|
    panel "Identification" do
      attributes_table_for m, :created_at, :updated_at, :hmdb_id, :name, :description, :cas do
        row(:structure) do
          moldb_thumbnail(metabolite)
        end
        row(:export_to_hmdb) do
          status_tag (m.export_to_hmdb? ? "Exported" : "Not Exported"),
            (m.export_to_hmdb? ? :ok : :error)
        end
        row(:hml_compound) do
          status_tag (m.hml_compound? ? "Yes" : "No"),
            (m.hml_compound? ? :ok : :error)
        end
      end
      table_for(m.accession_numbers) do
        column "Accession Numbers" do |an|
          an.number
        end
      end
      table_for(m.synonyms) do
        column "Synonyms" do |synonym|
          synonym.synonym
        end
      end
    end

    panel "Experimental Properties" do
      attributes_table_for m, :state
      render partial: "experimental_properties", locals: { metabolite: m }
    end

    panel "Ontology" do
      attributes_table_for m.ontology, :origin, :biofunction, :application, :location
    end

    panel "Biological Properties" do
      table_for(m.metabolite_cellular_locations) do
        column "Cellular Location" do |cl|
          cl.cellular_location
        end
        column "Predicted from logP?" do |cl|
          cl.predicted_from_logp? ? 'Yes' : 'No'
        end
        column "Comment" do |cl|
          cl.comment
        end
      end

      attributes_table_for m do
        row :biofluid_locations do
          m.biofluid_locations.join('; ')
        end
        row :tissue_locations do
          m.tissues.join("; ")
        end
      end
    end

    panel "Links" do
      render partial: "links", locals: { metabolite: m }
    end

    panel "References" do
      attributes_table_for m, :synthesis_reference do
        if m.msds.exists?
          row :msds do
            link_to "Show (#{number_to_human_size(m.msds_file_size)})", m.msds.url
          end
        else
          row :msds do
            "None"
          end
        end
      end

      if m.articles.any?
        table_for(m.articles) do
          column "PubMed ID" do |article|
            bio_link_out(:pubmed, article.pubmed_id)
          end
          column "Citation" do |article|
            article.citation
          end
        end
      end
      if m.textbooks.any?
        table_for(m.textbooks) do
          column "ISBN" do |textbook|
            bio_link_out(:open_isbn, textbook.isbn)
          end
          column "Title" do |textbook|
            textbook.title
          end
        end
      end
      if m.external_links.any?
        table_for(m.external_links) do
          column "Link" do |external_link|
            link_to(external_link.name, external_link.url, target: '_blank')
          end
        end
      end
    end

    active_admin_comments
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.actions

    tabs do
      tab 'Identification' do
        f.inputs "Identification", id: 'metabolite-identification' do
          f.input :hmdb_id, input_html: { disabled: true }
          f.input :export_to_hmdb
          f.input :hml_compound
          f.input :name
          f.input :description, input_html: { rows: 5 }
          f.input :cas
          render partial: "structure_form", locals: { form: f }


          f.has_many :accession_numbers, allow_destroy: true do |an_f|
            an_f.input :number, input_html: { style: 'width: 400px'}
          end
          f.has_many :synonyms, allow_destroy: true do |synonym_f|
            synonym_f.input :synonym, input_html: { style: 'width: 400px'}
          end
        end
      end

      tab 'Experimental Properties' do
        f.inputs 'Experimental Properties' do
          f.input :state, collection: Metabolite.states
          f.has_many :experimental_property_set, heading: false, new_record: false do |eps_f|
            eps_f.input :logp
            eps_f.input :logp_reference
            eps_f.input :water_solubility
            eps_f.input :water_solubility_reference
            eps_f.input :melting_point
            eps_f.input :melting_point_reference
            eps_f.input :boiling_point
            eps_f.input :boiling_point_reference
          end
        end
      end

      tab 'Ontology' do
        f.inputs 'Ontology', for: [:ontology], id: 'metabolite-ontology' do |ontology_f|
          ontology_f.input :origin
          ontology_f.input :biofunction, input_html: { rows: 2 }
          ontology_f.input :application
          ontology_f.input :location
        end
      end

      tab 'Biological Properties' do
        f.inputs "Biological Properties", id: 'metabolite-biological-properties' do
          f.has_many :metabolite_cellular_locations, allow_destroy: true do |mcl_f|
            mcl_f.input :cellular_location
            mcl_f.input :predicted_from_logp
            mcl_f.input :article_text_references, as: :text, label: 'Pubmed References', input_html: { rows: 3 }
            mcl_f.input :comment, input_html: { rows: 2 }

          end

          f.has_many :metabolite_tissues, allow_destroy: true do |mt_f|
            mt_f.input :tissue
            mt_f.input :article_text_references, as: :text, label: 'Pubmed References', input_html: { rows: 3 }
            mt_f.input :comment, input_html: { rows: 2 }
          end
        end
      end

      tab 'Links' do
        f.inputs 'Links', for: [:link_set], id: 'metabolite-links' do |link_set_f|
          link_set_f.input :drugbank_id
          link_set_f.input :phenol_compound_id, as: :string, input_html: { maxlength: nil }
          link_set_f.input :phenol_metabolite_id, as: :string, input_html: { maxlength: nil }
          link_set_f.input :foodb_id
          link_set_f.input :knapsack_id
          link_set_f.input :jecfa_id, as: :string, input_html: { maxlength: nil }
          link_set_f.input :chemspider_id, as: :string, input_html: { maxlength: nil }
          link_set_f.input :kegg_id
          link_set_f.input :biocyc_id
          link_set_f.input :bigg_id
          link_set_f.input :wiki_id
          link_set_f.input :metagene, input_html: { rows: 2 }
          link_set_f.input :metlin_id, as: :string, input_html: { maxlength: nil }
          link_set_f.input :pubchem_cid, as: :string, input_html: { maxlength: nil }
          link_set_f.input :chebi_id, as: :string, input_html: { maxlength: nil }
          link_set_f.input :pdb_id
        end
      end

      tab 'References' do
        f.inputs "References", id: 'metabolite-references' do
          f.input :msds
          f.input :synthesis_reference, input_html: { rows: 3 }
          f.input :article_text_references, as: :text, label: 'Pubmed References', input_html: { rows: 3 }
          f.has_many :textbook_referencings, heading: "Textbooks", new_record: "Add Textbook", allow_destroy: true do |textbook_f|
            textbook_f.input :textbook_id, as: :string, placeholder: 'Start typing to search',
            input_html: { class: 'remote-selector formatted-select-wide',
                          data: { source: cite_this.textbooks_path,
                                  label: textbook_f.object.try(:textbook).try(:to_label) } }
          end
          f.has_many :external_link_referencings, heading: "External Links", new_record: "Add External Link", allow_destroy: true do |link_f|
            link_f.input :external_link_id, as: :string, placeholder: 'Start typing to search',
            input_html: { class: 'remote-selector formatted-select-wide',
                          data: { source: cite_this.external_links_path,
                                  label: link_f.object.try(:external_link).try(:to_label) } }
          end
        end
      end
    end
    f.actions
  end
end
