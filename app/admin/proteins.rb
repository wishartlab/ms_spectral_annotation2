ActiveAdmin.register Protein do
  menu false
  config.sort_order = 'hmdbp_id'
  config.batch_actions = true

  scope :exported
  scope :not_exported
  scope :enzymes
  scope :transporters

  filter :hmdbp_id
  filter :name
  filter :uniprot_id

  controller do
    defaults :finder => :find_by_hmdbp_id
  end

  index do
    selectable_column
    column :hmdbp_id
    column :name
    column("UniProt ID") do |p|
      bio_link_out(:uniprot, p.uniprot_id, nil, :target => '_blank')
    end
    column("Exported") do |p|
      status_tag (p.export? ? "Exported" : "Not Exported"), (p.export? ? :ok : :error)
    end
    actions
  end

  # Used to search for proteins from select2
  collection_action :search, method: :get do
    @proteins = Protein.limit(20)
    params[:q].to_s.split.each do |word|
      @proteins =
        @proteins.where("name LIKE :q OR uniprot_id LIKE :q OR uniprot_name LIKE :q",
                        q: "%#{word}%")
    end
    respond_to do |format|
      format.json do
        render json: @proteins.as_json(only: [:id, :name], methods: [:label])
      end
    end
  end
end
