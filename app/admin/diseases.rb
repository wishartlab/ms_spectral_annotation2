ActiveAdmin.register Disease do
  menu priority: 5

  filter :name, as: :string, label: 'Disease Name'
  filter :synonyms, as: :string, label: 'Disease Synonyms'
  filter :metagene_id, as: :string, label: 'MetaGene ID'
  filter :omim_id, as: :string, label: 'OMIM ID'

  index do
    column :name
    column :synonyms
    column :metagene_id
    column :omim_id
    column :created_at
    column :updated_at
    actions
  end

  show do |u|
    attributes_table_for u, :name, :synonyms, :metagene_id, :omim_id
  end

  form do |f|
    f.inputs "Details" do
      f.input :name
      f.input :synonyms, hint: 'Enter synonyms in a semi-colon delimeted list'
      f.input :metagene_id
      f.input :omim_id
    end
    f.actions
  end

  # Used to search for disease names from select2
  collection_action :search, method: :get do
    @diseases = Disease.limit(20)
    params[:q].to_s.split.each do |word|
      @diseases = @diseases.where("name LIKE :q OR synonyms LIKE :q", q: "%#{word}%")
    end
    respond_to do |format|
      format.json { render json: @diseases.as_json(only: [:id, :name], methods: [:label]) }
    end
  end
end
