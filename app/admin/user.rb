ActiveAdmin.register User do
  filter :email
  filter :name

  scope :active
  scope :inactive

  index do
    column :name
    column :email
    column("Status") do |u| 
      status_tag (u.active? ? "Active" : "Inactive"), (u.active? ? :ok : :error) 
    end
    column :current_sign_in_at
    column :last_sign_in_at
    column :sign_in_count
    actions
  end

  show :title => :to_label do |u|
    attributes_table_for u, :name, :email, :active
  end
  
  form do |f|
    f.inputs "Admin Details" do
      f.input :name
      f.input :email
      f.input :active
    end
    f.actions
  end
end
