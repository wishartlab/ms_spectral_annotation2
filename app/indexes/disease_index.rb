class DiseaseIndex
  include Unearth::Index::Indexable

  set_index_name 'diseases'

  mapping do
    indexes :id, index: :not_analyzed
    indexes :name, boost: 10000
    indexes :synonyms, as: 'synonym_list', boost: 400

    indexes :metabolite_id, as: "metabolites.map(&:hmdb_id)", index: :not_analyzed
    indexes :metabolite_name, as: "metabolites.map(&:all_names).flatten",
      boost: 200

    indexes :omim_id, index: :not_analyzed
    indexes :metagene_id, index: :not_analyzed
  end

  def self.indexed_document_scopes
    [ Disease.exported.includes(metabolites: [:synonyms]) ]
  end
end
