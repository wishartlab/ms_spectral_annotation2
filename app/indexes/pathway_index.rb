class PathwayIndex
  include Unearth::Index::Indexable

  set_index_name 'pathways'

  mapping do
    indexes :id, index: :not_analyzed
    indexes :name, boost: 1000
    indexes :synonyms, as: 'synonyms.map(&:synonym)', boost: 1000
    indexes :smpdb_description
    [:kegg_id, :smpdb_id, :unipathway_id, :biocyc_id].each do |attribute|
      indexes attribute, index: :not_analyzed
    end
    indexes :metabolite_id, as: "metabolites.map(&:hmdb_id)", index: :not_analyzed
    indexes :metabolite_name, as: "metabolites.map(&:all_names).flatten",
      boost: 200
  end

  def self.indexed_document_scopes
    [ Pathway.exported.includes(metabolites: [:synonyms]) ]
  end
end
