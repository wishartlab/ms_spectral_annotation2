class MetaboliteIndex
  include Unearth::Index::Indexable

  set_index_name 'metabolites'

  SHORTHAND_STATUS = {
    Concentration::EXPECTED_NOT_QUANTIFIED => 'expected',
    Concentration::DETECTED_AND_QUANTIFIED => 'quantified',
    Concentration::DETECTED_NOT_QUANTIFIED => 'detected'
  }.freeze

  mapping do
    indexes :id, index: :not_analyzed
    indexes :name
    indexes :synonyms, as: Proc.new { synonyms.map(&:synonym) }
    indexes :names, as: Proc.new { [name].concat(synonyms.map(&:synonym)) }, index: 'not_analyzed'
    indexes :iupac
    indexes :chemical_formula, as: 'moldb_formula'

    # Index the description for full text search
    indexes :description

    # Indexes needed for the search filters
    indexes :hml_compound, type: "boolean"
    indexes :status, index: 'not_analyzed',
      as: Proc.new { SHORTHAND_STATUS[status] }
    indexes :biofluid_source,
      as: Proc.new {
        biofluid_names = concentrations.map(&:biofluid_source).uniq.map do |s|
          s.gsub(/\s/,"_").downcase
        end
        if biofluid_names.include?( "cerebrospinal_fluid_(csf)")
          biofluid_names.delete "cerebrospinal_fluid_(csf)"
          biofluid_names << "csf"
        end
        other_fluids   = ( biofluid_names - FilterGroups::MetaboliteBiofluid.new.filters.keys.map(&:to_s) )
        biofluid_names << "other_fluids" if other_fluids.length > 0
        biofluid_names
    }

    # Index biofluids for the search results page
    indexes :mono_mass, as: 'moldb_mono_mass', index: 'not_analyzed'
    indexes :average_mass, as: 'moldb_average_mass', index: 'not_analyzed'
    indexes :biofluids, as: Proc.new { concentrations.map(&:biofluid_source).uniq },
                        index: 'not_analyzed'

    # Index all the ids we want to be able to search with
    indexes :hmdb_id, index: 'not_analyzed', type: "string"
    %w[ kegg_id pubchem_cid chebi_id
      bigg_id wiki_id metlin_id chemspider_id
      drugbank_id foodb_id phenol_metabolite_id knapsack_id
      phenol_compound_id
    ].each do |id_attribute|
      indexes id_attribute,
        as: Proc.new { link_set.try(id_attribute) },
        index: 'not_analyzed', type: "string"
    end
    indexes "omim_id",
      as: Proc.new { link_set.try(:omim_ids) },
      index: 'not_analyzed', type: "string"
    indexes :cas, index: 'not_analyzed', type: "string"
    indexes :pdb_id, index: 'not_analyzed', type: "string"

    # Index the attributes for ontology
    attributes_to_ignore = %w[id metabolite_id created_at updated_at hmdb_id]
    associated_classes   = { Ontology => :ontology }
    associated_classes.each do |klass, association|
      attributes_to_index = klass.attribute_names - attributes_to_ignore
      attributes_to_index.each do |attribute|
        indexes attribute, as: "#{association}.try(:#{attribute})"
      end
    end

    # Classyfire taxonomy
    indexes :classyfied, as: 'classyfired?', type: 'boolean'
    indexes :direct_parent,
      as: Proc.new { classyfirecation.try(:direct_parent_name) }
    indexes :kingdom,
      as: Proc.new { classyfirecation.try(:kingdom_name) }
    indexes :superklass,
      as: Proc.new { classyfirecation.try(:superklass_name) }
    indexes :klass,
      as: Proc.new { classyfirecation.try(:klass_name) }
    indexes :subklass,
      as: Proc.new { classyfirecation.try(:subklass_name) }
    indexes :alternative_parents,
      as: Proc.new { classyfirecation.try(:alternative_parent_names) }
    indexes :substituents,
      as: Proc.new { classyfirecation.try(:substituent_names) }
    indexes :molecular_framework,
      as: Proc.new { classyfirecation.try(:molecular_framework_name) }
    indexes :external_descriptors,
      as: Proc.new { classyfirecation.try(:external_descriptor_annotations) }
  end

  def self.indexed_document_scopes
    [Metabolite.exported.
      includes(:synonyms, :concentrations, :ontology, :link_set)]
  end
end
