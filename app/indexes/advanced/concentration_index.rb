module Advanced
  class ConcentrationIndex
    include Unearth::Index::Indexable

    set_index_name 'advanced_concentrations'
    set_default_field 'biofluid'

    # Define the index
    mapping do
      indexes :id, index: :not_analyzed

      # Index biofluids and status
      indexes :biofluid, as: 'biofluid_source'
      indexes :detected_and_quantified,
        as: Proc.new { detected_and_quantified? },
        type: 'boolean'
      indexes :detected_not_quantified,
        as: Proc.new { detected_not_quantified? },
        type: 'boolean'
      indexes :expected_not_quantified,
        as: Proc.new { expected_not_quantified? },
        type: 'boolean'

      # Concentrations
      indexes :sex, index: "not_analyzed", type: "string"
      indexes :age, type: "string"
      indexes :condition, as: 'conc_condition'
      indexes :patient_normal, as: Proc.new { normal? }, type: 'boolean'
      indexes :patient_abnormal, as: Proc.new { abnormal? }, type: 'boolean'
      indexes :concentration, as: Proc.new { conc_value.to_f }, type: 'double'
      indexes :source_hmp, as: Proc.new { hmp_source? }, type: 'boolean'
      indexes :source_referenced, as: Proc.new { reference_source? }, type: 'boolean'

      # Index the names we want to search with
      indexes :metabolite_name, as: Proc.new { metabolite.name }, index: 'not_analyzed'
      indexes :metabolite_names,
        as: Proc.new { [metabolite.name].concat(metabolite.synonyms.map(&:synonym)) }
      indexes :metabolite_hmdb_id,
        as: Proc.new { metabolite.hmdb_id },
        index: "not_analyzed",
        type: "string"
      indexes :metabolite_cas_number, as: Proc.new { metabolite.cas },
        index: "not_analyzed",
        type: "string"
    end

    # Scopes that are used to build the index. This allows us to index >1
    # model. For example you can return [ Synonym.exported, Brand.exported ]
    # if for example you are indexing different types of names.
    def self.indexed_document_scopes
      [ Concentration.exported.includes(:metabolite) ]
    end

    # TODO: Move this into customindex.
    # At least as something you can call to get a basic setup via super()
    def self.searchable_fields
      reject = [:id, :model_type, :metabolite_name]
      fields = []

      self.mapping.each do |field, options|
        next if field =~ /.+\_exact\z/
        next if reject.include? field
        fields << field
      end

      fields
    end
  end
end
