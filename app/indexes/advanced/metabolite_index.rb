module Advanced
  class MetaboliteIndex
    include Unearth::Index::Indexable

    set_index_name 'advanced_metabolites'
    set_default_field 'names'

    # Define the index
    mapping do
      indexes :id, index: :not_analyzed

      # Index the names we want to search with
      indexes :name, index: 'not_analyzed'
      indexes :names, as: Proc.new { [name].concat(synonyms.map(&:synonym)) }
      indexes :hmdb_id, index: "not_analyzed", type: "string"
      indexes :cas_number, as: 'cas', index: "not_analyzed", type: "string"

      # Index biofluids and status
      indexes :biofluids, as: Proc.new { biofluid_locations }, index: 'not_analyzed'
      indexes :detected_and_quantified,
        as: Proc.new { quantified? },
        type: 'boolean'
      indexes :detected_not_quantified,
        as: Proc.new { detected? },
        type: 'boolean'
      indexes :expected_not_quantified,
        as: Proc.new { expected? },
        type: 'boolean'

      # Index the description for full text search
      indexes :description

      # MolDB fields
      indexes_with_exact :chemical_formula, as: 'moldb_formula'
      indexes :mono_mass, as: 'moldb_mono_mass', type: 'double'
      indexes :average_mass, as: 'moldb_average_mass', type: 'double'
      indexes :predicted_logp, as: 'moldb_alogps_logp', type: 'double'
      indexes :predicted_logs, as: 'moldb_alogps_logs', type: 'double'
      indexes :predicted_solubility,
        as: Proc.new { moldb_alogps_solubility.to_f },
        type: 'double'
      indexes :electron_acceptor_count, as: 'moldb_acceptor_count', type: 'integer'
      indexes :electron_donor_count, as: 'moldb_donor_count', type: 'integer'
      indexes :refractivity, as: 'moldb_refractivity', type: 'double'
      indexes :polarizability, as: 'moldb_polarizability', type: 'double'
      indexes :formal_charge, as: 'moldb_formal_charge', type: 'integer'
      indexes :physiological_charge, as: 'moldb_physiological_charge', type: 'integer'
      indexes :pka_strongest_acidic, as: 'moldb_pka_strongest_acidic', type: 'double'
      indexes :pka_strongest_basic, as: 'moldb_pka_strongest_basic', type: 'double'

      # Other fields
      indexes :state, index: 'not_analyzed'
      indexes :origin,
        as: Proc.new { ontology.origin }
      indexes :origin,
        as: Proc.new { ontology.origin }
      indexes :biofunction,
        as: Proc.new { ontology.biofunction }
      indexes :application,
        as: Proc.new { ontology.application }
      indexes :cellular_location,
        as: Proc.new { cellular_locations.map(&:name).uniq }
      indexes :tissue_location,
        as: Proc.new { tissues.map(&:name).uniq }

      # Proteins
      indexes :number_of_proteins, as: Proc.new { proteins.count }, type: 'integer'
      indexes :protein_hmdb_id,
        as: Proc.new { proteins.map(&:to_param) }
      indexes :protein_name,
        as: Proc.new { proteins.map(&:name) }
      indexes :protein_uniprot_id,
        as: Proc.new { proteins.map(&:uniprot_id) }
    end

    # Scopes that are used to build the index. This allows us to index >1
    # model. For example you can return [ Synonym.exported, Brand.exported ]
    # if for example you are indexing different types of names.
    def self.indexed_document_scopes
      # We don't do includes on proteins or ontology here, it get's too
      # memory intensive for the production server.
      [ Metabolite.exported.includes(:synonyms) ]
    end

    # TODO: Move this into customindex.
    # At least as something you can call to get a basic setup via super()
    def self.searchable_fields
      reject = [:id, :model_type, :name]
      fields = []

      self.mapping.each do |field, options|
        next if field =~ /.+\_exact\z/
        next if reject.include? field
        fields << field
      end

      fields
    end
  end
end
