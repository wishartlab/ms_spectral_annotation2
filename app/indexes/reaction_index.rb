class ReactionIndex
  include Unearth::Index::Indexable

  set_index_name 'reactions'

  mapping do
    indexes :id, index: :not_analyzed
    indexes :elements, as: Proc.new { element_names_and_synonyms }
  end

  def self.indexed_document_scopes
    [ Reaction.exported.
               includes(reaction_enzymes: { protein: :synonyms },
                        reaction_elements: { metabolite: :synonyms } ) ]
  end
end