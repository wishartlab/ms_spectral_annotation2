class ProteinIndex
  include Unearth::Index::Indexable

  set_index_name 'proteins'

  mapping do
    indexes :id, index: :not_analyzed
    indexes :name, boost: 800
    indexes :synonyms, as: 'synonyms.map(&:synonym)'
    indexes :gene_name, boost: 800
    indexes :general_function
    indexes :specific_function
    indexes :protein_type
    indexes :metabolite_count,
      as: Proc.new { metabolites.exported.count },
      index: :not_analyzed,
      type: "integer"

    # Location
    indexes :locus
    indexes :chromosome_location, index: :not_analyzed,
      as: Proc.new{ chromosome_location&.remove(/Chromosome:/) }

    # IDs
    %w[ hmdbp_id uniprot_id uniprot_name geneatlas_id hgnc_id
      pdb_ids genbank_gene_id genbank_protein_id
    ].each do |attribute|
      indexes attribute, index: :not_analyzed
    end

    # To use for filtering
    indexes :enzyme, index: :not_analyzed
    indexes :transporter, index: :not_analyzed
  end

  def self.indexed_document_scopes
    [ Protein.exported.includes(:synonyms) ]
  end
end
