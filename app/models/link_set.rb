class LinkSet < ActiveRecord::Base
  belongs_to :metabolite, touch: true

  validates :chebi_id, :chemspider_id, :phenol_metabolite_id, :phenol_compound_id, :metlin_id, :jecfa_id,
            numericality: { only_integer: true },
            uniqueness: true,
            allow_blank: true

  validates :wiki_id,
            length: { maximum: 255 }
  validates :knapsack_id,
            length: { is: 9 },
            format: { with: /\AC\d{8}\z/, message: "must be C followed by 8 numbers (i.e. C12345678)" },
            allow_blank: true
  validates :foodb_id,
            length: { is: 9 },
            format: { with: /\AFDB\d{6}\z/, message: "must be FDB followed by 6 numbers (i.e. FDB123456)" },
            allow_blank: true
  validates :drugbank_id,
            length: { is: 7 },
            format: { with: /\ADB\d{5}\z/, message: "must be DB followed by 5 numbers (i.e. DB12345)" },
            allow_blank: true
  validates :drugbank_metabolite_id,
            length: { is: 10 },
            format: { with: /\ADBMET\d{5}\z/, message: "must be DBMET followed by 5 numbers (i.e. DBMET12345)" },
            allow_blank: true
  validates :kegg_id,
            length: { is: 6 },
            format: { with: /\AC\d{5}\z/, message: "must be a C followed by 5 numbers (i.e. C12345)" },
            allow_blank: true
  validates :pdb_id,
            length: { maximum: 255 },
            format: { with: /\A[A-Z0-9]+\Z/ },
            allow_blank: true

  # TODO the multiple id columns should use serialize and be
  # renamed to indicate that they are plural
  def omim_ids
    return [] if omim.nil?
    omim.split("; ")
  end

  def pubchem_sids
    return [] if pubchem_sid.nil?
    pubchem_sid.split("; ")
  end
end
