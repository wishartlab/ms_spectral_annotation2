class Disease < ActiveRecord::Base
  has_many :concentration_disease_links, dependent: :destroy
  has_many :concentrations, through: :concentration_disease_links
  has_many :abnormal_concentrations, through: :concentration_disease_links
  has_many :metabolites, -> { where(export_to_hmdb: true).uniq },
    through: :concentrations
  has_many :articles, -> { uniq }, through: :concentrations
  has_many :textbooks, -> { uniq }, through: :concentrations
  has_many :external_links, -> { uniq }, through: :concentrations

  scope :omim, -> { where("diseases.omim_id <> ''") }
  scope :exported, -> { joins(:metabolites).merge(Metabolite.exported).distinct }

  validates :name, presence: true, uniqueness: true, length: { maximum: 255 }
  validates :omim_id, numericality: { only_integer: true }, allow_blank: true
  validates :metagene_id, numericality: { only_integer: true }, allow_blank: true

  alias_attribute :label, :name

  def metabolite_articles(metabolite)
    articles.where(concentrations: { metabolite_id: metabolite.id })
  end

  def metabolite_textbooks(metabolite)
    textbooks.where(concentrations: { metabolite_id: metabolite.id })
  end

  def metabolite_external_links(metabolite)
    external_links.where(concentrations: { metabolite_id: metabolite.id })
  end

  def has_metabolite_references?(metabolite)
    metabolite_articles(metabolite).present? ||
      metabolite_textbooks(metabolite).present? ||
      metabolite_external_links(metabolite).present?
  end

  def exported_concentrations
    self.concentrations.exported
  end

  def synonym_list
    synonyms.nil? ? [] : synonyms.split("; ").sort
  end
end
