class Revocation < ActiveRecord::Base
  validates_presence_of :hmdb_id, :comment, :name
  
  def self.find_by_metabolite_id(id)
    self.where("hmdb_id = ? OR accession_numbers LIKE ?", id, "%#{id}%").first
  end
end
