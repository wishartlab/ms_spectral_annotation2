class MetaboliteReference < ActiveRecord::Base
  belongs_to :reference
  belongs_to :metabolite, touch: true
  validates_uniqueness_of :metabolite_id, :scope => [ :reference_id ]
end
