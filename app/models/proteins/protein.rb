class Protein < ActiveRecord::Base
  include XmlConversion # A Concern that describes the XML output for a protein
  include CiteThis::Referencer

  ALLOWED_PROTEIN_TYPES = ['Unknown', 'Enzyme', 'Transporter'].freeze

  has_one :gene_sequence, as: :sequenceable, dependent: :destroy
  has_one :polypeptide_sequence, as: :sequenceable, dependent: :destroy

  has_many :accession_numbers, dependent: :destroy, as: :element
  has_many :synonyms, class_name: "ProteinSynonym", dependent: :destroy
  has_many :enzyme_classes, dependent: :destroy

  has_many :metabolite_protein_links, dependent: :destroy
  has_many :exported_metabolite_protein_links,
    -> { joins(:metabolite).merge(Metabolite.exported) },
    class_name: 'MetaboliteProteinLink'

  has_many :metabolites, through: :metabolite_protein_links
  has_many :exported_metabolites, through: :exported_metabolite_protein_links,
    source: :metabolite

  has_many :pathway_links, as: :element, dependent: :destroy
  has_many :pathways, through: :pathway_links

  has_many :pfam_memberships, dependent: :destroy
  has_many :pfams, through: :pfam_memberships

  has_many :protein_subcellular_locations, dependent: :destroy
  has_many :subcellular_locations, through: :protein_subcellular_locations

  has_many :protein_go_classes, dependent: :destroy
  has_many :go_classes, through: :protein_go_classes

  has_many :reaction_enzymes, dependent: :destroy
  has_many :reactions, through: :reaction_enzymes

  has_many :transport_transporters, dependent: :destroy
  has_many :transports, through: :transport_transporters

  before_validation :increment_hmdbp_id, on: :create
  before_validation :set_protein_type

  validates :name, presence: true, uniqueness: true, length: { maximum: 255 }
  validates :uniprot_id, presence: true, uniqueness: true, length: { maximum: 6 }
  validates :hmdbp_id, presence: true, uniqueness: true
  validates :protein_type, inclusion: { in: ALLOWED_PROTEIN_TYPES }
  validates :uniprot_name, length: { maximum: 255 }
  validates :organism, length: { maximum: 255 }
  validates :gene_name, length: { maximum: 255 }
  validates :genbank_gene_id, length: { maximum: 255 }
  validates :genbank_protein_id, length: { maximum: 255 }
  validates :genecard_id, length: { maximum: 255 }
  validates :chromosome_location, length: { maximum: 255 }
  validates :locus, length: { maximum: 255 }
  validates :geneatlas_id, length: { maximum: 255 }
  validates :hgnc_id, length: { maximum: 255 }
  validates :kegg_id, length: { maximum: 255 }
  validates :meta_cyc_id, length: { maximum: 255 }
  validates :cellular_location, length: { maximum: 255 }

  scope :exported, -> { where(export: true) }
  scope :not_exported, -> { where(export: false) }
  scope :transporters, -> { where(transporter: true) }
  scope :enzymes, -> { where(transporter: false) }

  # Finder to find by HMDB ID (avoids method missing - we use this in many places)
  def self.find_by_hmdbp_id(hmdbp_id)
    self.where(hmdbp_id: hmdbp_id).take
  end

  def to_param
    self.hmdbp_id
  end

  def label
    "#{self.name} (#{self.uniprot_id})"
  end

  def metabolites_by_page(page, per=10)
    self.metabolites.
         exported.
         select("name, hmdb_id").
         order(:name).
         page(page).
         per(per)
  end

  def signal_regions
    if self[:signal_regions].nil?
      []
    else
      self[:signal_regions].split("\n")
    end
  end

  def transmembrane_regions
    if self[:transmembrane_regions].nil?
      []
    else
      self[:transmembrane_regions].split("\n")
    end
  end

  def pdb_id_list
    if self[:pdb_ids].nil?
      []
    else
      self[:pdb_ids].split(";")
    end
  end

  # All reactions involving this protein and the given metabolite
  def reactions_with_metabolite(metabolite)
    self.reactions.includes(:metabolites).to_a.delete_if { |reaction| !reaction.metabolites.include?(metabolite) }
  end

  def revoke!(comment)
    Protein.transaction do
      ProteinRevocation.create!( hmdbp_id: self.hmdbp_id, name: self.name, comment: comment,
                                 uniprot_id: self.uniprot_id,
                                 accession_numbers: self.accession_numbers.map(&:number).join("; ") )
      if !self.destroy
        raise ActiveRecord::DeleteRestrictionError, "Cannot destroy Protein when trying to revoke"
      end
    end
  end

  # Indicates if this protein's sequences are included in the BLAST database
  def blastable?
    self.export? && self.metabolites.exported.exists?
  end

  # Set the HMDBP to the next available ID based on the protein entries that already exist. This
  # runs pre-validation, and only sets the HMDBP ID if it hasn't been set explicitely.
  def increment_hmdbp_id
    if self.hmdbp_id.blank?
      self.hmdbp_id = Protein.exists? ? Protein.order(:hmdbp_id).last.hmdbp_id.next : 'HMDBP00001'
    end
  end

  private

  def set_protein_type
    self.protein_type =
      if self.enzyme && self.transporter
        'Enzyme/Transporter'
      elsif self.enzyme
        'Enzyme'
      elsif self.transporter
        'Transporter'
      else
        'Unknown'
      end
  end
end
