class Pfam < ActiveRecord::Base
  has_many :pfam_memberships, :dependent => :destroy
  has_many :proteins, :through => :pfam_memberships

  validates_presence_of :identifier, :name
  validates_uniqueness_of :identifier, :name
  validates_format_of :identifier, :with => /\APF\d{5}\z/, 
                                   :message => 'must be in the proper Pfam ID format', 
                                   :allow_blank => true
end
