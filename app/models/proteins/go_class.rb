class GoClass < ActiveRecord::Base
  has_many :protein_go_classes, dependent: :destroy
  has_many :proteins, through: :protein_go_classes
  
  validates_presence_of :category, :description
  validates_uniqueness_of :category, scope: :description
  
  default_scope { order(:category) }
end
