class ProteinSynonym < ActiveRecord::Base
  belongs_to :protein

  validates :protein, presence: true
  validates :synonym, presence: true, 
                      uniqueness: { scope: :protein_id },
                      length: { maximum: 255 }
  
  def to_s
    self.synonym
  end
end
