class EnzymeClass < ActiveRecord::Base
  belongs_to :protein
  validates :ec, 
    presence: true, 
    uniqueness: { scope: :protein_id }, 
    format: { with: /\A\d+\.(\d+|-)\.(\d+|-)\.(\d+|-)\z/ }
end