class ProteinGoClass < ActiveRecord::Base
  belongs_to :protein
  belongs_to :go_class
  
  validates_uniqueness_of :protein_id, :scope => :go_class_id
end
