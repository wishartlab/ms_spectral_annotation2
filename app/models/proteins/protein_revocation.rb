class ProteinRevocation < ActiveRecord::Base
  validates_presence_of :hmdbp_id, :comment, :name
  
  def self.find_by_protein_id(id)
    self.where("hmdbp_id = ? OR accession_numbers LIKE ?", id, "%#{id}%").take
  end
end
