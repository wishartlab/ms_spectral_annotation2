class ProteinSubcellularLocation < ActiveRecord::Base
  belongs_to :protein
  belongs_to :subcellular_location
end