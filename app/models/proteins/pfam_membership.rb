class PfamMembership < ActiveRecord::Base
  belongs_to :protein
  belongs_to :pfam
  
  validates_presence_of :protein_id, :pfam_id
  validates_uniqueness_of :protein_id, :scope => :pfam_id
  validates_uniqueness_of :pfam_id, :scope => :protein_id  
end
