class MetaboliteProteinLink < ActiveRecord::Base
  TYPES = %w{ enzyme transporter }.freeze

  include CiteThis::Referencer

  attr_accessor :metabolite_hmdb_id, :protein_hmdb_or_uniprot_id # Used by LIMS

  belongs_to :metabolite, touch: true
  belongs_to :protein

  validates :metabolite_id, presence: true, uniqueness: { scope: :protein_id }
  validates :protein_id, presence: true

  validate :hmdb_id_valid
  validate :protein_hmdb_id_or_uniprot_id_valid

  before_validation :set_association_ids, on: :create

  def to_s
    "#{self.metabolite.name} (#{self.metabolite.hmdb_id}) - #{self.protein.name}"
  end

  # Allows us to set the metabolite via HMDB ID
  def metabolite_hmdb_id
    @metabolite_hmdb_id || self.metabolite.try(:hmdb_id)
  end

  # Allows us to set the protein via HMDB ID or UniProt ID
  def protein_hmdb_or_uniprot_id
    @protein_hmdb_or_uniprot_id || self.protein.try(:hmdbp_id)
  end

  def self.types
    TYPES
  end

  private

  # Ensure the metabolite exists when setting via LIMS
  def hmdb_id_valid
    if self.metabolite_hmdb_id.present? && self.metabolite_id.blank?
      if self.metabolite_hmdb_id !~ /^HMDB\d{7}$/
        self.errors[:metabolite_hmdb_id] << "is invalid"
      elsif Metabolite.find_by_hmdb_id(self.metabolite_hmdb_id).blank?
        self.errors[:metabolite_hmdb_id] << "does not exist"
      end
    end
  end

  # Ensure the protein exists when setting via LIMS
  def protein_hmdb_id_or_uniprot_id_valid
    if self.protein_hmdb_or_uniprot_id.present? && self.protein_id.blank?
      if self.protein_hmdb_or_uniprot_id !~ /\A(HMDBP\d{7}|\W{6})\z/
        self.errors[:protein_hmdb_or_uniprot_id] << "is invalid"
      elsif Protein.where("uniprot_id = ? OR hmdbp_id = ?", self.protein_hmdb_or_uniprot_id, self.protein_hmdb_or_uniprot_id).first.blank?
        self.errors[:protein_hmdb_or_uniprot_id] << "does not exist"
      end
    end
  end

  # Allows the association with metabolite to be set via the HMDB ID on create
  def set_association_ids
    if self.metabolite_hmdb_id.present? && !self.persisted?
      metabolite = Metabolite.find_by_hmdb_id(self.metabolite_hmdb_id)
      self.metabolite_id = metabolite.id if metabolite.present?
    end

    if self.protein_hmdb_or_uniprot_id.present? && !self.persisted?
      protein = Protein.where("uniprot_id = ? OR hmdbp_id = ?", self.protein_hmdb_or_uniprot_id, self.protein_hmdb_or_uniprot_id).first
      self.protein_id = protein.id if protein.present?
    end
  end
end
