class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable

  validates :name, presence: true, length: { maximum: 255 }, uniqueness: true

  after_create { |user| user.send_reset_password_instructions }

  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }

  def to_label
    "#{self.name} (#{self.email})"
  end

  # Don't require the password when creating a new user.
  # Allows us to just send the user the instructions on how to set their own password.
  def password_required?
    new_record? ? false : super
  end

  # This method is called from devise to get the list of users that are allowed to authenticate. 
  # So we skip users that aren't 'active'.
  def self.find_for_authentication(conditions={})
    conditions[:active] = 1
    super
  end
end
