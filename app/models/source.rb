class Source < ActiveRecord::Base
  has_many :metabolites, dependent: :restrict_with_error

  validates :organism, presence: true, length: { maximum: 255 }, :uniqueness => {:case_sensitive => false}

end


