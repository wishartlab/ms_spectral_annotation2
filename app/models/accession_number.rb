class AccessionNumber < ActiveRecord::Base
  belongs_to :element, polymorphic: true

  default_scope { order('number ASC') }

  validates :element, presence: true
  validates :element_type, presence: true
  validates_presence_of :number
  validates_format_of :number, with: /\A(?:HMDB)(\d{5}|\d{7})\z/,
                               allow_blank: true,
                               if: "self.element_type == 'Metabolite'"
  validates_format_of :number, with: /\A(?:HMDBP)\d{5}\z/,
                               allow_blank: true,
                               if: "self.element_type == 'Protein'"
  validates_uniqueness_of :number, scope: :element_type

  scope :for_metabolites, -> { where(element_type: 'Metabolite') }
  scope :for_proteins, -> { where(element_type: 'Protein') }

  def to_s
    self.number
  end
end
