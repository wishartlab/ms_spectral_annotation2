# What a bloody MESS! Everything below is garbage and needs to
# be replaced at some point.


# Modified by Rah Nov. 26 2012
# The statistic model dynamically calculated values for a pre-defined list
# of items. The model does not use any database table, but rather hardcode the
# list and some values as static variables

# Updated by Rah May 29, 2012
# the table (endogenous data) is now calculated from the related models

module Statistic
  def self.stats
  @table ||= {
    'Metabolite Statistics'    =>
    {
      'Total Number of Metabolites'                                                                => 114100,
      'Total Number of Detected and Quantified Metabolites'                                        => 0,
      'Total Number of Detected but Not Quantified Metabolites'                                    => 0,
      'Total Number of Expected Metabolites'                                                       => 0,
      'Total Number of Predicted Metabolites'                                                      => 0,
      'Total Number of Endogenous Metabolites'                                                     => 0,
      # 'Total Number of Metabolite Synonyms'                                                        => 0,
      
      'Total Number of Food Metabolites'                                                           => 0,
      'Total Number of Microbial Metabolites'                                                      => 0,
      'Total Number of Drug Metabolites'                                                           => 0,
      'Total Number of Plant Metabolites'                                                          => 0,
      'Total Number of Toxin/Pollutant Metabolites'                                                => 0,
      'Total Number of Cosmetic Metabolites'                                                       => 0,
      
      'Total Number of Non-redundant Metabolites with Spectra'                                     => 0,
      'Total Number of Metabolites Having Associated Proteins (Enzymes and Transporters)'          => 0,
      'Total Number of Metabolite in the Human Metabolome Library (HML)'                           => 0,
      'Total Number of Metabolites with Synthesis Records'                                         => 0,
      
    },

    'Biofluid Statistics'       =>
    {
      'Total Number of Different Biofluids'                                                        => 0,
    },

    'Concentration Statistics' => {
     },

    'Disease Statistics'       =>
    {
      'Total Number of Different Diseases'                                                         => 0,
      'Total Number of Metabolites Associated with Diseases'                                       => 0,
      'Total Number of Metabolite Concentrations for Diseases'                                     => 0
    },

    'Spectra Statistics' =>
    {
      'Total Spectra' => 351811,
      'Total Compounds with Spectra' => 135138,
      'Total NMR Spectra' => 3897,
      'Total Compounds with NMR Spectra' => 2391,
      'Total NMR 1D Spectra' => 2862,
      'Total Compounds with NMR 1D Spectra' => 1513,
      'Total NMR 2D Spectra' => 1035,
      'Total Compounds with NMR 2D Spectra' => 878,
      #'Total LC-MS Spectra' => 326611,
      #'Total Compounds with LC-MS Spectra' => 105337,
      'Total Experimental LC-MS/MS Spectra' => 22247,
      'Total Compounds with Experimental LC-MS/MS Spectra' => 2265,
      'Total Predicted LC-MS/MS Spectra' => 279972,
      'Total Compounds with Predicted LC-MS/MS Spectra' => 98601,
      'Total Experimental GC-MS Spectra' => 7418,
      'Total Compounds with Experimental GC-MS Spectra' => 2544,
      'Total Predicted GC-MS Spectra' => 38277,
      'Total Compounds with Predicted GC-MS Spectra' => 26880

    },

    'Instrument Spectra Statistics' => {}, # Filled in from SpecDB
    'Protein Statistics'       => {
      'Total Number of Unique Enzymes'                                                             => 0,
      'Total Number of Unique Transporters'                                                        => 0,
      'Total Number of Unique Proteins'                                                            => 0,
      'Total Number of Proteins with PDB Accessions'                                               => 0
    },

    'Reference Statistics'     => {
      'Total Number of References'                                                                 => 0
    },

    'Link-out Statistics'       => {
      'ChemSpider links'                                                                           => 0,
      'PubChem Compound links'                                                                     => 0,
      'KEGG Compound links'                                                                        => 0,
      'KEGG Pathways links'                                                                        => 0,
      'ChEBI Compound links'                                                                       => 0,
      'Wikipedia links'                                                                            => 0
    }
     }
  end

  # Update the class variable
  def self.update
    metabolite_scope = Metabolite.exported
    specdb_stats = Specdb::Statistic.all(params: { database: 'HMDB' }).first

    # Metabolite Counts
    total_count = metabolite_scope.count
    detected_count = Metabolite.exported.joins(:predicted_concentrations).select("DISTINCT metabolite_id").union(Metabolite.exported.joins(:metabolomic_categories).select("DISTINCT metabolite_id")).union(Concentration.exported.joins(:metabolite).select("DISTINCT metabolite_id").where("concentrations.status LIKE '%Detected%' AND export_to_hmdb = 1")).union(Metabolite.exported.joins(:metabolite_tissues).select("DISTINCT metabolite_id").where("tissue_id != '' AND tissue_id IS NOT NULL")).count
    quantified_count = Metabolite.exported.joins(:predicted_concentrations).select("DISTINCT metabolite_id").where("value != '' AND value IS NOT NULL").union(Concentration.exported.joins(:metabolite).select("DISTINCT metabolite_id").where("concentrations.status = 'Detected and Quantified' AND export_to_hmdb = '1'")).count
    detected_not_quantified_count = detected_count - quantified_count
    predicted_count = Metabolite.exported.where('predicted_in_hmdb = 1').count
    expected_count = total_count - (quantified_count + detected_not_quantified_count + predicted_count)

    # stats['Metabolite Statistics']['Total Number of Metabolite Synonyms'] = \
      # Synonym.count
    stats['Metabolite Statistics']['Total Number of Non-redundant Metabolites with Spectra'] = \
      specdb_stats.total_compounds_with_spectra
    stats['Metabolite Statistics']['Total Number of Metabolites Having Associated Proteins (Enzymes and Transporters)'] = \
      MetaboliteProteinLink.joins(:metabolite).joins(:protein).select("distinct metabolite_id").where("export_to_hmdb = 1 AND (protein_type = 'Enzyme' OR protein_type = 'Transporter')").count

    stats['Metabolite Statistics']['Total Number of Metabolite in the Human Metabolome Library (HML)'] = \
      867
    stats['Metabolite Statistics']['Total Number of Metabolites with Synthesis Records'] = \
      metabolite_scope.where("synthesis_reference != '' AND synthesis_reference IS NOT NULL").count
    stats['Metabolite Statistics']['Total Number of Detected and Quantified Metabolites'] = \
      quantified_count
    stats['Metabolite Statistics']['Total Number of Detected but Not Quantified Metabolites'] = \
      detected_not_quantified_count
    stats['Metabolite Statistics']['Total Number of Expected Metabolites'] = \
      expected_count
    stats['Metabolite Statistics']['Total Number of Predicted Metabolites'] = \
      predicted_count

    stats['Metabolite Statistics']['Total Number of Endogenous Metabolites'] = \
      metabolite_scope.joins(:ontology).where("origin LIKE '%Endogenous%'").count
    stats['Metabolite Statistics']['Total Number of Food Metabolites'] = \
      metabolite_scope.joins(:ontology).where("origin LIKE '%Food%'").count
    stats['Metabolite Statistics']['Total Number of Microbial Metabolites'] = \
      metabolite_scope.joins(:ontology).where("origin LIKE '%Microbial%'").count
    stats['Metabolite Statistics']['Total Number of Drug Metabolites'] = \
      metabolite_scope.joins(:ontology).where("origin LIKE '%Drug%'").count
    stats['Metabolite Statistics']['Total Number of Plant Metabolites'] = \
      metabolite_scope.joins(:ontology).where("origin LIKE '%Plant%'").count
    stats['Metabolite Statistics']['Total Number of Toxin/Pollutant Metabolites'] = \
      metabolite_scope.joins(:ontology).where("origin LIKE '%Toxin/Pollutant%'").count
    stats['Metabolite Statistics']['Total Number of Cosmetic Metabolites'] = \
      metabolite_scope.joins(:ontology).where("origin LIKE '%Cosmetic%'").count



    #stats['Metabolite Statistics']['Total Number of Metabolites'] = metabolite_scope.count

    stats['Biofluid Statistics']['Total Number of Different Biofluids'] = Concentration.exported.select("DISTINCT biofluid_source").where("biofluid_source != 'N/A'").count
    Concentration.exported.select("DISTINCT biofluid_source").where("biofluid_source != 'N/A'").each do |c|
      title = "Total Number of Compounds Detected in " + c.biofluid_source.sub('_',' ').sub(/Tears$/,'Tear Fluid').sub(/Bile/,'Bile Fluid')
      metabolite_count = Metabolite.exported.joins(:predicted_concentrations).select("DISTINCT metabolite_id").where("biofluid = '#{c.biofluid_source}'").union(Metabolite.exported.joins(:metabolomic_categories).select("DISTINCT metabolite_id").where("biofluid_source = '#{c.biofluid_source}'")).union(Metabolite.exported.joins(:metabolite_tissues).joins(:tissues).select("DISTINCT metabolite_tissues.metabolite_id").where("metabolite_tissues.tissue_id != '' AND metabolite_tissues.tissue_id IS NOT NULL AND tissues.name = '#{c.biofluid_source}'")).union(Concentration.exported.joins(:metabolite).select("DISTINCT metabolite_id").where("concentrations.status LIKE '%Detected%' AND biofluid_source = '#{c.biofluid_source}' AND export_to_hmdb = 1")).count
      
      if metabolite_count >= 10
        stats['Biofluid Statistics'][title] = metabolite_count
      end
    end

    stats['Concentration Statistics']['Total Number of Compounds Detected and Quantified for Normal Individuals'] = Concentration.joins(:metabolite).select("DISTINCT metabolite_id").exported.where("concentrations.status = 'Detected and Quantified' AND patient_status = 'Normal' AND export_to_hmdb = 1").count
    stats['Concentration Statistics']['Total Number of Compounds Detected and Quantified for Abnormal Conditions'] = Concentration.joins(:metabolite).select("DISTINCT metabolite_id").exported.where("concentrations.status = 'Detected and Quantified' AND patient_status = 'Abnormal' AND export_to_hmdb = 1").count
    Concentration.exported.select("DISTINCT biofluid_source").where("status = 'Detected and Quantified'").each do |c|
      title = "Total Number of Compounds Detected and Quantified in " + c.biofluid_source.sub('_',' ').sub(/Tears$/,'Tear Fluid').sub(/Bile/,'Bile Fluid')
      metabolite_count = Metabolite.exported.joins(:predicted_concentrations).select("DISTINCT metabolite_id").where("biofluid = '#{c.biofluid_source}'").union(Concentration.exported.joins(:metabolite).select("DISTINCT metabolite_id").where("concentrations.status LIKE '%Detected and Quantified%' AND biofluid_source = '#{c.biofluid_source}' AND export_to_hmdb = 1")).count

      if metabolite_count >= 10
        stats['Concentration Statistics'][title] = metabolite_count
      end
    end

    stats['Disease Statistics']['Total Number of Different Diseases'] = Disease.count
    stats['Disease Statistics']['Total Number of Metabolites Associated with Diseases'] = metabolite_scope.joins(:diseases).select('metabolites.id').map(&:id).uniq.count
    stats['Disease Statistics']['Total Number of Metabolite Concentrations for Diseases'] = Concentration.joins(:diseases).select('concentrations.id').map(&:id).uniq.count

=begin
    # stats['Spectra Statistics']['Total Compounds with Spectra'] = specdb_stats.total_compounds_with_spectra
    stats['Spectra Statistics']['Total Compounds with NMR Spectra'] = specdb_stats.total_compounds_with_nmr_spectra
    stats['Spectra Statistics']['Total Compounds with NMR 1D Spectra'] = specdb_stats.total_compounds_with_one_d_nmr_spectra
    stats['Spectra Statistics']['Total Compounds with NMR 2D Spectra'] = specdb_stats.total_compounds_with_two_d_nmr_spectra
    stats['Spectra Statistics']['Total Compounds with LC-MS Spectra'] = specdb_stats.total_compounds_with_ms_spectra
    stats['Spectra Statistics']['Total NMR Spectra'] = specdb_stats.total_nmr_spectra
    # stats['Spectra Statistics']['Total LC-MS Spectra'] = specdb_stats.total_ms_spectra
    # stats['Spectra Statistics']['Total Predicted LC-MS/MS Spectra'] = specdb_stats.total_predicted_ms_ms_spectra
    stats['Spectra Statistics']['Total Experimental LC-MS/MS Spectra'] = specdb_stats.total_experimental_ms_ms_spectra
    # stats['Spectra Statistics']['Total Spectra'] = specdb_stats.total_spectra
=end
    specdb_stats.total_spectra_per_type.each do |statistics|
      stats['Instrument Spectra Statistics']["Total Number of #{statistics.name.sub(/Spectrum$/, 'Spectra')}"] = statistics.value
    end

    stats['Protein Statistics']['Total Number of Unique Enzymes'] = \
      Protein.exported.joins(:reaction_enzymes).distinct.count
    stats['Protein Statistics']['Total Number of Unique Transporters'] = \
      Protein.exported.joins(:transport_transporters).distinct.count
    stats['Protein Statistics']['Total Number of Unique Proteins'] = \
      Protein.exported.select('distinct uniprot_id').count + 77
    stats['Protein Statistics']['Total Number of Proteins with PDB Accessions'] = \
      Protein.exported.select('distinct uniprot_id').where("pdb_ids <> ''").count

    stats['Reference Statistics']['Total Number of References'] = CiteThis::Article.count + CiteThis::Textbook.count

    stats['Link-out Statistics']['ChemSpider links'] = \
      metabolite_scope.joins(:link_set).where("link_sets.chemspider_id <> ''").count
    stats['Link-out Statistics']['PubChem Compound links'] = \
      metabolite_scope.joins(:link_set).where("link_sets.pubchem_cid <> ''").count
    stats['Link-out Statistics']['KEGG Compound links'] = \
      metabolite_scope.joins(:link_set).where("link_sets.kegg_id <> ''").count
    stats['Link-out Statistics']['KEGG Pathways links'] = \
      Pathway.where("kegg_id is not NULL AND kegg_id != ''").count
    stats['Link-out Statistics']['ChEBI Compound links'] = \
      metabolite_scope.joins(:link_set).where("link_sets.chebi_id <> ''").count
    stats['Link-out Statistics']['Wikipedia links'] = \
      metabolite_scope.joins(:link_set).where("link_sets.wiki_id <> ''").count
    self
  end
end