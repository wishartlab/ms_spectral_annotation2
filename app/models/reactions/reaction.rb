class Reaction < ActiveRecord::Base
  include CiteThis::Referencer

  has_many :reaction_elements, dependent: :destroy
  has_many :left_reaction_elements, dependent: :destroy
  has_many :right_reaction_elements, dependent: :destroy
  has_many :reaction_enzymes, dependent: :destroy
  has_many :proteins, through: :reaction_enzymes
  has_many :metabolites, through: :reaction_elements

  validates :locations, :ecocyc_id, length: { maximum: 255 }
  validates :kegg_reaction_id, format: { with: /\AR\d{5}\z/ }, allow_blank: true

  validate :has_left_and_right_elements

  scope :exported, -> { where(export: true) }

  def to_s
    str = ""
    self.left_reaction_elements.each do |re|
      str += re.stoichiometry.to_i.to_s + " " if !re.stoichiometry.nil? && re.stoichiometry > 1
      if re.metabolite.nil?
        str += re.altext + " "
      else
        str += re.metabolite.name + " "
      end

      str += "+ " if self.left_reaction_elements.last != re

    end

    str += '→'
    self.right_reaction_elements.each do |re|
      str += re.stoichiometry.to_i.to_s if !re.stoichiometry.nil? && re.stoichiometry > 1
      if re.metabolite.nil?
        str += " " + re.altext
      else
        str += " " + re.metabolite.name
      end

      str += " +" if self.right_reaction_elements.last != re
    end
    str
  end

  def element_names_and_synonyms
    names = reaction_elements.pluck(:altext)
    reaction_elements.each do |elem|
      unless elem.metabolite_id.nil?
        metabolite = elem.metabolite
        names << metabolite.name if metabolite.try(:name)
        if metabolite.present? && metabolite.synonyms.present?
          names += metabolite.synonyms.pluck("synonym")
        end
      end
    end
    reaction_enzymes.each do |enzyme|
      unless enzyme.protein_id.nil?
        names << enzyme.protein.name if enzyme.protein.present? && enzyme.protein.name.present?
        if enzyme.protein.present? && enzyme.protein.synonyms.present?
          names += enzyme.protein.synonyms.map(&:synonym)
        end
      end
    end
    return names
  end

  def status
    if self.predicted_in_hmdb?
      "Predicted Data"
    else
      "Experimental Data"
    end
  end

  def exported?
    self.export?
  end

  protected

  def has_left_and_right_elements
    if self.exported?
      if self.left_reaction_elements.blank?
        self.errors[:left_reaction_elements] << "Left reaction element must exist"
      end
      if self.right_reaction_elements.blank?
        self.errors[:right_reaction_elements] << "Right reaction element must exist"
      end
    end
  end
end
