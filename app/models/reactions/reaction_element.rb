class ReactionElement < ActiveRecord::Base
  belongs_to :reaction
  belongs_to :metabolite, touch: true
  
  validates :reaction, presence: true
  validates :stoichiometry, 
    allow_blank: true, 
    format: { with: /\A(\d+\.?\d*)|(n\+?\d*)\z/ }
  validates :type, presence: true
  
  def stoichiometry_string
    return '' if self.stoichiometry.to_f == 1
    if self.stoichiometry =~ /^\d+\.?\d*$/
      self.stoichiometry.sub(/\.?0+$/,'')
    else
      self.stoichiometry
    end
  end
end