class ReactionEnzyme < ActiveRecord::Base
  belongs_to :reaction
  belongs_to :protein
end