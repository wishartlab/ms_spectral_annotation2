class ExperimentalPropertySet < ActiveRecord::Base
  belongs_to :metabolite, touch: true
  
  validates :logp, 
    numericality: true, allow_blank: true
  validates :water_solubility, :melting_point, :boiling_point,
    length: { maximum: 255 }, allow_blank: true
  validates :logp_reference, :water_solubility_reference, :melting_point_reference, :boiling_point_reference,
    length: { maximum: 1000 }, allow_blank: true
end
