class Ontology < ActiveRecord::Base
  belongs_to :metabolite, touch: true

  validates :origin, length: { maximum: 255 }
  validates :application, length: { maximum: 255 }
  validates :location, length: { maximum: 255 }

  ALLOWED_ORIGINS = [
    "Endogenous", "Food", "Microbial", "Drug", "Plant",
    "Toxin/Pollutant", "Cosmetic", "Exogenous", "Drug Metabolite"
  ].freeze

  def self.allowed_origins
    ALLOWED_ORIGINS
  end

  # Takes an array of origins, an empty array will return the
  # scope without applying a filter)
  def self.filter_by_origin(origins_to_select)
    return all if origins_to_select.empty?

    origin_scope = origins_to_select.map do |origin_name|
      arel_table[:origin].matches("%#{origin_name}%" )
    end.reduce(&:or)

    where(origin_scope)
  end

  def origins
    return [] if origin.nil?
    @origins ||= origin.split('; ').sort
  end

  def biofunctions
    return [] if biofunction.nil?
    @biofunctions ||= biofunction.split('; ').sort
  end

  def applications
    return [] if application.nil?
    @applications ||= application.split('; ').sort
  end
end
