class MetaboliteTissue < ActiveRecord::Base
  include CiteThis::Referencer

  belongs_to :metabolite, touch: true
  belongs_to :tissue
end
