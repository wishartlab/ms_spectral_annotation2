class MetabolomicCategory < ActiveRecord::Base
  include CiteThis::Referencer

  belongs_to :metabolite

  TYPES = ["DiurnalLink","BmiLink","AgeLink","GenderLink","GenoLink","PharmacoLink"]
  AGES =  ['Not Specified', 'Newborn (0-30 days old)', 'Infant (0-1 year old)',
           'Children (1-13 years old)', 'Adolescent (13-18 years old)', 'A few days - 30 years old', 'Adult ',
           'Adult (>18 years old)', 'Adult (24-38years old)', 'Adult (60 years old)', 'Adult (40 years old)',
           'Elderly (>65 years old)', 'Adult (25-30 years old)','Centenial (>99 years old)'].freeze
  SEXES = ['Not Specified', 'Male', 'Female', 'Both'].freeze
  EFFECTS = ["Increase","Decrease","Female>Male","Male>Female"].freeze
  ALLOWED_BIOFLUIDS = ['Urine', 'Saliva', 'Cerebrospinal Fluid (CSF)', 'Blood',
                       'Bile', 'Cellular Cytoplasm', 'Amniotic Fluid', 'Semen',
                       'Breast Milk', 'Pericardial Effusion', 'Ascites Fluid',
                       'Aqueous Humour', 'Sweat', 'Tears', 'Lymph',
                       'Prostate Tissue', 'Feces'].freeze

  validates :type, presence: true, inclusion: { in: TYPES }
  # validates :age, inclusion: { in: AGES }, allow_blank: true
  validates :age,
    format: { with: /(^Infant )|(^Adult )|(^Adolescent )|(^Newborn )|(^Children )|(^Elderly )|(^Centenial )|(^Not Specified)/,
              message: "Age group must be adult, elderly, centenial, infant, adolescent, newborn or children or Not Specified" }#, allow_blank: true
  validates :sex, inclusion: { in: SEXES }#, allow_blank: true
  validates :effect, presence: true, inclusion: { in: EFFECTS }
  validates :biofluid_source, presence: true, inclusion: { in: ALLOWED_BIOFLUIDS }

  def self.types
    TYPES
  end

end
