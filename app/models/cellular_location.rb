class CellularLocation < ActiveRecord::Base
  has_many :metabolite_cellular_locations, dependent: :destroy
  has_many :metabolites, through: :metabolite_cellular_locations

  validates :name, presence: true, uniqueness: true, length: { maximum: 255 }

  default_scope -> { order(:name) }
  
  def to_s
    self.name
  end
end
