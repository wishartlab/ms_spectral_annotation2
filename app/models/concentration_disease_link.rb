class ConcentrationDiseaseLink < ActiveRecord::Base
  belongs_to :concentration
  belongs_to :disease
end