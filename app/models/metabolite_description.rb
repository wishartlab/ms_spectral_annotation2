class MetaboliteDescription < ActiveRecord::Base
  belongs_to :metabolite, touch: true
  validates :description, :metabolite_id, :source, :presence => 'true'
end
