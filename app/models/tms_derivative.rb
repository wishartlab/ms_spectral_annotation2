class TmsDerivative < ActiveRecord::Base
  belongs_to :metabolite

  validates :moldb_derivative_mw, presence: true
  validates_uniqueness_of :moldb_derivative_mw, :scope => [ :moldb_derivative_type, :metabolite_id ]
  # validates_uniqueness_of :moldb_derivative_mw, :scope => [ :moldb_derivative_exact_mass ]
  # validates_uniqueness_of :moldb_derivative_exact_mass, :scope => [ :moldb_derivative_type, :metabolite_id ]
end