class Metabolite < ActiveRecord::Base
  belongs_to :source

  validates :database_id, presence: true
  validates :name, presence: true, length: { maximum: 255 }
  validates :source_id, presence: true
  validates_uniqueness_of :database_id, :scope => :source_id

  scope :exported, -> { where(exported: true) }
  
end
