require 'csv'

class ChangeSet < ActiveRecord::Base
  belongs_to :user
  has_and_belongs_to_many :metabolites
  has_attached_file :data

  validates :description, :presence => true, :uniqueness => true
  validates :user, :presence => true
  validates :source, :presence => true, :length => { :maximum => 255 }

  validates_attachment_content_type :data, :message => 'must be in CSV or TSV format', 
    :content_type => [ 'text/comma-separated-values', 'text/csv', 'application/csv', 
      'text/tab-separated-values', 'text/tsv', 'application/tsv' ]

  after_create :associate_metabolites
  
  private
    # Associate the CSV file with the metabolites, based on the first column in the file being the HMDB ID
    def associate_metabolites
      col_sep = self.data.queued_for_write[:original].content_type =~ /csv/ ? ',' : "\t"
      done = {}
      CSV.foreach(self.data.queued_for_write[:original].path, :col_sep => col_sep) do |row|
        if row[0] =~ /\AHMDB\d{7}\Z/
          hmdb_id = row[0]
          next if done[hmdb_id]
          done[hmdb_id] = true
          metabolite = Metabolite.find_by_hmdb_id(hmdb_id)
          if metabolite.present?
            self.metabolites << metabolite
          else
            self.errors.add(:base, "Can't find metabolite with HMDB ID #{hmdb_id} - please run your import first")
            return false
          end
        end
      end
    end
end
