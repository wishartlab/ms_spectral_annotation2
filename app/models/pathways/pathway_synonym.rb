class PathwaySynonym < ActiveRecord::Base
  belongs_to :pathway

  validates :pathway, presence: true
  validates :synonym, presence: true, 
                      uniqueness: { scope: :pathway_id },
                      length: { maximum: 255 }
  
  def to_s
    self.synonym
  end
end
