class PathwayLink < ActiveRecord::Base
  belongs_to :element, :polymorphic => true
  belongs_to :pathway

  validates :element, presence: true
  validates :pathway, presence: true
end