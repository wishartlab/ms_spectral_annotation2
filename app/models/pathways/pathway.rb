class Pathway < ActiveRecord::Base
  has_many :pathway_links
  has_many :metabolites, 
    :through => :pathway_links,
    :source => :element, 
    :source_type => "Metabolite"
  has_many :proteins,
    :through => :pathway_links,
    :source => :element,
    :source_type => "Protein"
  has_many :synonyms, class_name: "PathwaySynonym", dependent: :destroy

  has_many :concentrations, :through => :metabolites
  
  scope :exported, -> { joins(:metabolites).merge(Metabolite.exported).distinct }
  
  default_scope { order("name ASC") }
  
  validates :name, presence: true, length: { maximum: 255 }
  validates :kegg_id, 
            format: { with: /\Amap\d{5}\z/ },
            exclusion: { in: %w(map01100) },
            allow_blank: true
  validates :smpdb_id,
            format: { with: /\ASMP\d{5}\z/ },
            allow_blank: true
  validates :pathwhiz_id,
            format: { with: /\APW\d{6}\z/ },
            allow_blank: true
  validates :unipathway_id,
            format: { with: /\AUPA\d{5}\z/ },
            allow_blank: true
end
