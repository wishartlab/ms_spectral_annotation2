class BiofluidStats
  attr_reader :biofluid

  def initialize(biofluid)
    biofluid = 'blood' if biofluid.downcase == 'serum'
    @biofluid = biofluid
    @biofluid_scope = nil

    @metabolite_scope = Metabolite.exported
  end

  def get_stat(sql)
    ActiveRecord::Base.connection.execute(sql).first.first
  end

  def biofluid_scope
    return @biofluid_scope unless @biofluid_scope.nil?
    @biofluid_scope = Concentration.exported.filter_by_biofluids([@biofluid])
  end

  def total_metabolites_count
    @total_metabolites_count ||= @metabolite_scope.joins(:concentrations).
      merge(biofluid_scope).
      uniq.count
  end

  def total_quantified_count
    @total_quantified_count ||= @metabolite_scope.joins(:concentrations).
      merge(biofluid_scope.filter_by_status([:quantified]) ).
      uniq.count
  end

  def total_detected_count
    @total_detected_count ||= @metabolite_scope.joins(:concentrations).
      merge( biofluid_scope.filter_by_status([:detected]) ).
      uniq.count
  end

  def total_expected_count
    @total_expected_count ||=  @metabolite_scope.joins(:concentrations).
      merge( biofluid_scope.filter_by_status([:expected]) ).
      uniq.count
  end

  def protein_count
    @protein_count ||= get_stat(<<-SQL)
    select count(distinct metabolite_protein_links.protein_id)
    from metabolites inner join concentrations on concentrations.metabolite_id=metabolites.id
    inner join metabolite_protein_links on metabolite_protein_links.metabolite_id = metabolites.id
    where metabolites.export_to_hmdb=1 and concentrations.biofluid_source="#{@biofluid}";
    SQL
  end

  def condition_count
    @condition_count ||= get_stat(<<-SQL)
    select count( distinct conc_condition ) from concentrations
    where biofluid_source="#{@biofluid}" and status="Detected and Quantified"
    and patient_status="Abnormal";
    SQL
  end

  def total_concentration_count
    @total_concentration_count ||=
      biofluid_scope.detected_and_quantified.count
  end

  def total_abnormal_concentration_count
    @total_abnormal_concentration_count ||=
      biofluid_scope.abnormal.detected_and_quantified.count
  end

  def total_normal_concentration_count
    @total_normal_concentration_count ||=
      biofluid_scope.normal.detected_and_quantified.count
  end

  def total_referenced_concentrations
    @total_referenced_concentrations ||=
      biofluid_scope.detected_and_quantified.
        where(source:"Referenced").count
  end

  def endogenous_metabolites_count
    @endogenous_metabolites_count ||= get_stat(<<-SQL)
    select count(distinct metabolites.id)
    from metabolites, concentrations, ontologies
    where metabolites.id=concentrations.metabolite_id
    and metabolites.id=ontologies.metabolite_id and biofluid_source="#{@biofluid}"
    and metabolites.export_to_hmdb=1 and ontologies.origin like "%Endogenous%";
    SQL
  end

  def exogenous_metabolites_count
    @exogenous_metabolites_count ||= get_stat(<<-SQL)
    select count(distinct metabolites.id) from ontologies, metabolites inner join
    concentrations on concentrations.metabolite_id=metabolites.id where
    ontologies.metabolite_id=metabolites.id and metabolites.export_to_hmdb=1
    and concentrations.biofluid_source="Urine" and export_hmdb="Yes"and (origin
    like "%Food%" or origin like "%Microbial%" or origin like "%Drug%" or origin
    like "%Plant%" or origin like "%Toxin%" or origin like "%Cosmetic%" or origin
    like "%Exogenous%")
    SQL
  end

  def quantified_concentration_count
    @quantified_concentration_count ||= biofluid_scope.detected_and_quantified.count
  end

  def chemspider_link_count
    @chemspider_link_count ||=
      @metabolite_scope.includes(:link_set).
      joins(:concentrations).merge( biofluid_scope ).
      where("link_sets.chemspider_id <> ''").
      references(:link_sets).count
  end

  def pubchem_compound_link_count
    @pubchem_compound_link_count ||=
      @metabolite_scope.includes(:link_set).
      joins(:concentrations).merge( biofluid_scope ).
      where("link_sets.pubchem_cid <> ''").
      references(:link_sets).count
  end

  def kegg_compound_link_count
    @kegg_compound_link_count ||=
      @metabolite_scope.includes(:link_set).
      joins(:concentrations).merge( biofluid_scope ).
      where("link_sets.kegg_id <> ''").
      references(:link_sets).count
  end

  def kegg_pathway_link_count
    @kegg_pathway_link_count ||=
      Pathway.joins(:concentrations).merge( biofluid_scope ).
      where("kegg_id <> ''").count
  end

  def chebi_compound_link_count
    @chebi_compound_link_count ||=
      @metabolite_scope.includes(:link_set).
      joins(:concentrations).merge( biofluid_scope ).
      where("link_sets.chebi_id <> ''").
      references(:link_sets).count
  end

  def wikipedia_link_count
    @wikipedia_link_count ||=
      @metabolite_scope.includes(:link_set).
      joins(:concentrations).merge( biofluid_scope ).
      where("link_sets.wiki_id <> ''").
      references(:link_sets).count
  end
end
