class OntologySynonym < ActiveRecord::Base
  belongs_to :ontology_term

  validates :ontology_term_id, presence: true
  validates :synonym, presence: true, uniqueness: true
end
