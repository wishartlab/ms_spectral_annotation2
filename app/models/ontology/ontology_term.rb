class OntologyTerm < ActiveRecord::Base

  # has_many :ontology_term_categories
  belongs_to :parent, :class_name => 'OntologyTerm'
  has_many :children, :class_name => 'OntologyTerm', :foreign_key => 'parent_id'
  has_many :metabolite_ontology_terms
  has_many :metabolites, through: :metabolite_ontology_terms
  has_many :ontology_synonyms

  validates :term, presence: true, uniqueness: true
end
