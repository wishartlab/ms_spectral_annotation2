class OntologyCategory < ActiveRecord::Base
  belongs_to :parent, :class_name => 'OntologyCategory'
  has_many :children, :class_name => 'OntologyCategory', :foreign_key => 'parent_id'

  validates :name, presence: true, uniqueness: true
  validates :level, presence: true

end
