class OntologyTermCategory < ActiveRecord::Base
  belongs_to :ontology_term
  belongs_to :ontology_category

  validates :ontology_term_id, presence: true
  validates :ontology_category_id, presence: true
  validates_uniqueness_of :ontology_term_id, scope: :ontology_category_id
end
