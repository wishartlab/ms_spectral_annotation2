class Tissue < ActiveRecord::Base
  has_many :metabolite_tissues, dependent: :destroy
  has_many :metabolites, through: :metabolite_tissues

  validates :name, presence: true, uniqueness: true, length: { maximum: 255 }

  default_scope { order(:name) }

  def to_s
    self.name
  end
end
