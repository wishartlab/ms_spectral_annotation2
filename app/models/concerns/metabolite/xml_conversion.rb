class Metabolite
  module XmlConversion
    extend ActiveSupport::Concern

    included do
      alias_method :old_to_xml, :to_xml
      alias_method :to_xml, :complete_xml
    end

    def complete_xml
      old_to_xml(:only=>[]) do |xml|
        generate_xml(xml)
      end
    end

    def generate_xml(xml)
      xml.version Rails.application.config.version
      xml.creation_date created_at
      xml.update_date updated_at
      xml.accession hmdb_id
      xml.secondary_accessions do
        accession_numbers.each do |acc|
          xml.accession acc
        end
      end
      xml.name name
      xml.description description
      xml.synonyms do
        synonyms.each do |syn|
          xml.synonym syn
        end
      end
      xml.chemical_formula chemical_formula
      xml.average_molecular_weight average_mass
      xml.monisotopic_molecular_weight mono_mass
      xml.iupac_name iupac
      xml.traditional_iupac traditional_iupac
      xml.cas_registry_number cas
      xml.smiles smiles
      xml.inchi inchi
      xml.inchikey inchikey
      if classyfired?
        xml.taxonomy do
          xml.description classyfirecation.description
          xml.direct_parent classyfirecation.direct_parent_name
          xml.kingdom classyfirecation.kingdom_name
          xml.super_class classyfirecation.superklass_name
          xml.class classyfirecation.klass_name
          xml.sub_class classyfirecation.subklass_name
          xml.molecular_framework classyfirecation.molecular_framework
          xml.alternative_parents do
            classyfirecation.alternative_parent_names.each do |name|
              xml.alternative_parent name
            end
          end
          xml.substituents do
            classyfirecation.substituent_names.each do |s|
              xml.substituent s
            end
          end
        end
      end
      xml.ontology do
        xml.status status
        xml.origins do
          if ontology.present?
            ontology.origins.each do |o|
              xml.origin o
            end
          end
        end
        xml.biofunctions do
          if ontology.present?
            ontology.biofunctions.each do |b|
              xml.biofunction b
            end
          end
        end
        xml.applications do
          if ontology.present?
            ontology.applications.each do |a|
              xml.application a
            end
          end
        end
        xml.cellular_locations do
          metabolite_cellular_locations.each do |c|
            xml.cellular_location c
          end
        end
      end

      xml.state state

      xml.experimental_properties do
        [:water_solubility, :logp, :melting_point, :boiling_point].each do |field|
          if send("experimental_#{field}").present?
            xml.property do
              xml.kind field.to_s
              xml.value send("experimental_#{field}")
              xml.source send("experimental_#{field}_reference")
            end
          end
        end
      end

      xml.predicted_properties do
        [:logp, :logs, :solubility].each do |field|
          if send("moldb_alogps_#{field}").present?
            xml.property do
              xml.kind field.to_s
              xml.value send("moldb_alogps_#{field}")
              xml.source 'ALOGPS'
            end
          end
        end

        [:logp, :pka_strongest_acidic, :pka_strongest_basic, :iupac,
         :average_mass, :mono_mass, :smiles, :formula, :inchi,
         :inchikey, :polar_surface_area, :refractivity, :polarizability,
         :rotatable_bond_count, :acceptor_count, :donor_count,
         :physiological_charge, :formal_charge].each do |field|
          if send("moldb_#{field}").present?
            xml.property do
              xml.kind field.to_s
              if field.in? [:logp, :pka_strongest_acidic, :pka_strongest_basic, :polar_surface_area, :polarizability, :refractivity]
                xml.value send("moldb_#{field}").to_f.smart_round_to_s
              else
                xml.value send("moldb_#{field}")
              end
              xml.source 'ChemAxon'
            end
          end
        end
      end

      xml.spectra do
        spectra.compact.each do |s|
          xml.spectrum do
            xml.type s.class
            xml.spectrum_id s.id
          end
        end
      end

      xml.biofluid_locations do
        biofluid_locations.each do |b|
          xml.biofluid b
        end
      end

      xml.tissue_locations do
        tissues.each do |t|
          xml.tissue t.name
        end
      end

      xml.pathways do
        pathways.each do |p|
          xml.pathway do
            xml.name p.name
            xml.smpdb_id p.smpdb_id
            xml.kegg_map_id p.kegg_id
          end
        end
      end

      xml.normal_concentrations do
        normal_concentrations.each do |c|
          xml.concentration do
            xml.biofluid c.biofluid
            xml.concentration_value c.conc_value
            xml.concentration_units c.conc_unit
            xml.subject_age c.age
            xml.subject_sex c.sex
            xml.subject_condition c.conc_condition
            xml.comment c.comment if c.export_comment?
            xml.references do
              c.articles.each do |article|
                xml.reference do
                  xml.reference_text article.citation
                  xml.pubmed_id article.pubmed_id
                end
              end
              c.textbooks.each do |textbook|
                xml.reference do
                  xml.reference_text textbook.title
                  xml.pubmed_id nil
                end
              end
              c.external_links.each do |external_link|
                xml.reference do
                  xml.reference_text "#{external_link.name}: #{external_link.url}"
                end
              end
            end
          end
        end
      end
      xml.abnormal_concentrations do
        abnormal_concentrations.each do |c|
          xml.concentration do
            xml.biofluid c.biofluid
            xml.concentration_value c.conc_value
            xml.concentration_units c.conc_unit
            xml.patient_age c.age
            xml.patient_sex c.sex
            xml.patient_information c.conc_condition
            xml.comment c.comment if c.export_comment?
            xml.references do
              c.articles.each do |article|
                xml.reference do
                  xml.reference_text article.citation
                  xml.pubmed_id article.pubmed_id
                end
              end
              c.textbooks.each do |textbook|
                xml.reference do
                  xml.reference_text textbook.title
                  xml.pubmed_id nil
                end
              end
              c.external_links.each do |external_link|
                xml.reference do
                  xml.reference_text "#{external_link.name}: #{external_link.url}"
                end
              end
            end
          end
        end
      end

      xml.diseases do
        diseases.each do |d|
          xml.disease do
            xml.name d.name
            xml.omim_id d.omim_id
            xml.references do
              d.articles.each do |a|
                xml.reference do
                  xml.reference_text a.citation
                  xml.pubmed_id a.pubmed_id
                end
              end
              d.textbooks.each do |t|
                xml.reference do
                  xml.reference_text t.title
                  xml.pubmed_id nil
                end
              end
              d.external_links.each do |link|
                xml.reference do
                  xml.reference_text "#{link.name}: #{link.url}"
                end
              end
            end
          end
        end
      end

      xml.drugbank_id link_set&.drugbank_id
      xml.drugbank_metabolite_id link_set&.drugbank_metabolite_id
      xml.phenol_explorer_compound_id link_set&.phenol_compound_id
      xml.phenol_explorer_metabolite_id link_set&.phenol_metabolite_id
      xml.foodb_id link_set&.foodb_id
      xml.knapsack_id link_set&.knapsack_id
      xml.chemspider_id link_set&.chemspider_id
      xml.kegg_id link_set&.kegg_id
      xml.biocyc_id link_set&.biocyc_id
      xml.bigg_id link_set&.bigg_id
      xml.wikipidia link_set&.wiki_id
      xml.nugowiki id
      xml.metagene link_set&.metagene
      xml.metlin_id link_set&.metlin_id
      xml.pubchem_compound_id link_set&.pubchem_cid
      xml.het_id pdb_id
      xml.chebi_id link_set&.chebi_id

      xml.synthesis_reference synthesis_reference

      xml.general_references do
        articles.each do |article|
          xml.reference do
            xml.reference_text article.citation
            xml.pubmed_id article.pubmed_id
          end
        end
        textbooks.each do |textbook|
          xml.reference do
            xml.reference_text textbook.title
          end
        end
        external_links.each do |external_link|
          xml.reference do
            xml.reference_text "#{external_link.name}: #{external_link.url}"
          end
        end
      end

      xml.protein_associations do
        proteins.each do |p|
          xml.protein do
            xml.protein_accession p.hmdbp_id
            xml.name p.name
            xml.uniprot_id p.uniprot_id
            xml.gene_name p.gene_name
            xml.protein_type p.protein_type
          end
        end
      end
    end
  end
end
