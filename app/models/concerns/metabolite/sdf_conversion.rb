class Metabolite
  module SdfConversion
    extend ActiveSupport::Concern

    def to_sdf
      Moldbi::Sdf.new(self).generate do |sdf|
        sdf.tag('HMDB_ID', self.hmdb_id)
        sdf.tag('GENERIC_NAME', self.name)
        sdf.tag('SYNONYMS', self.synonyms.map(&:synonym)) if self.synonyms.present?
      end
    end
  end
end
