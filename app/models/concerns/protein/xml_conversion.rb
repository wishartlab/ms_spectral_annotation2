class Protein
  module XmlConversion
    extend ActiveSupport::Concern

    included do
      alias_method :old_to_xml, :to_xml
      alias_method :to_xml, :complete_xml
    end

    def complete_xml
      self.old_to_xml(:only=>[]) do |xml|
        generate_xml(xml)
      end
    end

    def generate_xml(xml)
      xml.version Rails.application.config.version
      xml.creation_date self.created_at
      xml.update_date self.updated_at
      xml.accession self.hmdbp_id
      xml.secondary_accessions do
        self.accession_numbers.each do |acc|
          xml.accession acc
        end
      end
      xml.protein_type self.protein_type
      xml.synonyms do
        self.synonyms.each do |s|
          xml.synonym s
        end
      end
      xml.gene_name self.gene_name
      xml.general_function self.general_function
      xml.specific_function self.specific_function
      xml.pathways do
        self.pathways.each do |p|
          xml.pathway do
            xml.name p.name
            xml.smpdb_id p.smpdb_id
            xml.kegg_map_id p.kegg_id
          end
        end
      end
      xml.metabolite_associations do
        self.exported_metabolite_protein_links.map(&:metabolite).uniq.each do |m|
          xml.metabolite do
            xml.accession m.hmdb_id
            xml.name m.name
          end
        end
      end
      xml.go_classifications do
        self.go_classes.each do |go_class|
          xml.go_class do
            xml.category go_class.category
            xml.description go_class.description
            xml.go_id go_class.go_id
          end
        end
      end
      xml.subcellular_locations do
        self.subcellular_locations.each do |l|
          xml.subcellular_location l.name
        end
      end
      xml.gene_properties do
        xml.chromosome_location self.chromosome_location
        xml.locus self.locus
        xml.gene_sequence self.gene_sequence&.fasta
      end
      xml.protein_properties do
        xml.residue_number self.num_residues
        xml.molecular_weight self.molecular_weight
        xml.theoretical_pi self.theoretical_pi
        xml.pfams do
          self.pfams.each do |p|
            xml.pfam do
              xml.name p.name
              xml.pfam_id p.identifier
            end
          end
        end
        xml.transmembrane_regions do
          self.transmembrane_regions.each do |t|
            xml.region t
          end
        end

        xml.signal_regions do
          self.signal_regions.each do |s|
            xml.region s
          end
        end
        xml.polypeptide_sequence self.polypeptide_sequence&.fasta
      end
      xml.genbank_protein_id self.genbank_protein_id
      xml.uniprot_id self.uniprot_id
      xml.uniprot_name self.uniprot_name
      xml.pdb_ids do
        self.pdb_id_list.each do |pdb_id|
          xml.pdb_id pdb_id
        end
      end
      xml.genbank_gene_id self.genbank_gene_id
      xml.genecard_id self.genecard_id
      xml.geneatlas_id self.geneatlas_id
      xml.hgnc_id self.hgnc_id
      xml.general_references do
        self.articles.each do |article|
          xml.reference do
            xml.reference_text article.citation
            xml.pubmed_id article.pubmed_id
          end
        end
        self.textbooks.each do |textbook|
          xml.reference do
            xml.reference_text textbook.title
          end
        end
        self.external_links.each do |external_link|
          xml.reference do
            xml.reference_text "#{external_link.name}: #{external_link.url}"
          end
        end
      end
      xml.metabolite_references do
        self.exported_metabolite_protein_links.each do |me|
          me.articles.each do |article|
            xml.metabolite_reference do
              xml.metabolite do
                xml.name me.metabolite&.name
                xml.accession me.metabolite&.hmdb_id
              end

              xml.reference do
                xml.reference_text article.citation
                xml.pubmed_id article.pubmed_id
              end
            end
          end
          me.textbooks.each do |textbook|
            xml.metabolite_reference do
              xml.metabolite do
                xml.name me.metabolite&.name
                xml.accession me.metabolite&.hmdb_id
              end

              xml.reference do
                xml.reference_text textbook.title
              end
            end
          end
          me.external_links.each do |external_link|
            xml.metabolite_reference do
              xml.metabolite do
                xml.name me.metabolite&.name
                xml.accession me.metabolite&.hmdb_id
              end

              xml.reference do
                xml.reference_text "#{external_link.name}: #{external_link.url}"
              end
            end
          end
        end
      end
    end
  end
end
