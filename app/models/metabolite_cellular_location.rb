class MetaboliteCellularLocation < ActiveRecord::Base
  include CiteThis::Referencer

  belongs_to :metabolite, touch: true
  belongs_to :cellular_location

  validates :metabolite, :cellular_location, presence: true
  validates :cellular_location_id, uniqueness: { scope: :metabolite_id }

  def to_s
    if self.predicted_from_logp?
      "#{self.cellular_location.name} (predicted from logP)"
    else
      self.cellular_location.name
    end
  end
end
