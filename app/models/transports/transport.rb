class Transport < ActiveRecord::Base
  has_many :transport_metabolites, :dependent => :destroy
  has_many :transport_transporters, :dependent => :destroy
  has_many :proteins, :through => :transport_transporters
  
  validates :direction, :ecocyc_id, :kegg_id, :from, :to, length: { maximum: 255 }
  validates :altext, presence: true, length: { maximum: 255 }
end