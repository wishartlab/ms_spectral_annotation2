class TransportMetabolite < ActiveRecord::Base
  belongs_to :transport
  belongs_to :metabolite, touch: true

  validates :transport, :metabolite, presence: true
end