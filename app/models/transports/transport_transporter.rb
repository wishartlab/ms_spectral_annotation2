class TransportTransporter < ActiveRecord::Base
  belongs_to :transport
  belongs_to :protein

  validates :transport, :protein, presence: true
end