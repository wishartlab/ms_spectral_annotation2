class Synonym < ActiveRecord::Base
  belongs_to :metabolite, touch: true
  validates :synonym, presence: true, 
                      uniqueness: { scope: :metabolite_id }, 
                      length: { maximum: 1000 }
  validates :metabolite, presence: true

  def to_s
    self.synonym
  end
end
