class PredictedConcentration < ActiveRecord::Base
  ALLOWED_BIOFLUIDS = ['Blood', 'Urine'].freeze
  ALLOWED_SEXES = [ 'Not Specified', 'Male', 'Female', 'Both' ].freeze
  ALLOWED_AGES =  [ 'Not Specified', 'Newborn (0-30 days old)', 'Infant (0-1 year old)',
                    'Children (1-13 year old)', 'Adolescent (13-18 years old)',
                    'Adult (>18 years old)', 'Adult (60 years old)', 'Adult (40 years old)',
                    'Elderly (>65 years old)' ].freeze

  belongs_to :metabolite, touch: true

  validates :biofluid, inclusion: { in: ALLOWED_BIOFLUIDS }
  validates :value, presence: true, length: { maximum: 255 }
  validates :units, presence: true, length: { maximum: 255 }
  validates :original_sex, inclusion: { in: ALLOWED_SEXES }, allow_blank: true
  validates :original_age, inclusion: { in: ALLOWED_AGES }, allow_blank: true
  validates :original_conditions, presence: true
end
