class Concentration < ActiveRecord::Base
  include CiteThis::Referencer

  # Patient status constants
  NORMAL           = 'Normal'.freeze
  ABNORMAL         = 'Abnormal'.freeze
  DISEASE          = 'Disease'.freeze
  PATIENT_STATUSES = [NORMAL, ABNORMAL, DISEASE].freeze

  # Detection status constants
  DETECTED_AND_QUANTIFIED = 'Detected and Quantified'.freeze
  DETECTED_NOT_QUANTIFIED = 'Detected but not Quantified'.freeze
  EXPECTED_NOT_QUANTIFIED = 'Expected but not Quantified'.freeze
  ALLOWED_STATUSES        = [DETECTED_AND_QUANTIFIED,
                             DETECTED_NOT_QUANTIFIED,
                             EXPECTED_NOT_QUANTIFIED].freeze

  # Source constants
  HMP_SOURCE        = 'HMP'.freeze
  REFERENCE_SOURCE  = 'Referenced'.freeze
  ALLOWED_SOURCES   = [HMP_SOURCE, REFERENCE_SOURCE].freeze

  # Biofluids (TODO: move into model)
  ALLOWED_BIOFLUIDS = ['Urine', 'Saliva', 'Cerebrospinal Fluid (CSF)', 'Blood',
                       'Bile', 'Cellular Cytoplasm', 'Amniotic Fluid', 'Semen',
                       'Breast Milk', 'Pericardial Effusion', 'Ascites Fluid',
                       'Aqueous Humour', 'Sweat', 'Tears', 'Lymph',
                       'Prostate Tissue', 'Feces'].freeze

  # Patient condition constants
  UNITS = ['uM', 'umol/mmol creatinine', 'nmol/mmol creatinine',
           'umol/mmol', 'umol/g tissue', 'g/day', 'umol/day',
           'umol/day/kg of weight',
           'mol/day/m² of skin', 'nmol/g of feces', 'ppm'].freeze
  AGES =  ['Not Specified', 'Newborn (0-30 days old)', 'Infant (0-1 year old)',
           'Children (1-13 years old)', 'Adolescent (13-18 years old)', 'A few days - 30 years old', 'Adult ',
           'Adult (>18 years old)', 'Adult (24-38years old)', 'Adult (60 years old)', 'Adult (40 years old)',
           'Elderly (>65 years old)', 'Adult (25-30 years old)'].freeze
  SEXES = ['Not Specified', 'Male', 'Female', 'Both'].freeze

  EXPORT_STATES = ["Yes", "No"].freeze

  attr_accessor :metabolite_hmdb_id # Used by LIMS

  belongs_to :metabolite, touch: true

  has_many :concentration_disease_links, dependent: :destroy
  accepts_nested_attributes_for :concentration_disease_links, allow_destroy: true
  has_many :diseases, through: :concentration_disease_links

  scope :exported, -> { where(export_hmdb: 'Yes').order(:biofluid_source) }

  scope :normal, -> { exported.where(patient_status: NORMAL) }
  scope :abnormal, -> { exported.where(patient_status: ABNORMAL) }
  scope :disease, -> { exported.where(patient_status: DISEASE) }

  scope :referenced, -> { where(source: REFERENCE_SOURCE) }
  scope :hmp, -> { where(source: HMP_SOURCE) }

  scope :detected_and_quantified, -> { where(status: DETECTED_AND_QUANTIFIED) }
  scope :detected_not_quantified, -> { where(status: DETECTED_NOT_QUANTIFIED) }
  scope :expected_not_quantified, -> { where(status: EXPECTED_NOT_QUANTIFIED) }

  # validates :age, inclusion: { in: AGES }, allow_blank: true
  validates :age,
    format: { with: /(^Infant )|(^Adult )|(^Adolescent )|(^Newborn )|(^Children )|(^Not Specified)/,
              message: "Age group must be infant, adult, adolescent, newborn or children or Not Specified" }
  validates :sex, inclusion: { in: SEXES }, allow_blank: true
  validates :patient_status, presence: true, inclusion: { in: PATIENT_STATUSES }
  validates :conc_unit, inclusion: { in: UNITS }, allow_blank: true
  validates :source, inclusion: { in: ALLOWED_SOURCES }, presence: true
  validates :biofluid_source, presence: true, inclusion: { in: ALLOWED_BIOFLUIDS }
  validates :status, inclusion: { in: ALLOWED_STATUSES }
  validates :export_comment, inclusion: { in: EXPORT_STATES }
  validates :export_hmdb, inclusion: { in: EXPORT_STATES }
  validates :expected_source, length: { maximum: 255 }
  validates :metabolite, presence: true
  validates :conc_condition,
            presence: { if: Proc.new { |c| c.abnormal? },
            message: "can't be blank when patient status is abnormal" }
  validates :conc_value, :uniqueness => {:scope => [:metabolite_id, :conc_unit, :age, :sex, :biofluid_source]}

  validate :status_valid
  validate :units_valid
  validate :patient_status_valid
  validate :creatinine_units_valid
  validate :hmdb_id_valid
  # validate :referenced_has_reference
  validate :has_reference

  before_validation :set_metabolite_id, on: :create
  after_commit { self.metabolite.set_status! }

  # Aliases
  alias_attribute :condition, :conc_condition
  alias_attribute :biofluid, :biofluid_source
  alias_attribute :units, :conc_unit

  def to_s
    "#{self.metabolite.hmdb_id} - Concentration #{self.id}"
  end

  # Allows us to set the metabolite via HMDB ID
  def metabolite_hmdb_id
    @metabolite_hmdb_id || self.metabolite.try(:hmdb_id)
  end

  def detected_and_quantified?
    self.status == DETECTED_AND_QUANTIFIED
  end

  def detected_not_quantified?
    self.status == DETECTED_NOT_QUANTIFIED
  end

  def expected_not_quantified?
    self.status == EXPECTED_NOT_QUANTIFIED
  end

  def hmp_source?
    self.source == HMP_SOURCE
  end

  def reference_source?
    self.source == REFERENCE_SOURCE
  end

  def export_comment?
    self.export_comment.to_s == 'Yes'
  end

  def export_to_hmdb?
    self.export_hmdb == 'Yes'
  end

  def normal?
    self.patient_status.to_s == NORMAL
  end

  def abnormal?
    self.patient_status.to_s == ABNORMAL
  end

  def disease?
    self.patient_status.to_s == DISEASE
  end

  def first_reference
    if self.articles.present?
      self.articles.first
    elsif self.textbooks.present?
      self.textbooks.first
    elsif self.external_links.present?
      self.external_links.first
    else
      nil
    end
  end

  def self.biofluids
    ALLOWED_BIOFLUIDS.sort
  end

  def self.units
    UNITS
  end

  def self.sources
    ALLOWED_SOURCES
  end

  def self.patient_statuses
    PATIENT_STATUSES
  end

  def self.sexes
    SEXES
  end

  def self.ages
    AGES
  end

  def self.statuses
    ALLOWED_STATUSES
  end

  BIOFLUID_FILTER_MAPPINGS = {
    :blood  => "'Blood'",
    :urine  => "'Urine'",
    :csf    => "'Cerebrospinal Fluid (CSF)'",
    :saliva => "'Saliva'",
    :feces  => "'Feces'",
    :sweat  => "'Sweat'",
    :breast_milk => "'Breast Milk'",
    :bile => "'Bile'",
    :amniotic_fluid => "'Amniotic Fluid'",
    :cellular_cytoplasm => "'Cellular Cytoplasm'"
  }.freeze

  BIOFLUID_FILTERS = [:blood, :urine, :csf, :saliva, :feces, :sweat, :breast_milk, 
    :bile, :amniotic_fluid, :other_fluids, :all].freeze

  # Filters the concentrations to only the specified filters
  # in an array. Only filters from BIOFLUID_FILTERS are allowed
  def self.filter_by_biofluids(filters)
    # Use only allowed fluids
    biofluids = filters.uniq.map(&:to_sym) & BIOFLUID_FILTERS
    if filters.blank? || filters.include?(:all)
      return self.current_scope
    end

    my_scope = self.current_scope || self

    specific_fluids = biofluids.map do |fluid|
      BIOFLUID_FILTER_MAPPINGS[fluid]
    end.compact

    filter_conditions = Array.new
    unless specific_fluids.empty?
      filter_conditions << "( #{self.table_name}.biofluid_source IN ( #{specific_fluids.join(", ")} ) )"
    end

    if filters.include?(:other_fluids)
      filter_conditions << "( #{self.table_name}.biofluid_source NOT IN ( #{BIOFLUID_FILTER_MAPPINGS.values.join(", ")} ) )"
      #<<-SQL
      #  ( #{self.table_name}.biofluid_source NOT IN
      #    ( #{ BIOFLUID_FILTER_MAPPINGS.values.join(", ") } ) )
      #SQL
    end
    my_scope.where( "( #{filter_conditions.join(' OR ')} )" )
  end

  def self.filter_by_status(filters)
    myscope = current_scope || all
    return myscope if filters.blank?

    statuses = filters.uniq.map do |filter|
      case filter.to_sym
      when :expected   then EXPECTED_NOT_QUANTIFIED
      when :quantified then DETECTED_AND_QUANTIFIED
      when :detected   then DETECTED_NOT_QUANTIFIED
      else
        raise "Invalid metabolite status filter. (#{filter})"
      end
    end

    myscope.where(status: statuses)
  end

  protected

    # Abnormal and normal have a conc_condition and cannot have disease associated
    # Disease cannot have a conc_condition and must have a disease associated
    def patient_status_valid
      # if self.disease?
      #   if self.conc_condition.present?
      #     self.errors[:conc_condition] << "cannot be present for disease " \
      #                                     "concentration"
      #   end
      # else
      if self.normal? && self.diseases.present?
        self.errors[:base] << "cannot have a disease associated with " \
                              "normal concentration"
      end
    end

    def status_valid
      if self.detected_and_quantified?
        if self.conc_value.blank? || self.conc_unit.blank?
          self.errors[:base] << "Missing value or unit when " \
                                "detected and quantified"
        end
      elsif self.detected_not_quantified? || self.expected_not_quantified?
        if self.conc_value.present? || self.conc_unit.present?
          self.errors[:base] << "Value or unit set when detected " \
                                "but NOT quantified"
        end
      end

      if self.expected_not_quantified? && self.expected_source.blank?
        self.errors[:expected_source] << 'must be present for an ' \
                                         'expected concentration'
      end

      if !self.expected_not_quantified? && self.expected_source.present?
        self.errors[:expected_source] << 'must NOT be present unless the ' \
                                         'concentration is expected'
      end
    end

    def units_valid
      if self.detected_and_quantified?
        if self.biofluid_source == 'Blood' && self.units != 'uM'
          self.errors[:conc_unit] << 'Invalid units in blood'
        elsif self.biofluid_source == 'Urine' && self.units !~ /[un]mol\/mmol creatinine/
          self.errors[:conc_unit] << 'Invalid units in urine'
        end
      end
    end

    # Creatinine is the only metabolite that should always have the concentration uM
    # in urine.
    def creatinine_units_valid
      metabolite = self.metabolite || Metabolite.where(id: self.metabolite_id)

      if self.detected_and_quantified? && self.biofluid_source == 'Urine'
        if metabolite.present? && metabolite.name.downcase == 'creatinine'
          self.errors.delete(:conc_unit)
          if self.units != 'uM'
            self.errors[:conc_unit] << 'Invalid units for creatinine in ' \
                                       'urine - can only be uM'
          end
        end
      end
    end

    def hmdb_id_valid
      if self.metabolite_hmdb_id.present? && self.metabolite_id.blank?
        if self.metabolite_hmdb_id !~ /\AHMDB\d{7}\z/
          self.errors[:metabolite_hmdb_id] << "is invalid"
        elsif Metabolite.find_by_hmdb_id(self.metabolite_hmdb_id).blank?
          self.errors[:metabolite_hmdb_id] << "does not exist"
        end
      end
    end

    # def referenced_has_reference
    #   if self.reference_source?
    #     if self.article_text_references.blank? &&
    #         self.textbook_referencings.blank? &&
    #         self.external_link_referencings.blank?
    #       self.errors[:base] <<
    #         "Referenced concentration must have at least one article, " \
    #         "textbook or external link"
    #     end
    #   end
    # end

    def has_reference
      if self.reference_source?
        if !self.articles.present? && !self.textbooks.present? && !self.external_links.present?
          self.errors[:base] <<
            "Referenced concentration must have at least one article, " \
            "textbook or external link"
        end
      end
    end

    # Allows the association with metabolite to be set via the HMDB ID on create
    def set_metabolite_id
      if self.metabolite_hmdb_id.present? && !self.persisted?
        metabolite = Metabolite.find_by_hmdb_id(self.metabolite_hmdb_id)
        self.metabolite_id = metabolite.id if metabolite.present?
      end
    end
end
