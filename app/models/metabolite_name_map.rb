class MetaboliteNameMap < ActiveRecord::Base
  belongs_to :metabolite, touch: true
  
  validates :inchi, uniqueness: true, allow_blank: false
end
