class ConcentrationReference < ActiveRecord::Base
  belongs_to :reference
  belongs_to :concentration
end
