class MetaboliteOntologyTerm < ActiveRecord::Base
  include CiteThis::Referencer
  
  belongs_to :metabolite
  # belongs_to :ontology_term_category
  belongs_to :ontology_term

  validates :metabolite_id, presence: true
  # validates :ontology_term_category_id, presence: true
  validates :ontology_term_id, presence: true
  # validates_uniqueness_of :metabolite_id, scope: :ontology_term_category_id
  validates_uniqueness_of :metabolite_id, scope: :ontology_term_id
end
