//= require active_admin/base
//= require select2
//= require moldbi/moldbi

$ ->
  $('.remote-selector').each ->
    add_remote_autocomplete($(this))

$ ->
  $(".has_many_container").on "has_many_add:after", (item, parent) ->
    add_remote_autocomplete($(parent).find(".remote-selector"))

@add_remote_autocomplete = (select) ->
  return unless select.length
  select.select2
    placeholder: $(this).attr('placeholder')
    minimumInputLength: select.data('min-length') || 3
    ajax:
      quietMillis: 100
      url: -> $(this).data('source') + ".json"
      dataType: 'json'
      data: (term, page) ->
        { q: term }
      results: (data, page) -> # parse the results into the format expected by Select2.
        { results: $.map( data, (item) -> # Return a JSON structure with the label to display and the ID
          { text: item.label, id: item.id } ) }
    initSelection: (element, callback) ->
      # If the object is already selected, we need to set the value in the select box
      id = $(element).val()
      if id != ""
        data = { id: id, text: $(element).data('label') }
        callback(data)