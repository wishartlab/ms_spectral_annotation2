# Load any reference tooltips
$ ->
  $(".reference-icon-link").tooltip()

# Load any tip tooltips
$ ->
  $(".tips").tooltip()

$ ->
  # $('.datatable').dataTable()

  # Customize datatables to change the look
  $('.dataTables_filter label').each ->
    searcher = $(this).find('input').clone(true)
    $(this).html('')
    searcher.appendTo($(this))

  $(".dataTables_filter input").attr('placeholder', 'Search')

$ ->
    $("table").tooltip()

# reset modals loaded from remote when hidden
# allows us to re-use modals
$ ->
  $(document).on 'hidden.bs.modal', (e) ->
    $(e.target).removeData('bs.modal')

$ ->
  # Resize header links font size on window resize
  # The quickfit tolerance param make the text look too small locally but bigger
  # in production, I don't know why...
  $(window).resize () ->
    $('.link-bar.one .link-text').quickfit({max: "60", tolerance: 0.46})
    $('.link-bar.two .link-text').css('font-size', $('.link-bar.one .link-text').css('font-size'))

  $(window).load () ->
    $('.link-bar.one .link-text').quickfit({max: "60", tolerance: 0.46})
    $('.link-bar.two .link-text').css('font-size', $('.link-bar.one .link-text').css('font-size'))

    $('.link-bar.one').animate({
      right: '0%'
    }, 2000)

    $('.link-bar.two').animate({
      right: '0%'
    }, 2000)

  # Add show loader class to links for MetaboCards using a regex on the url
  # Regex is specific to avoid links to .xml files, etc.
  $("a").filter( ->
    @href.match /metabolites\/HMDB\d{7}$/
  ).addClass('show-loader')

  $('.search-loader').addClass('show-loader')
  $('.search-loader-moldbi').addClass('show-loader')

  # Show loading gif when clicking on specific links, only if we are NOT holding down a key
  # (i.e. command-click opening a new window)
  $('.show-loader').click (e) ->
    if !e.metaKey
      $('#load-screen').show()

  # Make sure we trun off the loading gif when you use back links
  $(document).on "page:change", ->
    $('#load-screen').hide()

# Enable popovers on ontology terms
$ ->
  # if $('.term-ontnode').length
  $('.term-ontnode').tooltip({
    html: true
  });
$ ->
  # if $('.category-ontnode').length
  $('.category-ontnode').tooltip()
$ ->
  # if $('.main-ontnode').length
  $('.main-ontnode').tooltip()

$ ->
  $(".met-description").truncate max_length: 2000