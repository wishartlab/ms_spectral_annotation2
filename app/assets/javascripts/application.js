// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require wishart
//= require seq_search/seq_search
//= require moldbi/moldbi
//= require specdb/application
//= require unearth/application

// Application JS
//= require hmdb
//= require metabolites
//= require diseases
//= require jquery.quickfit
//= require jquery.truncator

// Always load turbolinks last
//= require jquery.turbolinks
//= require_tree

// This file is automatically included by javascript_include_tag :defaults