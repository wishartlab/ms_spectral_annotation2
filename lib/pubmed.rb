# NCBI announces that queries without email address will return error
# after June 2010. When you modify the script, please enter your email
# address instead of the staff's.
# Bio::NCBI.default_email = 'staff@bioruby.org'

module PubMed
  Bio::NCBI.default_email = "app@wishartlab.com"
  
  # Download and annotate the given pubmed_id and return a citation in textile format.
  # Example Citation: 
  # Smythe MA, Stephens JL, Koerber JM, Mattson JC: A comparison of lepirudin and argatroban outcomes. 
  # Clin Appl Thromb Hemost. 2005 Oct;11(4):371-4. "Pubmed":http://www.ncbi.nlm.nih.gov/pubmed/16244762
  def PubMed.annotate_to_textile(pubmed_id)
    raise ArgumentError unless pubmed_id.to_i > 0
    citation = ''
    result = Bio::PubMed.efetch(pubmed_id)
    raise ArgumentError if result.size <= 0 || result.first =~ /Error occurred/ 
    medline = Bio::MEDLINE.new(result.first)

    if medline.au && !( medline.au.strip.nil? || medline.au.strip == '')
      citation = medline.au.gsub(/\n/, ', ') + ': ' + medline.title + ' ' + medline.source
    elsif !(medline.nil? || medline == '')
      citation = medline.title + ' ' + medline.source
    else
      raise ArgumentError
    end

    citation.gsub!(/\n/, ' ')
    citation.gsub!(/\s{2,}/, ' ')
    citation = "# #{citation} \"Pubmed\":http://www.ncbi.nlm.nih.gov/pubmed/#{pubmed_id}"
  end

  # Download and annotate the given pubmed_id and return a citation in textile format.
  # Example Citation: 
  # Smythe MA, Stephens JL, Koerber JM, Mattson JC: A comparison of lepirudin and argatroban outcomes. 
  # Clin Appl Thromb Hemost. 2005 Oct;11(4):371-4.
  def PubMed.citation(pubmed_id)
    raise ArgumentError unless pubmed_id.to_i > 0
    citation = ''
    result = Bio::PubMed.efetch(pubmed_id)
    raise ArgumentError if result.size <= 0 || result.first =~ /Error occurred/ 
    medline = Bio::MEDLINE.new(result.first)

    if medline.au && !( medline.au.strip.nil? || medline.au.strip == '')
      citation = medline.au.gsub(/\n/, ', ') + ': ' + medline.title + ' ' + medline.source
    elsif !(medline.nil? || medline == '')
      citation = medline.title + ' ' + medline.source
    else
      raise ArgumentError
    end

    citation.gsub!(/\n/, ' ')
    citation.gsub!(/\s{2,}/, ' ')
    citation
  end

  # Returns FirstAuthor_Year (or nil if no author found) from the given pubmed_id
  def PubMed.find_author_year(pubmed_id)
    raise ArgumentError unless pubmed_id.to_i > 0
    author_year = ''
    result = Bio::PubMed.pmfetch(pubmed_id)
    raise ArgumentError if result =~ /Error occurred/
    medline = Bio::MEDLINE.new(result)
    
    if medline.authors.first
      author_year = medline.authors.first.gsub(/\n/, ', ').gsub(/,*\s\w*\./,'') + ' (' + medline.year + ')'
    end
  end

  # Returns an array of pubmed_ids from the given publication title
  def PubMed.find_pmid_from_title(title)
    pmids = Bio::PubMed.esearch(title, 'field' => 'titl', 'retmax' => 5)
    unless pmids.empty?
      pmids.collect! { |pmid| output = "#{title}\t#{pmid}" }
    else
      output = "#{title}\tNO RESULT"
    end
  end

end