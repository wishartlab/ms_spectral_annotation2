# require "json"

namespace :parse_kegg_enzymes do
  task :get_ec => [:environment] do

    # A KEGG
    # protein_hash = Hash.new{ |hash, key| hash[key] =  Array.new }
    # output = File.open("ec_mappings.csv", "w")

    # # Search for Homo sapiens
    # proteins = open("http://rest.kegg.jp/conv/T01001/uniprot").read
    # proteins.split("\n").each do |prot_line|
    #   prot_split = prot_line.gsub(/\s+/m, ' ').split(" ")

    #   uniprot = prot_split[0].split(":")[1]
    #   gene = prot_split[1]

    #   # sequence = open("http://rest.kegg.jp/get/#{gene}/aaseq").read
    #   # kegg_seq.write(">"+uniprot+"\n"+sequence.split("\n")[1..-1].join("\n")+"\n")
    #   # puts gene
    #   enzymes = open("http://rest.kegg.jp/link/enzyme/#{gene}").read
    #   next if enzymes.empty?
    #   next if !enzymes.include?("ec")

    #   puts uniprot
    #   enzymes.split("\n").each do |enzyme|
    #     ec_num = enzyme.gsub(/\s+/m, ' ').split(" ")[1]
    #     protein_hash[ec_num].push(uniprot)
    #     puts ec_num

    #     # ec_num.gsub(" ", "").split(",").each do |ec|
    #     #   compounds = open("http://rest.kegg.jp/link/compound/#{ec_num}").read
    #     #   compounds.split("\n").each do |comp_line|

    #     #     kegg = comp_line.gsub(/\s+/m, ' ').split(" ")[1].split(":")[1]
    #     #     kegg_links.write("\""+uniprot+"\",\""+kegg+"\"\n")
    #     #   end
    #     # end
    #   end

    # end

    # protein_hash.each do |ec, uniprot_ids|
    #   output.puts ec+","+uniprot_ids.join(",")
    # end
    # output.close

    # B UNIPROT

    # output = File.open("ec_uniprot_mapping_yannick.tsv","w")
    output = File.open("ec_uniprot_mapping_kevin.tsv","w")
    ec_mappings = Hash.new{ |hash, key| hash[key] = Hash.new }
    ec_kegg_mappings = Hash.new{ |hash, key| hash[key] = Array.new }

    File.open("ec_mappings.csv").each_line do |line|
      line.strip!
      split_line = line.split(",")
      ec_kegg_mappings[split_line[0].gsub("ec:","")] = split_line[1..-1]
    end

    output.puts("\n")
    ec_numbers = File.read("/Users/AnaMarcu/Documents/ec_numbers_kevin.txt").split(",")
    # ec_numbers = File.read("/Users/AnaMarcu/Documents/ec_numbers_yannick.txt").split(",")
    ec_numbers.each do |ec|
      # puts "http://www.uniprot.org/uniprot/?query=ec%3A#{ec.gsub("EC:","")}+organism%3A9606&sort=score"
      # response = open("http://www.uniprot.org/uniprot/?query=ec%#{ec.gsub("EC:","")}+organism%3A9606&sort=score")
      # puts response

      if ec.include?("EC")
        type = "EC"
      elsif ec.include?("TCDB")
        type = "TCDB"
      elsif ec.include?("TC-")
        type = "TC-"
      else
        type = ""
      end
      ec = ec.gsub("EC:","").gsub(" ","")
      ec = ec.gsub("TCDB:","").gsub(" ","")
      ec = ec.gsub("TC-","").gsub(" ","")
      # puts "http://www.uniprot.org/uniprot/?query=ec%3A#{ec}+organism%3A9606&sort=score&fil=&format=fasta&force=yes"
      puts ec

      begin
        open("http://www.uniprot.org/uniprot/?query=ec%3A#{ec}+organism%3A9606&sort=score&fil=&format=fasta&force=yes") {|f|
          f.each_line do |line|
            # puts line
            if line[0] == ">"
              uniprot = line.split("|")[1]
              meta_data = line.split("|")[2].strip
              # puts uniprot
              # puts meta_data

              ec_mappings[ec][uniprot]=meta_data
            end
          end
        }
      rescue
        puts ec + " NOT Uniprot"
      end

      if type == "TC-"
        ec_data = type+ec+" (Uniprot):"
      elsif !type.empty?
        ec_data = type+":"+ec+" (Uniprot):"
      else
        ec_data = ec+" (Uniprot):"
      end
      ec_mappings[ec].each do |uniprot_id, desc|
        ec_data += "\t"+uniprot_id+"("+desc+")"
      end

      ec_data += "\tADDITIONAL (Kegg):"
      ec_kegg_mappings[ec].each do |id|
        next if ec_mappings[ec].keys.include?(id)
        ec_data += "\t"+id

        begin
          uniprot = Nokogiri::XML(open("http://www.uniprot.org/uniprot/#{id}.xml"))
        rescue
          puts ec + " NOT Uniprot"
        end
        uniprot.remove_namespaces!
        name_element = uniprot.xpath("//protein/recommendedName/fullName").first
        if name_element
          ec_data += "("+name_element.content+")"
          # puts name_element.content
        end
      end

      output.puts(ec_data)
      # break

    end
    output.close
    # puts ec_mappings
  end

  # task :compare_ec => [:environment] do
  #   ec_numbers = Array.new
  #   overlap = Array.new
  #   ec_mappings = Hash.new{ |hash, key| hash[key] =  Array.new }

  #   File.open("/Users/AnaMarcu/Documents/EC_numbers.csv", "r").each_line do |ec|
  #     ec.strip!
  #     ec_numbers.push(ec.downcase) if !ec_numbers.include?(ec.downcase)
  #   end

  #   File.open("ec_mappings.csv", "r").each_line do |ec|
  #     ec.strip!
  #     ec_mappings[ec.split(",")[0].downcase] = ec.split(",")[1..-1]
  #   end

  #   exact_count = 0
  #   multiple_count = 0

  #   ec_numbers.each do |ec|
  #     overlap.push(ec) if ec_mappings.keys.include?(ec)
  #     if ec_mappings.keys.include?(ec)
  #       if ec_mappings[ec].count == 1
  #         exact_count += 1
  #         # puts ec
  #         # puts ec_mappings[ec]
  #         # puts ec_mappings[ec].count
  #       else
  #         multiple_count += 1
  #       end
  #     end
  #   end

  #   puts ec_mappings.keys.count
  #   puts ec_numbers.count
  #   puts overlap.count
  #   puts exact_count
  #   puts multiple_count

  # end

  task :get_compound_names => [:environment] do
    
    # characters = Array.new
    file = File.read("/Users/AnaMarcu/Downloads/compoundsSep13.json")
    # file.split("").each do |char|
    #   next if char == " "
    #   if !characters.include?(char.downcase)
    #     characters.push(char.downcase)
    #     puts char.downcase
    #   end
    # end

    data = JSON.parse(file)["compounds"]
    data.keys.each do |k|
      puts k
    end
    # puts "GOT"
    # puts data.keys
    # puts data["InChIKey=LRCYZCCKRIVTHN-UHFFFAOYSA-N"]

    # file_data.keys
    # file_data.keys.each do |cmpd|
    #   puts cmpd if !cmpd["NAME"].empty?
    # end

    # inchikey = "LRCYZCCKRIVTHN-UHFFFAOYSA-N"
    # inchi = "InChI=1S/C20H20O6/c1-20(9-25-20)17(23)7-13-15(22)5-4-11-14-8-24-16-6-10(21)2-3-12(16)19(14)26-18(11)13/h2-6,14,17,19,21-23H,7-9H2,1H3"

    # compound_data = DataWrangler::Annotate::Compound.annotate_by_inchikey(inchikey)
    # puts compound_data.name

    # data = DataWrangler::JChem::Convert.inchi_to_name(inchi)
    # puts data

    # new_m = Metabolite.new
    # new_m.name = "Test Predicted 17"
    # new_m.predicted_in_hmdb = 1
    # # new_m.status = Metabolite.statuses["predicted"]
    # new_m.export_to_hmdb = 1
    # new_m.save!

    # puts new_m.hmdb_id

    # new_r = Reaction.new
    # new_r.altext = "A 3-beta-hydroxy-Delta(5)-steroid + NAD(+) = a 3-oxo-Delta(5)-steroid + NADH"
    # new_r.direction = "unknown"
    # new_r.export = 1
    # new_r.predicted_in_hmdb = 1
    # new_r.save!
    # puts new_r.id

    # puts "ALL CHARS"
    # puts characters

  end

end
