namespace :blast do
  SEQUENCE_PATH = 'public/downloads'  

  desc "Build blast index for sequences"
  task :index => [:environment] do

    [ :protein, :gene ].each do |sequence_type|
      Zip::ZipFile.open("#{SEQUENCE_PATH}/sequences/#{sequence_type}.fasta.zip") do |zipfile|
        dbtype = sequence_type == :protein ? 'T' : 'F'
        fasta_file_name = "#{sequence_type}.fasta"
        title = fasta_file_name.sub(/\.fasta/, '')

        file = Tempfile.new('blast')
        file.write( zipfile.file.read(fasta_file_name) )

        output = `formatdb -i #{file.path} -p #{dbtype} -n index/blast/#{title} -t #{title}`
        print "running... formatdb -i #{file.path} -p #{dbtype} -n index/blast/#{title} -t #{title} \n"
        print output,"\n"
        file.close
      end
    end
  end
end