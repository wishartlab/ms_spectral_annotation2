# encoding: utf-8

namespace :clean do
  desc "Clean orphaned has one relationships, ensure required has_ones exist for metabolites"
  task :has_one => [:environment] do
    Metabolite.find_each do |m|
      m.create_ontology if m.ontology.blank?
      m.create_link_set if m.link_set.blank?
      m.create_experimental_property_set if m.experimental_property_set.blank?
    end

    [LinkSet, ExperimentalPropertySet, Ontology].each do |c|
      c.send(:find_each) do |entity|
        if entity.metabolite.blank?
          puts "Removing missing #{entity.class} (#{entity.metabolite_id})"
          entity.delete
        end
      end
    end
  end

  desc "Remove any metabolite protein links that are missing proteins or metabolites"
  task :metabolite_protein_links => [:environment] do
    MetaboliteProteinLink.includes(:metabolite, :protein).find_each do |mpl|
      if mpl.protein.blank? || mpl.metabolite.blank?
        puts "Destroying #{mpl.id}"
        mpl.destroy
      end
    end
  end
end

namespace :refactor do
  task :detected_concentrations => [ :environment ] do
    invalids = []
    Concentration.find_each do |c|
      puts "Setting #{c.id}"

      if c.conc_value.to_s.strip == 'Not Detected'
        c.destroy
        next
      end

      if c.source == 'Inferred' || c.conc_condition =~ /Phenol-Explorer/
        c.status = Concentration::EXPECTED_NOT_QUANTIFIED
        c.conc_value = nil
        c.conc_unit = nil
        c.age = nil
        c.sex = nil
        if c.comment.to_s =~ /DrugBank/
          c.expected_source = "DrugBank"
        end
        if c.comment.to_s =~ /Phenol Explorer/
          c.expected_source = "Phenol-Explorer"
        end
        if c.comment.to_s =~ /Inferred from detection in urine/
          c.expected_source = 'Inferred from presence in urine'
        end
      elsif c.conc_value.to_s == 'Detected' || c.comment.to_s =~ /detected|added by rah/ || c.comment.to_s.strip == 'Present'
        c.status = Concentration::DETECTED_NOT_QUANTIFIED
        c.conc_value = nil
        c.conc_unit = nil
      elsif c.conc_value.present?
        c.status = Concentration::DETECTED_AND_QUANTIFIED
      elsif c.comment.present? && c.comment.strip.downcase =~ /not detected|not present|increased|decreased|trace amount/
        c.destroy
        next
      else
        puts "Invalid source"
        invalids << c.id
        c.destroy
        next
      end

      if c.source == 'HMP Experimental' || c.source == 'Inferred'
        c.source = Concentration::HMP_SOURCE
      else
        c.source = Concentration::REFERENCE_SOURCE
      end

      if c.valid?
        c.save!
      else
        invalids << c.id
        c.destroy
      end
    end

    puts "Invalid: #{invalids.join(',')}"

  end

  task :move_pdb_ids => [:environment] do
    Metabolite.includes(:link_set).find_each do |m|
      next if m[:pdb_id].blank? || m[:pdb_id] =~ /Not Available/
      m.link_set.update_column(:pdb_id, m[:pdb_id])
    end
  end

  task :diseases => [:environment] do
    Disease.all.each do |d|
      d.synonyms = d.synonyms.to_s.strip.sub(/;\Z/, '')
      d.synonyms = nil if d.synonyms.blank?
      d.created_at = Time.now - 4.years
      d.save!
    end
  end

  namespace :references do
    task :move => [:environment] do
      Reference.pubmed.find_each do |r|
        Article.create!(:pubmed_id => r.pubmed_id, :citation => r.reference_text)
      end
      Reference.textbook.find_each do |r|
        Textbook.create!(:title => r.reference_text)
      end
    end

    task :reference_relationships => [ :environment ] do
      # models = { 'ConcentrationReference' => :concentration,
      #   'MetaboliteProteinReference' => :metabolite_protein_link, 'MetaboliteReference' => :metabolite }
      models = { 'MetaboliteReference' => :metabolite }
      models.each do |klass, relationship|
        klass.constantize.includes(relationship, :reference).find_each do |entity|
          o = entity.send(relationship)
          r = entity.reference
          next if o.blank? || r.blank?
          if r.pubmed_id.blank?
            textbook = Textbook.where(:title => r.reference_text)
            o.textbooks << textbook
          else
            article = Article.where(:pubmed_id => r.pubmed_id)
            o.articles << article
          end
        end
      end
    end
  end

  desc "Move experimental values to model"
  namespace :experimental_properties do
    task :all => [ :logp, :solubility, :melting_point ]

    task :logp => [:environment] do
      Metabolite.where("experimental_logp <> ''").find_each do |metabolite|
        eps = metabolite.experimental_property_set || metabolite.create_experimental_property_set

        if metabolite.experimental_logp.present?
          v = metabolite.experimental_logp
          ref = nil

          if v.to_s.strip.match(/\A[+-]?\d+?(\.\d+)?\Z/)
            v = v.strip
          elsif md = /(.*?)\[(.*)\]/.match(v)
            v = md[1].strip
            ref = md[2].strip
            if v.to_s.strip.match(/\A[+-]?\d+?(\.\d+)?\Z/)
            end
          else
            v = nil
          end

          if v.present? && ( (v.index('-').nil? && v.length > 5) || (v.index('-').present? && v.length > 6))
            v = v.to_f.round(2).to_s
            ref = "Extrapolated"
          end

          if v.present?
            puts "Setting logp for #{metabolite.hmdb_id}:"
            puts "\t#{v}"
            puts "\t#{ref}"
          else
            puts "Could not set logp for #{metabolite.hmdb_id}:"
            puts "\t#{metabolite.experimental_logp}"
          end
          eps.update_attributes!(:logp => v) if v.present?
          eps.update_attributes!(:logp_reference => ref) if ref.present?
        end
      end
    end

    task :solubility => [:environment] do
      CSV.open('tmp/solubility.csv').each do |row|
        hmdb_id, solubility, reference = row
        metabolite = Metabolite.find_by_hmdb_id(hmdb_id) || next
        eps = metabolite.experimental_property_set || metabolite.create_experimental_property_set

        if solubility.to_s.strip.present?
          solubility = solubility.gsub(/oC/, ' °C').gsub(/deg C/, ' °C').gsub(/  /, ' ').gsub(/g\/ml/, 'g/mL').gsub(/g\/l/, 'g/L').strip
          puts "#{hmdb_id}: #{solubility}"
        else
          solubility = nil
        end

        if reference.to_s.strip.blank?
          reference = nil
        else
          reference = reference.strip
        end

        eps.update_attributes!(:water_solubility => solubility, :water_solubility_reference => reference)
      end
    end

    task :melting_point => [:environment] do
      Metabolite.where("melting_point <> ''").find_each do |metabolite|
        eps = metabolite.experimental_property_set || metabolite.create_experimental_property_set

        v = metabolite.melting_point
        ref = nil

        if v.to_s.strip.match(/\A[-<>]?\s*(\d|\.)+ ?o?C\Z/)
          v = v.strip
        elsif v.to_s.strip.match(/\A(\d|\.)+\s*-\s*(\d|\.)+ ?o?C\Z/)
          v = v.strip
        else
          v = nil
        end

        if v.to_s.strip.present?
          v = v.gsub(/oC/, ' °C').gsub(/deg C/, ' °C').gsub(/ C/, ' °C').gsub(/  /, ' ').strip
          puts "#{metabolite.hmdb_id}: #{v}"
        end

        eps.update_attributes!(:melting_point => v)
      end
    end

    task :mp_csv => [:environment] do
      CSV.open('tmp/mp.csv').each do |row|
        hmdb_id, mp, bp = row
        metabolite = Metabolite.find_by_hmdb_id(hmdb_id) || next
        eps = metabolite.experimental_property_set || metabolite.create_experimental_property_set

        if mp.to_s.strip.present?
          mp = mp.gsub(/oC/, ' °C').gsub(/deg C/, ' °C').gsub(/ C/, ' °C').gsub(/  /, ' ').strip
          puts "MP: #{hmdb_id}: #{mp}"
        else
          mp = nil
        end

        if bp.to_s.strip.present?
          bp = bp.gsub(/oC/, ' °C').gsub(/deg C/, ' °C').gsub(/ C/, ' °C').gsub(/  /, ' ').strip
          puts "BP: #{hmdb_id}: #{bp}"
        else
          bp = nil
        end

        eps.update_attributes!(:melting_point => mp, :boiling_point => bp)
      end
    end
  end

end

# [
#     [ 0] 2200,
#     [ 1] "HMDB00192",
#     [ 2] "Referenced_Experimental",
#     [ 3] "Dan",
#     [ 4] "Abnormal",
#     [ 5] "Cystinuria",
#     [ 6] "Urine",
#     [ 7] "N/A",
#     [ 8] "> 117",
#     [ 9] "umol/mmol_creatinine",
#     [10] "Adult:>18_yrs_old",
#     [11] "Both",
#     [12] "8890399",
#     [13] "",
#     [14] "No",
#     [15] "Yes",
#     [16] nil
# ]

namespace :fix do

  desc "Extract Concentrationreferences into their own table"
  task :concentration_references => [:environment] do
    Concentration.includes(:references).find_each do |c|
      next if c.references.present?
      c.pubmed_ids.each do |pubmed_id|
        ref = Reference.where(pubmed_id: pubmed_id).first_or_create!
        if !c.references.include?(ref)
          puts "Adding #{pubmed_id} to #{c.id}"
          c.references << ref
        end
      end
      c.text_references.each do |ref_text|
        ref = Reference.where(reference_text: ref_text).first_or_create!
        if !c.references.include?(ref)
          puts "Adding #{ref_text} to #{c.id}"
          c.references << ref
        end
      end
    end
  end

  task :labm_concs => [ :environment ] do
    ActiveRecord::Base.connection.execute("SELECT * FROM tbl_concentrations WHERE conc_value <> ''").each do |row|
      hmdb_id = row[1]
      biofluid = row[6]
      value = row[8]
      exported = row[15]
      units = row[9]
      age = row[10]

      next if exported == 'No' || value == 'Detected'

      metabolite = Metabolite.find_by_hmdb_id(hmdb_id)
      next if metabolite.blank?

      c = metabolite.concentrations.where(conc_value: value).first

      puts "processing #{hmdb_id}:"
      if c.blank?
        puts "\tCan't find concentration #{value}"
        next
      end

      if c.conc_unit.blank? && units.present?
        c.update_column(:conc_unit, units)
      end

      if c.age.blank? && age.present?
        c.update_column(:age, age)
      end
    end
  end

  task :concentrations => [:environment] do
    age_translate = { 'N/A' => 'Not Specified',
      'Newborn:0-30_days_old' => 'Newborn (0-30 days old)',
      'Infant:0-1_yr_old' => 'Infant (0-1 year old)',
      'Children:1-13_yrs_old' => 'Children (1-13 year old)',
      'Adolescent:13-18_yrs_old' => 'Adolescent (13-18 years old)',
      'Adult:>18_yrs_old' => 'Adult (>18 years old)',
      'Elderly:>65_yrs_old' => 'Elderly (>65 years old)' }

    hmdb_ref = Reference.find_by_pubmed_id(18953024)

    Concentration.includes(:references).find_each do |c|
      if c.export_hmdb == "No" || c.conc_value.to_s.strip == "NA" || c.comment.to_s =~ /Biofluid added by Craig Knox/
        puts "Deleting because first"
        c.destroy
        next
      end

      puts "Updating #{c.id}"

      if c.conc_value.to_s.strip.downcase == "not detected"
        c.conc_value = 'Not Detected'
        c.conc_unit = nil
      end

      if c.patient_status.blank? && c.conc_value == 'Detected' && c.comment.to_s.strip == 'From serum paper'
        c.patient_status = "Normal"
        c.conc_condition = "Normal"
        c.source = 'Inferred'
      end

      if c.patient_status.blank? && c.conc_value == 'Detected' && c.comment.to_s.strip == 'From DrugBank'
        c.patient_status = "Normal"
        c.conc_condition = "Taking drug identified by DrugBank entry #{c.metabolite.drugbank_id}"
        c.source = 'Inferred'
      end

      if c.patient_status.blank? && c.comment.to_s.strip == 'Data from Phenol Explorer. Added by Anchi.'
        c.patient_status = "Normal"
        c.conc_condition = "Consuming polyphenols described by Phenol-Explorer entry #{c.metabolite.phenol_metabolite_id}"
        c.source = 'Referenced Experimental'
      end

      c.comment = nil if c.comment.blank?

      if c.biofluid_source.present?
        c.biofluid_source = c.biofluid_source.gsub(/_/, ' ').sub(/Cerebrospinal Fluid\(CSF\)/, 'Cerebrospinal Fluid (CSF)')
      end

      if c.conc_unit.present?
        if c.conc_value.to_s.strip.downcase == 'detected'
          c.conc_unit = nil
        else
          c.conc_unit = "Not Specified" if c.conc_unit == 'N/A'
          c.conc_unit = 'umol/g tissue' if c.conc_unit == 'umol/gr_tissue'
          c.conc_unit = c.conc_unit.gsub(/_/, ' ')
        end
      else
        c.conc_unit = nil
      end

      if c.conc_value.blank?
        c.conc_value = nil
        if c.conc_unit.present?
          c.conc_unit = nil
        end
      end

      if c.sex.present?
        c.sex = c.sex.gsub(/_/, ' ')
        c.sex = "Not Specified" if c.sex == 'N/A'
      else
        c.sex = nil
      end

      if c.age.present?
        c.age = found_age = age_translate[c.age]
        if found_age.nil?
          puts "Couldn't find age"
        end
      else
        c.age = nil
      end

      c.source = c.source.gsub(/_/, ' ')

      if c.patient_status.blank?
        puts "Deleting because c.patient_status.blank?"
        c.destroy
        next
      end

      if c.source == 'Referenced Experimental' && c.references.blank?
        puts "Deleting because c.source == 'Referenced Experimental' && c.references.blank?"
        c.destroy
        next
      end

      if c.abnormal? && c.conc_condition.blank?
        puts "Deleting because c.abnormal? && c.conc_condition.blank?"
        c.destroy
        next
      end

      if c.biofluid_source.blank?
        puts "Deleting because c.biofluid_source.blank?"
        c.destroy
        next
      end

      if c.experimental?
        if c.reference.to_s.strip =~ /\A\d+\Z/ || c.reference.to_s.strip =~ /\A\d+; \d+\Z/ || c.reference.to_s =~ /USDA|Tietz/
          c.source = 'Referenced Experimental'
        elsif c.references.blank?
          c.references << hmdb_ref
        end
      end

      c.save!
    end
  end
end
