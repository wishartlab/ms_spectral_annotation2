require 'progress_bar'
require 'zip'
require 'builder'
require 'admin_mailer'
require 'wishart/task_support/zipper'
require 'progress_bar'

namespace :moldb do
    desc "Synchronize the data available on the structure server for the given environment"
    task :sync => [:environment] do
        # count = Metabolite.count
        # batch_size  = 10
        # batch_count = (count.to_f/batch_size.to_f).ceil
        # progress = ProgressBar.new(batch_count)

        # puts "Updating cache from MolDB"
        # Metabolite.order(:hmdb_id).find_in_batches(:batch_size => batch_size) do |group|
        #   group.each do |metabolite|
        #     metabolite.update_cached_attributes
        #     metabolite.save!(:validate => false)
        #   end
        #   progress.increment!
        # end

        m = Metabolite.find_by(hmdb_id: "HMDB0000001")
        m.update_cached_attributes
        m.save!(:validate => false)

        # metabolites = Metabolite.where('id >= 111519')
        # metabolites = Metabolite.where('created_at like "2017%"')
        # metabolites.each do |m|
        #   puts m.id
        #   m.update_cached_attributes
        #   m.save!(:validate => false)
          
        #   # Also export
        #   m.export_to_hmdb = 1
        #   m.save!
        #   puts m.hmdb_id

        #   if m.predicted_in_hmdb
        #     if !ontology = Ontology.find_by(metabolite_id: m.id)
        #       ontology = Ontology.new
        #       ontology.metabolite_id = m.id
        #     end
        #     if !ontology.origin.present?
        #       ontology.origin = "Endogenous"
        #     end
        #     ontology.save!
        #     puts ontology.origin
        #   end
        # end
    end


    desc "Synchronize the data available in c_ms table in moldb for tms_derivative table in HMDB"
    task :sync_tms_derivative => [:environment] do
        mailer = AdminMailer.build "hmdb",
            subject: "TMS derivatives sync with hmdb",
            message: "Synchronize the data available in c_ms table in moldb for tms_derivative table in HMDB."

        # SYNC_URL = "something".freeze

        begin
            open("http://moldb.wishartlab.com/tms_derivatives.json") {|f| @data = JSON.load(f.read)}
            @data.each do | c_ms_entry |
                if c_ms_entry["database_id"].length != 11
                    @hm_id = "00"+c_ms_entry["database_id"][4..-1]
                else
                    @hm_id = c_ms_entry["database_id"]
                end

                met_id = Metabolite.find_by_hmdb_id(@hm_id)
                
                if !met_id.id.nil?
                    begin
                        new_tms_derivative = TmsDerivative.create(metabolite_id: met_id.id, moldb_derivative_type: c_ms_entry["derivative_type"], moldb_derivative_mw: c_ms_entry["molecular_weight"], moldb_derivative_exact_mass: c_ms_entry["exact_mass"])
                        new_tms_derivative.save!
                        puts "saved derivatives for metabolite #{met_id.id} "
                    rescue 
                        puts "not saved due to uniqness property"
                    end
                end
            end
            mailer.notify_success!
        rescue => e
            raise if Rails.env.development?
            mailer.notify_error! e
        end
    end
end