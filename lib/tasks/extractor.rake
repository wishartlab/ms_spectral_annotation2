namespace :extractor do
  desc "Test the index for the data extractor"
  task :test_index => [:environment] do
    LIMIT = 200
    progress = ProgressBar.new LIMIT
    Metabolite.includes(:proteins).limit(LIMIT).each do |metabolite|
      progress.increment!
      ExtractorModel.index_for_metabolite metabolite
    end
    Sunspot.commit
  end

  desc "Fully rebuild the index for the data extractor."
  task :index => [:environment] do
    count = Metabolite.exported.count
    batch_size = 800
    num_pages = (count.to_f/batch_size.to_f).ceil

    for page in 1..num_pages do
      puts "Indexing batch #{page} of #{num_pages}" 
      progress = ProgressBar.new batch_size

      Metabolite.exported.includes(:proteins).page(page).per(batch_size).all.each do |metabolite|
        progress.increment!
        ExtractorModel.index_for_metabolite metabolite
      end
      Sunspot.commit
    end
    
  end
end
