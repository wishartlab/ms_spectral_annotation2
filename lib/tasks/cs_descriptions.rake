namespace :hmdb_desc do
	desc "externally generated cs descriptions for existing exported metabolites that have no descriptions or 'UL' in description field!"
	task rewrite_desc: [:environment] do
		count = 0
		CSV.open('public/DrWishart_descriptions.tsv', 'r', :col_sep => "\t", :quote_char => "\x00").each do | row |
			if row.empty? || row.nil? || row=="" || row.blank?
				next
			else
				count+= 1
				id = row[0]
      	desc = row[1]
      	m = Metabolite.find_by_hmdb_id(id.strip)
      	#if m.description.empty? || m.description.nil? || m.description.blank? || m.description.strip == "" || m.description.strip == "UL"	
          m.description = desc.strip
      		m.save!	
      		puts "#{count}- done with: #{id}"
      	#else
      		#puts ">>>> did not rewrite description for: #{id}"
      		#next
      	#end
      end
    end
  end

  desc "add sentences about association of metabolite with IEMs"
  task add_IEM_to_desc: [:environment] do
    count = 0
    CSV.open('public/metabolites_desc_associated_with_IEM.tsv', 'r', :col_sep => "\t", :quote_char => "\x00").each do | row |
      if row.empty? || row.nil? || row=="" || row.blank?
        next
      else
        count+= 1
        id = row[0]
        desc = row[1]
        m = Metabolite.find_by_hmdb_id(id.strip) 
        m.description = desc.strip
        m.save! 
        puts "#{count}- done with: #{id}"
      end
    end
  end
end