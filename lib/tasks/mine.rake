namespace :mine do  

  desc "export classifications"
  task :classifications => [:environment] do
    ids = File.readlines(ENV['INPUT'])
    ids.each do |id|
      id.strip!
      if id =~ /HMDB\d{7}/
        metabolite = Metabolite.find_by_hmdb_id(id)
        out = "#{id}\t"
        out += "#{metabolite.classyfire_superklass.try(:[], :name)}\t"
        out += "#{metabolite.classyfire_klass.try(:[], :name)}\t"
        out += "#{metabolite.classyfire_subklass.try(:[], :name)}\t"
        out += "#{metabolite.classyfire_direct_parent.try(:[], :name)}\t"
        parents = metabolite.classyfire_alternative_parents ? metabolite.classyfire_alternative_parents.map {|c| c[:name]}.join(';') : ''
        out += "#{parents}\t"
        out += "#{metabolite.classyfire_substituents.try(:join, ';')}"
        puts out
      end
    end
  end

  desc "export assignment files"
  task :assignments => [:environment] do
    ids = File.readlines(ENV['INPUT'])
    out_dir = ENV['OUTDIR']
    ids.each do |id|
      id.strip!
      next if id.blank?
      puts id
      metabolite = Metabolite.find_by_hmdb_id(id)
      nmrs= metabolite.spectra.select { |s| s.class == Specdb::NmrOneD }
      puts "One D NMR: #{nmrs.count}"
      nmrs.each do |nmr|
        docs = nmr.documents.select { |d| d.description == 'List of chemical shift values for the spectrum' }
        if docs.length > 1
          puts "******Too many docs!********"
        elsif docs.length == 1
          dest = File.join(out_dir, "#{id}.#{nmr.nucleus}.assignments.txt")
          if File.exist?(dest)
            puts "File exists already!"
          else
            cmd = "wget -O #{dest} #{docs.first.url}"
            system(cmd)
          end
        end
      end
    end
  end

  desc "export files for nmrml creation"
  task :nmr_1_d => [:environment] do
    out_dir = ENV['OUT_DIR']
    ids_file = ENV['IDS_FILE']
    unless out_dir && ids_file
      puts "OUT_DIR and IDS_FILE must be set"
      exit
    end
    ids = File.readlines(ids_file).map(&:strip)
    ids.each do |id|
      metabolite = Metabolite.find_by_hmdb_id(id)
      puts metabolite.hmdb_id
      nmrs = metabolite.nmr_one_d_spectra
      next if nmrs.blank?
      nmrs.each do |nmr|
        name = "#{metabolite.hmdb_id}_#{nmr.nucleus}_#{nmr.id}"
        documents = nmr.documents
        assignments = documents.select { |d| d.description == 'List of chemical shift values for the spectrum' }
        images = documents.select { |d| d.description == 'Spectra image with peak assignments' }
        fids = documents.select { |d| d.description == 'Raw Free Induction Decay file for spectral processing' }
        if fids.length != 1 || images.length != 1 || assignments.length != 1
          puts "Doc Error '#{name}': Assignments=#{assignments.length}; Images=#{images.length}; FIDs=#{fids.length}"
        else
          metabolite_dir = File.join(out_dir, name)
          # puts name
          FileUtils.mkdir_p(metabolite_dir)
          docs = [assignments.first, images.first, fids.first]
          file_names = ['assignments.txt', 'image.png', 'fid.zip']
          docs.each_with_index do |doc, index|
            dest = File.join(metabolite_dir, file_names[index])
            cmd = "wget -O #{dest} #{doc.url} >> #{out_dir}/log.txt 2>&1"
            system(cmd)
          end
        end
      end
    end
  end

  desc "export concentration data for analysis"
  task :exportdata => [:environment] do

    Metabolite.find_each do |record|
      concs = Metabolite.find_by_id(record.id).concentrations
      concs.each do |c|
        unit = ""
        conc = c.conc_value
        mean = 0
        error = 0
        
        if (conc.nil?)
          next
        elsif(conc == "Detected" || conc == "not detected" || conc == "NA" || conc == "Not Detected")
          next
        else
          conc = conc.strip
        end
        
        if (conc =~/^([\+\-\d\.Ee]+)$/)
          mean = $1.to_f
          error = 0
        elsif (conc =~/^<?([\d\.]+)\s*\-\s*([\d\.]+)$/)
          mean = ($1.to_f + $2.to_f)/2
          error = ($2.to_f - $1.to_f)/2
        elsif (conc =~/^([\d\.]+)\s*\+\/\-\s*([\d\.]+)\s*/)
          mean = $1.to_f
          error = $2.to_f
        elsif (conc =~/^\>=?\s*([\d\.]+)$/)
          mean = $1.to_f
          error = 0
        elsif (conc =~/^\<=?\s*([\d\.]+)$/)
          mean = $1.to_f/2
          error = $1.to_f/2
        elsif (conc =~/^([\+\-\d\.Ee]+)\s+\+\/\-\s*([\+\-\d\.Ee]+)$/)
          mean = $1.to_f
          error = $2.to_f
        elsif (conc =~/^([\d\.]+)\s*\(\s*([\d\.]+)\s*[^\s\d\.\+]\s*([\d\.]+)\s*\)$/)
          mean = $1.to_f
          error = ($3.to_f - $2.to_f)/2
        else
          puts "cannot parse concentration: '#{conc}' id: #{c.id}"
          next
        end
        
        
        if (c.biofluid_source == "Blood")
          if (c.conc_unit == "uM")
            
          else
 #           puts "ERROR: unusually unit for blood concentration: #{c.conc_unit} id: #{c.id}"
            next
          end
         puts "#{record.hmdb_id}\t#{c.patient_status}\t#{c.conc_condition}\t#{c.biofluid_source}\t#{mean}\t#{error}\t#{c.age}\t#{c.sex}\t#{c.id}"
        elsif (c.biofluid_source == "Urine")
          if (c.conc_unit == "uM")
            # assume this is right (i.e. = umol/mmol_creatinine)
          elsif (c.conc_unit == "umol/mmol_creatinine" || c.conc_unit == "umol/mmol creatinine")
            
          elsif (c.conc_unit == "nmol/mmol_creatinine" || c.conc_unit == "nmol/mmol creatinine")
            mean = mean * 1000
            error = error * 1000
          else
#            puts "ERROR: unusually unit for urine concentration: #{c.conc_unit} id: #{c.id}"
            next
          end
         puts "#{record.hmdb_id}\t#{c.patient_status}\t#{c.conc_condition}\t#{c.biofluid_source}\t#{mean}\t#{error}\t#{c.age}\t#{c.sex}\t#{c.id}"
        else
          next
        end
        
        
      end
    end
  end

  desc "Grab data for T3DB"
  task :t3db_check => [:environment] do
    # Read in list of manually mapped compound names to HMDB IDs
    list_hmdb_ids = {}
    CSV.foreach(Rails.root + "public/t3db_list.csv", :headers => true) do |row|
      if row[0].present? 
        list_hmdb_ids[row[0]] = row[3]
      end
    end
    CSV.open(Rails.root + "public/output1-toxins-with-hmdb.csv", "wb") do |csv|
      csv << ["Import ID", "Common Name", "Description", "Types", "Synonyms", "CAS Number", 
        "InChI", "InChI Key", "SMILES", "Kingdom", "Super Class", "Class", "Sub Class", 
        "Direct Parent", "Alternate Parents", "Substituents", "Geometric Description", 
        "Classyfire Description", "Origin", "Cellular Locations", "Tissues", "Pathways",
        "State", "Appearance", "Melting Point", "Boiling Point", "Solubility", "LogP", 
        "Route of Exposure", "Mechanism of Toxicity", "Metabolism", "Toxicity", "Lethal Dose",
        "Carcinogenic", "Uses/Sources", "Minimum Risk Level", "Health Effects", "Adverse Effects", "Symptoms",
        "Treatment", "DrugBank ID", "HMDB ID", "PubChem ID", "KEGG ID", "KEGG Drug ID",
        "UniProt", "OMIM ID", "ChEBI ID", "BioCyc ID", "CTD ID", "Stitch", "PDB ID",
        "ACToR ID", "ChemSpider ID", "Meta Cyc ID", "ChEMBL ID", "Wikipedia", "Group", "Found On", "Alternate HMDB ID"]
      CSV.open(Rails.root + "public/output1-concentrations.csv", "wb") do |concentrations_csv|
        concentrations_csv << ["Compound Import ID", "Status", "Biofluid", "Value", "Units", "Age", "Sex"]
        CSV.open(Rails.root + "public/output1-concentration_refs.csv", "wb") do |concentration_refs_csv|
          concentration_refs_csv << ["Concentration Import ID", "Name", "PubMed ID", "Link", "ISBN"]
          conc_count = 1
          CSV.open(Rails.root + "public/output1-refs.csv", "wb") do |refs_csv|
          refs_csv << ["HMDB ID", "Name", "PubMed ID", "Link", "ISBN"]
            CSV.foreach(Rails.root + "public/output1-toxins.csv", :headers => true) do |row|
              inchi_key = row[7]
              if Metabolite.where("moldb_inchikey = ?", inchi_key).count > 0 || list_hmdb_ids[row[1]].present?
                # First try to get HMDB ID using the InChi Key
                if Metabolite.where("moldb_inchikey = ?", inchi_key).count > 0
                  metabolite = Metabolite.where("moldb_inchikey = ?", inchi_key).first
                  found_on = "InChI Key"
                  puts "FOUND FROM DW " + metabolite.name
                # Else use the HMDB ID from the list
                else
                  metabolite = Metabolite.where("hmdb_id = ?", list_hmdb_ids[row[1]]).first
                  found_on = "Name"
                  puts "FOUND FROM LIST " + metabolite.name
                end
                csv << [
                  row[0],
                  metabolite.name.present? ? metabolite.name : row[1], #Name
                  metabolite.description.present? ? metabolite.description : row[2], #Take HMDB description or give list from DW
                  row[3], #Types
                  metabolite.synonyms.present? ? "\"" + metabolite.synonyms.map { |s| s.synonym }.join("\", \"") + "\"" : row[4],  #Take HMDB synonyms or give list from DW
                  metabolite.cas.present? ? "CAS:" + metabolite.cas : "", #Take from HMDB or nothing
                  metabolite.moldb_inchi.present? ? metabolite.moldb_inchi : "", #Take from HMDB or nothing
                  row[7],
                  metabolite.moldb_smiles.present? ? metabolite.moldb_smiles : "", #Take from HMDB or nothing
                  row[9], 
                  row[10], 
                  row[11],
                  row[12],
                  row[13],
                  row[14],
                  row[15],
                  row[16],
                  row[17],
                  metabolite.ontology.present? ? metabolite.ontology.origin : "", #Origin
                  metabolite.cellular_locations.present? ? "\"" + metabolite.cellular_locations.map { |s| s.name }.join("\", \"") + "\"" : "", #Cellular locations, take from HMDB or nothing
                  metabolite.tissues.present? ? "\"" + metabolite.tissues.map { |s| s.name }.join("\", \"") + "\"" : "", #Tissues, take from HMDB or nothing
                  metabolite.pathways.present? ? "\"" + metabolite.pathways.map { |s| s.name }.join("\", \"") + "\"" : "", #Pathways, take from HMDB or nothing
                  metabolite.state, #State
                  row[23], #Appearance
                  metabolite.experimental_melting_point, #Melting Point
                  metabolite.experimental_boiling_point, #Boiling Point
                  metabolite.experimental_water_solubility, #Solubility
                  metabolite.experimental_logp, #LogP
                  row[28], #Route of Exposure
                  row[29], #Mechanism of Action
                  row[30], #Metabolism
                  row[31], #Toxicity
                  row[32], #Lethal Dose
                  row[33], #Carcinogenic
                  row[34], #Uses/Sources
                  row[35], #Minimum Risk Level
                  row[36],
                  row[37],
                  row[38], #Symptoms
                  row[39], #Treatment
                  metabolite.link_set.try(:drugbank_id), 
                  metabolite.hmdb_id, 
                  metabolite.link_set.try(:pubchem_cid),
                  metabolite.link_set.try(:kegg_id), 
                  row[44], 
                  row[45], #UniProt
                  row[46], #OMIM
                  metabolite.link_set.try(:chebi_id),
                  metabolite.link_set.try(:biocyc_id), #Bio Cyc
                  row[49], #CTD
                  row[50], #Stitch
                  metabolite.pdb_id, #PDB
                  row[52], #ACToR
                  metabolite.link_set.try(:chemspider_id), 
                  row[54], 
                  row[55], 
                  metabolite.link_set.try(:wiki_id), #Wikipedia
                  row[57],
                  found_on, 
                  metabolite.hmdb_id == list_hmdb_ids[row[1]] ? "" : list_hmdb_ids[row[1]]
                ]

                # Add Concentrations
                metabolite.concentrations.normal.each do |c|
                  if c.status != "Expected but not Quantified"
                    concentrations_csv << [
                      row[0],
                      c.status,
                      c.biofluid_source,
                      c.conc_value,
                      c.conc_unit,
                      c.age,
                      c.sex
                    ]
                    c.articles.each do |a|
                      concentration_refs_csv << [
                        conc_count,
                        a.citation,
                        a.pubmed_id,
                        "",
                        ""
                      ]
                    end
                    c.textbooks.each do |t|
                      concentration_refs_csv << [
                        conc_count,
                        t.title,
                        "",
                        "",
                        t.isbn
                      ]
                    end
                    c.external_links.each do |l|
                      concentration_refs_csv << [
                        conc_count,
                        l.name,
                        "",
                        l.url,
                        ""
                      ]
                    end
                    conc_count = conc_count + 1
                  end
                end

                # Add References
                metabolite.articles.each do |a|
                  refs_csv << [
                    metabolite.hmdb_id,
                    a.citation,
                    a.pubmed_id,
                    "",
                    ""
                  ]
                end
                metabolite.textbooks.each do |t|
                  refs_csv << [
                    metabolite.hmdb_id,
                    t.title,
                    "",
                    "",
                    t.isbn
                  ]
                end
                metabolite.external_links.each do |l|
                  refs_csv << [
                    metabolite.hmdb_id,
                    l.name,
                    "",
                    l.url,
                    ""
                  ]
                end
              else
                puts "NO MATCH"
                csv << [
                  row[0],
                  row[1], 
                  row[2],
                  row[3], #Types
                  row[4], 
                  row[5].present? ? "CAS:" + row[5] : "",
                  row[6], 
                  row[7],
                  row[8],
                  row[9], 
                  row[10], 
                  row[11],
                  row[12],
                  row[13],
                  row[14],
                  row[15],
                  row[16],
                  row[17],
                  row[18], #Origin
                  row[19],
                  row[20], #Tissues
                  row[21],
                  row[22], #State
                  row[23], #Appearance
                  row[24], #Melting Point
                  row[25], #Boiling Point
                  row[26], #Solubility
                  row[27], #LogP
                  row[28], #Route of Exposure
                  row[29], #Mechanism of Action
                  row[30], #Metabolism
                  row[31], #Toxicity
                  row[32], #Lethal Dose
                  row[33], #Carcinogenic
                  row[34], #Uses/Sources
                  row[35], #Minimum Risk Level
                  row[36],
                  row[37],
                  row[38], #Symptoms
                  row[39], #Treatment
                  row[40], 
                  row[41], 
                  row[42],
                  row[43], 
                  row[44], 
                  row[45], #UniProt
                  row[46], #OMIM
                  row[47],
                  row[48], #Bio Cyc
                  row[49], #CTD
                  row[50], #Stitch
                  row[51], #PDB
                  row[52], #ACToR
                  row[53], 
                  row[54], 
                  row[55], 
                  row[56], #Wikipedia
                  row[57],
                  "DW",
                  ""
                ]
              end
            end
          end
        end
      end
    end
  end

  desc "Compare data with T3DB"
  task :compare_t3db => [:environment] do
    CSV.open(Rails.root + "public/hmdb-mappings-all.csv", "wb") do |csv|
      csv << ["T3DB ID", "Common Name", "Description", "Types", "Synonyms", "CAS Number", 
        "InChI", "InChI Key", "SMILES", "Kingdom", "Super Class", "Class", "Sub Class", 
        "Direct Parent", "Alternate Parents", "Substituents", "Descriptors", "Origin", 
        "Cellular Locations", "Tissues", "Pathways",
        "State", "Appearance", "Melting Point", "Boiling Point", "Solubility", "LogP", 
        "Route of Exposure", "Mechanism of Toxicity", "Metabolism", "Toxicity", "Lethal Dose",
        "Carcinogenicity", "Uses/Sources", "Minimum Risk Level", "Health Effects", "Symptoms",
        "Treatment", "DrugBank ID", "HMDB ID", "PubChem ID", "KEGG ID",
        "UniProt ID", "OMIM ID", "ChEBI ID", "BioCyc ID", "CTD ID", "Stitch", "PDB ID",
        "ACToR ID", "ChemSpider ID", "ChEMBL ID", "Wikipedia"]
      CSV.foreach(Rails.root + "public/hmdb-mappings-step.csv", :headers => true) do |row|
        existing = Metabolite.find_by_hmdb_id(row[39].strip)
        puts row[39].strip

        #Print out original data
        csv << row

        # Print out hmdb
        csv << [
          row[39].strip,
          existing.name, 
          existing.description,
          "", #Types
          existing.synonyms.present? ? "\"" + existing.synonyms.map(&:synonym).join("\", \"") + "\"": "", 
          existing.cas,
          existing.moldb_inchi, 
          existing.moldb_inchikey,
          existing.moldb_smiles,
          existing.taxonomy.present? ? existing.taxonomy.kingdom : "",
          existing.taxonomy.present? ? existing.taxonomy.superclass : "",
          existing.taxonomy.present? ? existing.taxonomy.tax_class : "",
          existing.taxonomy.present? ? existing.taxonomy.subclass : "",
          existing.taxonomy.present? ? existing.taxonomy.directparent : "",
          "",
          existing.taxonomy.substituents.present? ? "\"" + existing.taxonomy.substituents.join("\", \"") + "\"" : "",
          existing.taxonomy.descriptors.present? ? "\"" + existing.taxonomy.descriptors.join("\", \"") + "\"" : "",
          existing.ontology.present? ? existing.ontology.origin : "", #Origin
          existing.cellular_locations.present? ? "\"" + existing.cellular_locations.map { |s| s.name }.join("\", \"") + "\"" : "", #Cellular locations, take from HMDB or nothing
          existing.tissues.present? ? "\"" + existing.tissues.map { |s| s.name }.join("\", \"") + "\"" : "", #Tissues, take from HMDB or nothing
          existing.pathways.present? ? "\"" + existing.pathways.map { |s| s.name }.join("\", \"") + "\"" : "", #Pathways, take from HMDB or nothing
          existing.state, #State
          "", #Appearance
          existing.experimental_melting_point, #Melting Point
          existing.experimental_boiling_point, #Boiling Point
          existing.experimental_water_solubility, #Solubility
          existing.experimental_logp, #LogP
          "", #Route of Exposure
          "", #Mechanism of Action
          "", #Metabolism
          "", #Toxicity
          "", #Lethal Dose
          "", #Carcinogenicity
          "", #Uses/Sources
          "", #Minimum Risk Level
          "",
          "", #Symptoms
          "", #Treatment
          existing.link_set.try(:drugbank_id), 
          existing.hmdb_id, 
          existing.link_set.try(:pubchem_cid),
          existing.link_set.try(:kegg_id), 
          "", #UniProt
          "", #OMIM
          existing.link_set.try(:chebi_id),
          existing.link_set.try(:biocyc_id),
          "", #CTD
          "", #Stitch
          existing.pdb_id, #PDB
          "", #ACToR
          existing.link_set.try(:chemspider_id), 
          "", 
          existing.link_set.try(:wiki_id).present? ? "http://en.wikipedia.org/wiki/" + existing.link_set.try(:wiki_id) : "", #Wikipedia
        ]

        # Print out differences
        csv << [
          "DIFF",
          (row[1].present? && existing.name.present? && row[1] != existing.name) ? "*" : "", 
          (row[2].present? && existing.description.present? && row[2] != existing.description) ? "*" : "",
          "", #Types
          (row[4].present? && existing.synonyms.present? && row[4] != "\"" + existing.synonyms.map(&:synonym).join("\", \"") + "\"") ? "*" : "", 
          (row[5].present? && existing.cas.present? && row[5] != existing.cas) ? "*" : "",
          (row[6].present? && existing.moldb_inchi.present? && row[6] != existing.moldb_inchi) ? "*" : "", 
          (row[7].present? && existing.moldb_inchikey.present? && row[7] != existing.moldb_inchikey) ? "*" : "",
          (row[8].present? && existing.moldb_smiles.present? && row[8] != existing.moldb_smiles) ? "*" : "",
          (row[9].present? && existing.taxonomy.kingdom.present? && row[9] != existing.taxonomy.kingdom) ? "*" : "",
          (row[10].present? && existing.taxonomy.superclass.present? && row[10] != existing.taxonomy.superclass) ? "*" : "",
          (row[11].present? && existing.taxonomy.tax_class.present? && row[11] != existing.taxonomy.tax_class) ? "*" : "",
          (row[12].present? && existing.taxonomy.subclass.present? && row[12] != existing.taxonomy.subclass) ? "*" : "",
          (row[13].present? && existing.taxonomy.directparent.present? && row[13] != existing.taxonomy.directparent) ? "*" : "",
          "",
          (row[15].present? && existing.taxonomy.substituents.present? && row[15] != "\"" + existing.taxonomy.substituents.join("\", \"") + "\"") ? "*" : "",
          (row[16].present? && existing.taxonomy.descriptors.present? && row[16] != "\"" + existing.taxonomy.descriptors.join("\", \"") + "\"") ? "*" : "",
          (row[17].present? && existing.ontology.present? && row[17] != existing.ontology.origin) ? "*" : "", #Origin
          (row[18].present? && existing.cellular_locations.present? && row[18] != "\"" + existing.cellular_locations.map { |s| s.name }.join("\", \"") + "\"") ? "*" : "",
          (row[19].present? && existing.tissues.present? && row[19] != "\"" + existing.tissues.map { |s| s.name }.join("\", \"") + "\"") ? "*" : "",
          (row[20].present? && existing.pathways.present? && row[20] != "\"" + existing.pathways.map { |s| s.name }.join("\", \"") + "\"") ? "*" : "",
          (row[21].present? && existing.state.present? && row[21] != existing.state) ? "*" : "", #State
          "", #Appearance
          (row[23].present? && existing.experimental_melting_point.present? && row[23] != existing.experimental_melting_point) ? "*" : "", #Melting Point
          (row[24].present? && existing.experimental_boiling_point.present? && row[24] != existing.experimental_boiling_point) ? "*" : "", #Boiling Point
          (row[25].present? && existing.experimental_water_solubility.present? && row[25] != existing.experimental_water_solubility) ? "*" : "", #Solubility
          (row[26].present? && existing.experimental_logp.present? && row[26] != existing.experimental_logp) ? "*" : "", #LogP
          "", #Route of Exposure
          "", #Mechanism of Action
          "", #Metabolism
          "", #Toxicity
          "", #Lethal Dose
          "", #Carcinogenicity
          "", #Uses/Sources
          "", #Minimum Risk Level
          "",
          "", #Symptoms
          "", #Treatment
          (row[38].present? && existing.link_set.try(:drugbank_id).present? && row[38] != existing.link_set.try(:drugbank_id).to_s) ? "*" : "", 
          (row[39].present? && existing.hmdb_id.present? && row[39] != existing.hmdb_id.to_s) ? "*" : "", 
          (row[40].present? && existing.link_set.try(:pubchem_cid).present? && row[40] != existing.link_set.try(:pubchem_cid).to_s) ? "*" : "",
          (row[41].present? && existing.link_set.try(:kegg_id).present? && row[41] != existing.link_set.try(:kegg_id).to_s) ? "*" : "", 
          "", #UniProt
          "", #OMIM
          (row[44].present? && existing.link_set.try(:chebi_id).present? && row[44] != existing.link_set.try(:chebi_id).to_s) ? "*" : "",
          (row[45].present? && existing.link_set.try(:biocyc_id).present? && row[45] != existing.link_set.try(:biocyc_id).to_s) ? "*" : "",
          "", #CTD
          "", #Stitch
          (row[48].present? && existing.pdb_id.present? && row[49] != existing.pdb_id.to_s) ? "*" : "", #PDB
          "", #ACToR
          (row[50].present? && existing.link_set.try(:chemspider_id).present? && row[50] != existing.link_set.try(:chemspider_id).to_s) ? "*" : "", 
          "", 
          (row[52].present? && existing.link_set.try(:wiki_id).present? && row[52] != "http://en.wikipedia.org/wiki/" + existing.link_set.try(:wiki_id)) ? "*" : "", #Wikipedia
        ]

        #Blank separator
        csv << Array.new(53, "")
      end
    end
  end

  desc "Grab concentrations and references"
  task :get_conc_and_refs => [:environment] do
    CSV.open(Rails.root + "public/hmdb-concentrations.csv", "wb") do |concentrations_csv|
      concentrations_csv << ["T3DB ID", "Status", "Biofluid", "Value", "Units", "Age", "Sex"]
      CSV.open(Rails.root + "public/hmdb-concentration_refs.csv", "wb") do |concentration_refs_csv|
        concentration_refs_csv << ["Concentration Import ID", "Name", "PubMed ID", "Link", "ISBN"]
        conc_count = 1
        CSV.open(Rails.root + "public/hmdb-refs.csv", "wb") do |refs_csv|
        refs_csv << ["T3DB ID", "Name", "PubMed ID", "Link", "ISBN"]
          CSV.foreach(Rails.root + "public/existing_t3db_with_hmdb_list.csv") do |row|
            metabolite = Metabolite.where("hmdb_id = ?", row[1]).first

            # Add Concentrations
            metabolite.concentrations.normal.each do |c|
              if c.status != "Expected but not Quantified"
                concentrations_csv << [
                  row[0],
                  c.status,
                  c.biofluid_source,
                  c.conc_value,
                  c.conc_unit,
                  c.age,
                  c.sex
                ]
                c.articles.each do |a|
                  concentration_refs_csv << [
                    conc_count,
                    a.citation,
                    a.pubmed_id,
                    "",
                    ""
                  ]
                end
                c.textbooks.each do |t|
                  concentration_refs_csv << [
                    conc_count,
                    t.title,
                    "",
                    "",
                    t.isbn
                  ]
                end
                c.external_links.each do |l|
                  concentration_refs_csv << [
                    conc_count,
                    l.name,
                    "",
                    l.url,
                    ""
                  ]
                end
                conc_count = conc_count + 1
              end
            end

            # Add References
            metabolite.articles.each do |a|
              refs_csv << [
                row[0],
                a.citation,
                a.pubmed_id,
                "",
                ""
              ]
            end
            metabolite.textbooks.each do |t|
              refs_csv << [
                row[0],
                t.title,
                "",
                "",
                t.isbn
              ]
            end
            metabolite.external_links.each do |l|
              refs_csv << [
                row[0],
                l.name,
                "",
                l.url,
                ""
              ]
            end
          end
        end
      end
    end
  end

  desc "Grab data for T3DB"
  task :t3db => [:environment] do
    CSV.open(Rails.root + "public/toxcast_in_hmdb_2.csv", "wb") do |csv|
      csv << ["HMDB ID", "Common Name", "Description", "Types", "Synonyms", "CAS Number", 
        "InChI", "InChI Key", "SMILES", "Kingdom", "Super Class", "Class", "Sub Class", 
        "Direct Parent", "Alternative Parents", "Substituents", "Descriptors", 
        "Origin", "Cellular Locations", "Tissues", "Pathways",
        "State", "Appearance", "Melting Point", "Boiling Point", "Solubility", "LogP", 
        "Route of Exposure", "Mechanism of Toxicity", "Metabolism", "Toxicity", "Lethal Dose",
        "Carcinogenicity", "Uses/Sources", "Minimum Risk Level", "Health Effects", "Symptoms",
        "Treatment", "DrugBank ID", "HMDB ID", "PubChem ID", "KEGG ID",
        "UniProt ID", "OMIM ID", "ChEBI ID", "BioCyc ID", "CTD ID", "Stitch", "PDB ID",
        "ACToR ID", "ChemSpider ID", "ChEMBL ID", "Wikipedia"]
      CSV.foreach(Rails.root + "public/toxcast_with_hmdb_2.csv", :headers => true) do |row|
        if row[1].present?
          hmdb_id = row[1]
          puts hmdb_id
          metabolite = Metabolite.find_by_hmdb_id(hmdb_id)

          if metabolite.link_set.try(:drugbank_id).present? && row[0].present? && (metabolite.link_set.try(:drugbank_id) != row[0])
            puts "DRUGBANK ID MISMATCH at " + hmdb_id
          end
            
          csv << [
            hmdb_id,
            metabolite.name,
            metabolite.description,
            "", #Types
            metabolite.synonyms.present? ? "\"" + metabolite.synonyms.map(&:synonym).join("\", \"") + "\""  : "",
            metabolite.cas.present? ? "CAS:" + metabolite.cas : "",
            metabolite.moldb_inchi,
            metabolite.moldb_inchikey,
            metabolite.moldb_smiles,
            metabolite.try(:taxonomy).try(:kingdom), 
            metabolite.try(:taxonomy).try(:superclass), 
            metabolite.try(:taxonomy).try(:tax_class),
            metabolite.try(:taxonomy).try(:subclass),
            metabolite.try(:taxonomy).try(:direct_parent),
            "",
            metabolite.try(:taxonomy).try(:substituents).present? ? "\"" + metabolite.try(:taxonomy).try(:substituents).join("\", \"") + "\"" : "",
            metabolite.try(:taxonomy).try(:descriptors).present? ? "\"" + metabolite.try(:taxonomy).try(:descriptors).join("\", \"") + "\"" : "", #Descriptors,
            metabolite.ontology.present? ? metabolite.ontology.origin : "", #Origin
            metabolite.cellular_locations.present? ? "\"" + metabolite.cellular_locations.map { |s| s.name }.join("\", \"") + "\"" : "", #Cellular locations, take from HMDB or nothing
            metabolite.tissues.present? ? "\"" + metabolite.tissues.map { |s| s.name }.join("\", \"") + "\"" : "",
            metabolite.pathways.present? ? "\"" + metabolite.pathways.map { |p| p.name }.join("\", \"") + "\"" : "", #Pathways
            metabolite.state, #State
            "", #Appearance
            metabolite.experimental_melting_point, #Melting Point
            metabolite.experimental_boiling_point, #Boiling Point
            metabolite.experimental_water_solubility, #Solubility
            metabolite.experimental_logp, #LogP
            "", #Route of Exposure
            "", #Mechanism of Toxicity
            "", #Metabolism
            "", #Toxicity, some of this should go into Symptoms (manual)
            "", #Lethal Dose
            "", #Carcinogenic
            "", #Uses/Sources
            "", #Minimum Risk Level
            "", #Health Effects
            "", #Symptoms
            "", #Treatment
            metabolite.link_set.try(:drugbank_id).present? ? metabolite.link_set.try(:drugbank_id) : row[0], 
            metabolite.hmdb_id, 
            metabolite.link_set.try(:pubchem_cid),
            metabolite.link_set.try(:kegg_id), 
            "", #UniProt
            "", #OMIM
            metabolite.link_set.try(:chebi_id),
            metabolite.link_set.try(:biocyc_id),
            "", #CTD
            "", #Stitch
            metabolite.pdb_id, #PDB
            "", #ACToR
            metabolite.link_set.try(:chemspider_id), 
            "", 
            metabolite.link_set.try(:wiki_id).present? ? "http://en.wikipedia.org/wiki/" + metabolite.link_set.try(:wiki_id) : ""
          ]
        elsif row[0].present?
          csv << [row[0]]
        end
      end
    end
  end

  desc "Grab ids for components"
  task :component_ids => [:environment] do
    CSV.open(Rails.root + "public/component_hmdbs.csv", "wb") do |csv|
      CSV.foreach(Rails.root + "public/component_names.csv") do |row|
        metabolite = Metabolite.find_by_name(row[0])
        if metabolite.present?
          csv << [row[0], row[1], metabolite.hmdb_id]
        else
          csv << [row[0], row[1], ""]
        end
      end
    end
  end

  desc "Grab references and other data for t3db"
  task :get_data => [:environment] do
    CSV.open(Rails.root + "public/toxcast_hmdb_data_2.csv", "wb") do |csv|
      csv << ["T3DB ID", "Common Name", "DrugBank ID", "HMDB ID", "HMDB Synonyms", "DrugBank Synonyms", "DrugBank Brands", "Synthesis Reference"]
      CSV.open(Rails.root + "public/toxcast_hmdb_refs_2.csv", "wb") do |refs_csv|
        refs_csv << ["T3DB ID", "Common_Name", "HMDB_ID", "Name", "PubMed ID", "Link", "ISBN"]
        CSV.foreach(Rails.root + "public/toxcast_additions.csv", headers: true) do |row|

          if row[3].present?
            metabolite = Metabolite.where(hmdb_id: row[3].strip).first

            if metabolite.blank?
              puts "MISSING " + row[1] + ": " + row[3]
              next
            end
          
            csv << [row[0], row[1], row[2], row[3], 
              metabolite.synonyms.present? ? "\"" + metabolite.synonyms.map(&:synonym).join("\", \"") + "\""  : "", 
              "", "", metabolite.synthesis_reference]

            # Add References
            metabolite.articles.each do |a|
              refs_csv << [
                row[0],
                row[1],
                row[3],
                a.citation,
                a.pubmed_id,
                "",
                ""
              ]
            end
            metabolite.textbooks.each do |t|
              refs_csv << [
                row[0],
                row[1],
                row[3],
                t.title,
                "",
                "",
                t.isbn
              ]
            end
            metabolite.external_links.each do |l|
              refs_csv << [
                row[0],
                row[1],
                row[3],
                l.name,
                "",
                l.url,
                ""
              ]
            end
          else
            csv << [row[0], row[1], row[2], row[3], "", "", "", ""]
          end
        end
      end
    end
  end

end
