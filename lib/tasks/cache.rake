namespace :cache do
  desc "Clear the redis cache"
  task clear: [:environment] do
    Rails.cache.clear
  end
end

