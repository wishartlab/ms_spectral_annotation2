namespace :sanitycheck do

  desc "Mine PMID from compound description"
  task :PMID => [:environment] do
    
    total_des_pmid = 0
    missing_pmid = 0
    inserted_pmid = 0
    
    Metabolite.find_each do |record|
      if (record.description.nil?)
        next
      end
      pmid_field = record.description.scan(/PMID[:\s]*([\d\s,]+)/)
      pmid_field.each do |field|
        if (field.nil? || field[0].nil?)
          next
        end
        pmids = field[0].scan(/(\d+)/)
        pmids.each do |pmid|
#          puts pmid
#          puts "found #{pmid[0]} in #{record.id}"
           total_des_pmid = total_des_pmid + 1
           if (record.references.where(:pubmed_id=>pmid[0]).empty?)
             missing_pmid = missing_pmid + 1 
             puts "inserting PMID #{pmid[0]} to #{record.hmdb_id} (ID: #{record.id})"
             
             record.references << Reference.find_or_create_by_pubmed_id(pmid[0])
             inserted_pmid = inserted_pmid + 1 #if(record.save!)
             
           end
#          if (Metabolite.find_by_id(record.id).references.where(:pubmed_id => pmid[0]).exists?)
#            puts "WARNING: #{pmid[0]} IS NOT IN THE REFERENCE of #{record.hmdb_id}"
#          end
        end
      end
    
    end
       
    puts "scan finished found #{total_des_pmid} in compound descriptions of which #{missing_pmid} are missing from general reference.\nThe program inserted #{inserted_pmid} of them."

  end
  
  desc "fix a number of spellings in description"
  task :fixspell => [:environment] do
    Metabolite.find_each do |record|
      next if (record.description.nil?)
      
      # 1. Change all instances of “by DF MS/MS (Biocrates kit)” to “by DI-MS/MS (Biocrates kit)” 
      if (record.description =~ /MS\/MS \(Biocrates kit\)/)
        puts "found and fixed spelling in #{record.hmdb_id} problem: DI-MS/MS format"
        record.description.gsub(/DF MS\/MS \(Biocrates kit\)/,"DI-MS/MS (Biocrates kit)")
      end
    end
  end
end