namespace :pubchem do
  desc "Export entries to SDF format suitable for PubChem upload"
  task :export => [:environment] do
    require 'open-uri'
    require 'moldb'
        
    file = File.open('data/pubchem.sdf', 'w')
    Metabolite.all.each do |metabolite|
      begin
        sr = StructureResource.find( metabolite.id )
      rescue Exception #ActiveResource::ResourceNotFound 
        next
      end
      
      puts "http://structures.wishartlab.com/molecules/#{metabolite.id}.mol"
      mol = open("http://structures.wishartlab.com/molecules/#{metabolite.id}.mol")
      sdf = pubchem_sdf(metabolite, mol.read)
      mol.close
      file << sdf
      puts "Processed #{metabolite.id}"
    end
    file.close
  end
end

def pubchem_sdf(metabolite, mol)
  synonyms = metabolite.synonyms.uniq.delete_if { |s| s.downcase == metabolite.name.downcase }.unshift(metabolite.name)
  hmdb_id = metabolite.id
  
  comments = Array.new
  comments << "Disorders: #{metabolite.diseases.collect { |d| d.name.gsub(/,/, ';') }.join(', ')}" if metabolite.diseases.present?
  comments << "Biofluids: #{metabolite.biofluid_locations.join(', ')}" if metabolite.biofluid_locations.present?

  pubmed_ids = []
  if metabolite.link_set.present?
    pubmed_ids = metabolite.link_set.refs.to_s.strip.split(/; /).delete_if { |r| r.to_i <= 0 }
  end

  sdf = ''
  sdf << mol
  
  sdf << "> <PUBCHEM_EXT_DATASOURCE_REGID>\n#{hmdb_id}\n\n"
  sdf << "> <PUBCHEM_SUBSTANCE_COMMENT>\n#{comments.join("\n")}\n\n" if comments.present?
  sdf << "> <PUBCHEM_EXT_DATASOURCE_URL>\nhttp://hmdb.ca\n\n"
  sdf << "> <PUBCHEM_EXT_SUBSTANCE_URL>\nhttp://hmdb.ca/metabolites/#{hmdb_id}\n\n"
  sdf << "> <PUBCHEM_SUBSTANCE_SYNONYM>\n#{synonyms.join("\n")}\n\n" unless synonyms.blank?
  sdf << "> <PUBCHEM_PUBMED_ID>\n#{pubmed_ids.join("\n")}\n\n" unless pubmed_ids.blank?
  sdf << "$$$$\n"
end
