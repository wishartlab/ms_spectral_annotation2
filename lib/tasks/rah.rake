require 'nokogiri'

namespace :rah do
  # remove empty reaction elements and their reactions
  desc "remove bad reactions"
  task :remove_bad_reactions => [:environment] do
    ReactionElement.where(altext: "").all.each do |bad_element|
      bad_element.reaction.destroy if not bad_element.reaction.nil?
    end
  end


  # add enzymes for glucuronides
  
  desc "add enzymes for glucuronides"
  task :glucuronides => [:environment] do
    list = %w[HMDB00127
    HMDB00496
    HMDB00580
    HMDB00639
    HMDB00652
    HMDB00663
    HMDB00693
    HMDB00935
    HMDB01013
    HMDB01204
    HMDB01272
    HMDB01394
    HMDB02061
    HMDB02091
    HMDB02429
    HMDB02430
    HMDB02472
    HMDB02513
    HMDB02545
    HMDB02577
    HMDB02579
    HMDB02596
    HMDB02704
    HMDB02829
    HMDB03141
    HMDB03193
    HMDB03325
    HMDB03363
    HMDB03402
    HMDB03976
    HMDB04483
    HMDB04484
    HMDB06203
    HMDB06224
    HMDB06765
    HMDB06766
    HMDB10313
    HMDB10315
    HMDB10316
    HMDB10317
    HMDB10318
    HMDB10319
    HMDB10320
    HMDB10321
    HMDB10322
    HMDB10323
    HMDB10324
    HMDB10325
    HMDB10326
    HMDB10327
    HMDB10328
    HMDB10329
    HMDB10330
    HMDB10332
    HMDB10333
    HMDB10335
    HMDB10336
    HMDB10337
    HMDB10338
    HMDB10339
    HMDB10340
    HMDB10341
    HMDB10342
    HMDB10344
    HMDB10345
    HMDB10346
    HMDB10348
    HMDB10349
    HMDB10350
    HMDB10351
    HMDB10352
    HMDB10353
    HMDB10354
    HMDB10355
    HMDB10356
    HMDB10357
    HMDB10358
    HMDB10359
    HMDB10360
    HMDB10361
    HMDB10362
    HMDB10363
    HMDB10364
    HMDB10365
    HMDB10366
    HMDB11658
    HMDB11686
    HMDB12302
    HMDB12304
    HMDB13847
    HMDB14551
    HMDB14709
    HMDB15356
    HMDB29212
    HMDB29214
    HMDB29357
    HMDB29406
    HMDB29500
    HMDB29844
    HMDB29881
    HMDB29917
    HMDB29923
    HMDB29938
    HMDB29940
    HMDB30498
    HMDB30499
    HMDB30976
    HMDB30977
    HMDB31022
    HMDB31023
    HMDB31024
    HMDB31025
    HMDB31026
    HMDB31027
    HMDB31353
    HMDB31656
    HMDB31883
    HMDB31884
    HMDB31959
    HMDB31960
    HMDB31961
    HMDB31965
    HMDB33105
    HMDB33106
    HMDB34379
    HMDB34389
    HMDB34390]
    
    list.each do |s|
      meta = Metabolite.find_by_hmdb_id(s)
      puts "update #{s} id #{meta.id}"
      meta.metabolite_protein_links.create!(protein_id: 5725) if meta.metabolite_protein_links.where(protein_id: 5725).empty?
      meta.metabolite_protein_links.create!(protein_id: 5732) if meta.metabolite_protein_links.where(protein_id: 5732).empty?
      meta.metabolite_protein_links.create!(protein_id: 5726) if meta.metabolite_protein_links.where(protein_id: 5726).empty?
      meta.metabolite_protein_links.create!(protein_id: 5696) if meta.metabolite_protein_links.where(protein_id: 5696).empty?
      meta.metabolite_protein_links.create!(protein_id: 5721) if meta.metabolite_protein_links.where(protein_id: 5721).empty?
      meta.metabolite_protein_links.create!(protein_id: 5719) if meta.metabolite_protein_links.where(protein_id: 5719).empty?
    end
  end
  
  desc "change hmdb ids to metabolite ids in metabolite_name_map table\n"
  task :convert_hmdb_id_to_meta_id => [:environment] do
    MetaboliteNameMap.all.each do |map|
      map.hmdb_id = Metabolite.find_by_hmdb_id(map.hmdb_id).id
      map.save!
    end
  end

  # the script does following:
  # change <br/> tags in description of new cards to \n
  # add \n to new descriptions in places where br tags were removed
  desc "change br tags to new line (and add them if removed) in description\n"
  task :fix_des_breaks => [:environment] do
    auto_fix = 0
    man_fix = 0
    
    OldMetabolite.all.each do |old|
      old_des = old.description
      new_des = old.current_metabolite.description if old.current_metabolite.present?
      update = false
      if old.current_metabolite.present? && old.current_metabolite.export_to_hmdb && new_des.present? && old_des != new_des
        old_tags = old_des.scan(/\<\s*[Bb][Rr]\s*\/?\s*\>/)
        new_tags = new_des.scan(/\<\s*[Bb][Rr]\s*\/?\s*\>/)
        
        # case 1: no br tag in either old or new description
        if old_tags.empty? && new_tags.empty?
          # pass

        # case 2: tags in new description but not in old
        elsif old_tags.empty? && new_tags.empty? == false
          # change tags to \n
          new_des.gsub!(/<\s*[Bb][Rr]\s*\/?\s*>\s*/, "\n\n")
          update = true

        # case 3: tags in old description but removed in new description
        elsif old_tags.empty? == false && new_tags.empty?
          # print out description for manually correction
          man_fix += 1
          puts "manual fix site \# #{man_fix}"
          puts old.current_metabolite.hmdb_id
          puts old_des
          puts ""
          puts new_des
          puts ""
          

        # case 4: tags in both new and old descriptions
        else
          # change tags to \n
          new_des.gsub!(/<\s*br\s*\/?\s*>\s*/, "\n\n")
          update = true

        end
      end
      
      if update
        auto_fix += 1
        puts "auto replace br tags for #{old.id}"
        record = Metabolite.find_by_hmdb_id(old.current_metabolite.hmdb_id)
        record.description = new_des
        record.save!
      end
      
    end
    puts "number of auto fixes = #{auto_fix}"
    puts "number of manual fixes = #{man_fix}"
  end
  
  
  @uniprot_id = Hash.new

  desc "Extract list of metabolite to add before import reactions"
  task :extract_metabolites => [:environment] do
    total = 0
    name = Array.new
    inchi = Array.new
    
    # NEW: loaded the normalized metabolite list
    old_metabolites= Hash.new
    File.open("data/import/hmdb_normalize.csv", "r").each_line do |line|
      if line =~ /^(HMDB\d+),(InChI=.+),(InChI=.+)\n$/
        old_metabolites[$3] = $1
      end
    end
    
    # read local XML file "human.xml"
    doc = Nokogiri::XML(File.open("data/import/human.xml"))
    
    doc.xpath("/proteins/protein").each do |proteins|
      # add metabolites from both sides of reactions to hash
      proteins.xpath("./reactions/reaction").each do |reaction|
        reaction.xpath("./left_elements/element").each do |element|
          if element.xpath("./structure/inchi").text.present?
            name[total] = element.xpath("./name").text
            inchi[total] = element.xpath("./structure/inchi").text
            total = total + 1
          end
        end
        reaction.xpath("./right_elements/element").each do |element|
          if element.xpath("./structure/inchi").text.present?
            name[total] = element.xpath("./name").text
            inchi[total] = element.xpath("./structure/inchi").text
            total = total + 1
          end
        end  
      end
      
      proteins.xpath("./transports/transport").each do |transport|
        transport.xpath("./elements/element").each do |element|
          if element.xpath("./structure/inchi").text.present?
            name[total] = element.xpath("./name").text
            inchi[total] = element.xpath("./structure/inchi").text
            total = total + 1
          end
        end
      end
    end
    
    puts "Reactions in the XML file contain #{total} metabolites"
    (1..total).each do |i|
      if Metabolite.find_by_moldb_inchi(inchi[i]).present?
        # already exist in HMDB
      elsif MetaboliteNameMap.find_by_inchi(inchi[i]).present?
        # already in alternative inchi lookup table
      elsif old_metabolites[inchi[i]].present?
        # already exist in normalized file
      elsif Metabolite.find_by_name(name[i]).present?
        # metabolites that have different structure
        old_str = Metabolite.find_by_name(name[i]).moldb_inchi
        new_str = inchi[i]
        
        puts "ALT\t#{new_str}\t#{name[i]}"
      else
        # new metabolite
        puts "NEW\t#{name[i]}\t#{inchi[i]}"
      end
        
    end
  end
  
  desc "Insert new metabolite inchi into inchi lookup table"
  task :update_inchi_map => [:environment] do
    File.open("data/import/inchi_map.tsv", "r").each_line do |line|
      parts = line.chomp.split("\t")
      if MetaboliteNameMap.find_by_inchi(parts[0]).nil?
        puts "insert new inchi for #{parts[1]}"
        MetaboliteNameMap.create!(inchi: parts[0], metabolite_id: Metabolite.find_by_hmdb_id(parts[1]).id)
      else
        puts "#{parts[1]} already exist"
      end
    end
  end
  
  desc "Tidy up punctuation surrounding PMID references in descriptions"
  task :fix_puncts => [:environment] do
    puts "Total #{Metabolite.where(:export_to_hmdb => true).count} records"
    modified = 0
    Metabolite.where(:export_to_hmdb => true).each do  |meta|
      dis = meta.description
      if dis.present?
        # reformat space
        dis = dis.gsub(/\(\s*PMID +([0-9]+)\s*\)/, '(PMID: \1)')
        # fix ':' mistake
        dis = dis.gsub(/\(\s*PMID[^:] +([0-9]+)\s*\)/, '(PMID: \1)')
        # tidy up
        dis = dis.gsub(/\(PMID: ([0-9]+)\)\s*\./, '(PMID: \1).')
        dis = dis.gsub(/\(PMID: ([0-9]+)\)\s*([^\.])/, '(PMID: \1). \2')
        if meta.description != dis
           meta.description = dis
           meta.save!
           modified = modified + 1
        end
      end
    end
    puts "Fixed #{modified} descriptions" 
  end
  
  desc "Extract pubmed references from descriptions and add to general refs"
  task :extract_references => [:environment] do
    total = 0
    Metabolite.where(:export_to_hmdb => true).each do  |meta|
      dis = meta.description
      if dis.present?
        PMIDs = dis.scan(/\(PMID: ([0-9,\s]+)\)/)
        if PMIDs.present?
          PMIDs.each do |pmid|
            ids = pmid.first.scan(/(\d+)/)
            ids.each do |id|
              if meta.articles.where(:pubmed_id => id.first).empty?
                puts "Add PMID #{id.first} to #{meta.hmdb_id}"
                meta.articles << Article.find_or_create_by_pubmed_id(id.first)
                total = total + 1  
              end
            end
          end
        end
        
      end
    end
    puts "Added #{total} PMID references from descriptions"
  end
  
  desc "import protein data in XML format generated from datawraggler "
  task :import_proteins => [:environment] do
    
    # NEW: loaded the normalized metabolite list
    old_metabolites= Hash.new
    File.open("data/import/hmdb_normalize.csv", "r").each_line do |line|
      if line =~ /^(HMDB\d+),(InChI=.+),(InChI=.+)$/
        old_metabolites[$3] = $1
      end
    end
    
    # read local XML file "human.xml"
    doc = Nokogiri::XML(File.open("data/import/human.xml"))
    
    doc.xpath("/proteins/protein").each do |proteins|
      # update proteins table using uniprot_id as the key
      uniprot_id = proteins.xpath("./uniprot_id").text # this is used as key
      puts "importing protein #{uniprot_id}"
      
      if uniprot_id.nil?
        abort ("XML contains protein record missing critical tag: uniprot_id")
      end
      
      protein_record = Protein.find_or_initialize_by_uniprot_id(uniprot_id)
      
      # set up singlar fields
      protein_record.name = proteins.xpath("./name").text
      protein_record.kegg_id = proteins.xpath("./kegg_id").text
      protein_record.hgnc_id = proteins.xpath("./hgnc_id").text
      protein_record.subunit = proteins.xpath("./subunit").text
      protein_record.gene_name = proteins.xpath("./gene_name").text
      protein_record.meta_cyc_id = proteins.xpath("./meta_cyc_id").text
      protein_record.theoretical_pi = proteins.xpath("./isoelectric_point").text
      protein_record.molecular_weight = proteins.xpath("./molecular_weight").text
      protein_record.specific_function = proteins.xpath("./function").text
      protein_record.tissue_specificity = proteins.xpath("./tissue_specificity").text
      
      # set up clustered fields
      protein_record.chromosome_location = proteins.xpath("./gene_sequence_properties/chromosome").to_ary.join(";")
      protein_record.locus = proteins.xpath("./gene_sequence_properties/locus").to_ary.join(";")
      protein_record.pdb_ids = proteins.xpath("./pdb/id").to_ary.join(";")
      protein_record.cofactor = proteins.xpath("./cofactors/cofactor").to_ary.join(";")
      
      # set up ncbi_sequence_ids
      ncbi_sequence_ids = Array.new
      proteins.xpath("./ncbi/sequence_reference").each do |sequence_reference|
        ncbi_sequence_ids.push("#{sequence_reference.xpath("./protein").text}:#{sequence_reference.xpath("./mrna").first.text}")
      end
      protein_record.ncbi_sequence_ids = ncbi_sequence_ids.join(";")

      # set up transmembrane_regions
      tran_regions = Array.new
      proteins.xpath("./polypeptide_sequence_properties/transmembrane_regions/region").each do |region|
        if region.xpath("./star").text.present? and region.xpath("./end").text.present?
          tran_regions.push("#{region.xpath("./star").text}-#{region.xpath("./end").text}")
        end
      end
      protein_record.transmembrane_regions = tran_regions.join(";")
      
      # set up signal_regions
      sig_regions = Array.new
      proteins.xpath("./polypeptide_sequence_properties/signal_regions/region").each do |region|
        if region.xpath("./star").text.present? and region.xpath("./end").text.present?
          sig_regions.push("#{region.xpath("./star").text}-#{region.xpath("./end").text}")
        end
      end
      protein_record.signal_regions = sig_regions.join(";")
      
      # update protein_record and get its id
      protein_record.save!
      protein_id = protein_record.id
      
      ### update other tables ###
      
      # update 
      proteins.xpath("./synonyms/synonym").each do |synonym|
        if synonym.text.present? && protein_record.synonyms.where(:synonym => synonym.text).present? == false
          protein_record.synonyms.create!(synonym: synonym.text)
        end
      end
      
      # update enzyme_classes table
      proteins.xpath("./enzyme_classes/ec_number").each do |ec_number|
        if ec_number.text.present? && protein_record.enzyme_classes.where(:ec => ec_number.text).present? == false
          protein_record.enzyme_classes.create!(ec: ec_number.text)
        end
      end
      
      # update subcellular_locations table
      cellular_locations = Array.new
      proteins.xpath("./subcellular_locations/location").each do |location|
        if location.text.present?
          cell = CellularLocation.find_or_initialize_by_name(location.text)
          cell.save!
          cellular_locations.push(cell.id)
        end
      end
      protein_record.cellular_location = cellular_locations.join(";")
      
      # update pathway table
      # the pathway table is different, new roll/update is made only if the SMPDB entry is non-empty
      # i.e. the process will not affect rows that contain SMPDB ids
      proteins.xpath("./pathways/pathway").each do |pathway|
        pathway_name = nil
        kegg_id = nil
        
        pathway_record = nil
        
        pathway_name = pathway.xpath("./name").text.strip if pathway.xpath("./name").text.present?
        kegg_id = pathway.xpath("./kegg_id").text.strip if pathway.xpath("./kegg_id").text.present?
               
        if kegg_id.nil? == false
          pathway_record = Pathway.where("kegg_id = ? AND smpdb_id IS NULL", kegg_id).all
          if pathway_record.size == 1
            pathway_record = pathway_record.first
          elsif pathway_record.size == 0
            pathway_record = Pathway.new
          else
            puts "multiple records for KEGG #{kegg_id} (SMPDB excluded)"
            next
          end
        else
          pathway_record = Pathway.where("name = ? AND smpdb_id IS NULL", pathway_name).all
          if pathway_record.size == 1
            pathway_record = pathway_record.first
          elsif pathway_record.size == 0
            pathway_record = Pathway.new
          else
            # generate a warning message as pathways should be unique
            puts "multiple records for name #{pathway_name} (SMPDB excluded)"
            next
          end
        end
        
        pathway_record.kegg_id = kegg_id if kegg_id.present?
        pathway_record.name = pathway_name if pathway_name.present?
        pathway_record.unipathway_id = pathway.xpath("./unipathway_id").text if pathway.xpath("./unipathway_id").text.present?

        pathway_record.save!
        
        # update pathway_links table
        pathway_link = PathwayLink.where(pathway_id: pathway_record.id, element_type: "Protein", element_id: protein_record.id).first
        if pathway_link.nil?
          pathway_link = PathwayLink.new(pathway_id: pathway_record.id, element_type: "Protein", element_id: protein_record.id)
          pathway_link.save!
        end
        
      end
      
      
      # update go_classes table and protein_go_classes table
      proteins.xpath("./go_annotations/gene_ontology").each do |gene_ontology|
        if gene_ontology.xpath("./id").text.present?
          go_class = GoClass.find_or_initialize_by_go_id(gene_ontology.xpath("./id").text)
          go_class.category = gene_ontology.xpath("./type").text
          go_class.description = gene_ontology.xpath("./description").text
          go_class.save!

          if protein_record.protein_go_classes.where(:go_class_id => go_class.id).present? == false
            protein_record.protein_go_classes.create!(:go_class_id => go_class.id)
          end
        end
        
      end
      
      # update pfams table
      proteins.xpath("./pfams/pfam").each do |pfam|
        pfam_id = pfam.xpath("./pfam_id").text
        if pfam_id.present?
          pfam_record = protein_record.pfams.where(:identifier => pfam_id)
          if pfam_record.present? == false
            if protein_record.pfams.create(:identifier => pfam_id, :name => pfam.xpath("./name").text) == false
              puts "WARNING: PFAM has duplicate names for identifier #{pfam_id} the new name is #{pfam.xpath("./name").text}"
            end
          end
        end
      end
      
      # update polypeptide_sequence table
      protein_seq = proteins.xpath("./polypeptide_sequence").text
      if protein_seq.present?
        header = protein_seq.split(/\n+/)[0]
        body = protein_seq.split(/\n+/)[1]
        if protein_record.polypeptide_sequence.present?
          protein_record.polypeptide_sequence.chain = body
          protein_record.polypeptide_sequence.header = header
        else
          protein_record.polypeptide_sequence = PolypeptideSequence.new(:chain => body, :header => header)
        end
      end
      
      # fill in reaction related tables
      proteins.xpath("./reactions/reaction").each do |reaction|
        reaction_record = protein_record.reactions.new # assume fresh import
        reaction_record.altext = reaction.xpath("./text").text if reaction.xpath("./text").text.present?
        reaction_record.kegg_reaction_id = reaction.xpath("./kegg_id").text if reaction.xpath("./kegg_id").text.present?
        reaction_record.direction = reaction.xpath("./direction").text if reaction.xpath("./direction").text.present?
        reaction_record.spontaneous = reaction.xpath("./spontaneous").text if reaction.xpath("./spontaneous").text.present?
        
        reaction_record.save! # save so we can get its id
        
        export_reaction = true # this value will be set at last and we will save reaction again
        comment_reaction = "imported from XML file; "
        
        # fill in reaction_elements table
        
        reaction.xpath("./left_elements/element").each do |element|
          metabolite = nil
          if element.xpath("./name").text.length > 255
            comment_reaction = comment_reaction + "ERROR left element name too long; "
            export_reaction = false
            next
          end
          
          # add the metabolite if inchi present, otherwise ignore it
          if element.xpath("./structure/inchi").text.present?
            metabolite = Metabolite.find_by_moldb_inchi(element.xpath("./structure/inchi").text)
            if metabolite.nil? && old_metabolites[element.xpath("./structure/inchi").text].present?
              metabolite = Metabolite.find_by_hmdb_id(old_metabolites[element.xpath("./structure/inchi").text])
            end
            
            if metabolite.nil? && MetaboliteNameMap.find_by_inchi(element.xpath("./structure/inchi").text).present?
              metabolite = MetaboliteNameMap.find_by_inchi(element.xpath("./structure/inchi").text).metabolite
            end
            
            if metabolite.present? == false
              # cannot find metabolite
              abort "CANNOT FIND LEFT METABOLITE " + element.xpath("./name").text + " " + element.xpath("./structure/inchi").text
            end
            
            # add protein as enzyme 
            metabolite.metabolite_protein_links.create!(protein_id: protein_record.id) if metabolite.metabolite_protein_links.where(protein_id: protein_record.id).empty?
          end
          
          
          # add the metabilite to reaction element 
          if metabolite.nil? # add altext
            reaction_record.left_reaction_elements.create!(:altext => element.xpath("./name").text)
          else # add the structure
            sto = nil
            sto = element.xpath("./structure/stoichiometry").text if element.xpath("./structure/stoichiometry").text.present?
            reaction_record.left_reaction_elements.create!(:metabolite_id => metabolite.id, :stoichiometry => sto, :altext => metabolite.name)
          end
        end
        
        reaction.xpath("./right_elements/element").each do |element|
          metabolite = nil
          if element.xpath("./name").text.length > 255
            comment_reaction = comment_reaction + "ERROR right element name too long; "
            export_reaction = false
            next
          end
           
          # add the metabolite if inchi present, otherwise ignore it
          if element.xpath("./structure/inchi").text.present?
            metabolite = Metabolite.find_by_moldb_inchi(element.xpath("./structure/inchi").text)
            if metabolite.nil? && old_metabolites[element.xpath("./structure/inchi").text].present?
              metabolite = Metabolite.find_by_hmdb_id(old_metabolites[element.xpath("./structure/inchi").text])
            end
            
            if metabolite.nil? && MetaboliteNameMap.find_by_inchi(element.xpath("./structure/inchi").text).present?
              metabolite = MetaboliteNameMap.find_by_inchi(element.xpath("./structure/inchi").text).metabolite
            end
            
            if metabolite.present? == false
              # cannot find metabolite
              abort "CANNOT FIND RIGHT METABOLITE " + element.xpath("./name").text + " " + element.xpath("./structure/inchi").text
            end
            
            # add protein as enzyme 
            metabolite.metabolite_protein_links.create!(protein_id: protein_record.id) if metabolite.metabolite_protein_links.where(protein_id: protein_record.id).empty?
          end

          # add the metabilite to reaction element 
          if metabolite.nil? # add altext
            reaction_record.right_reaction_elements.create!( :altext => element.xpath("./name").text)
          else
            sto = nil
            sto = element.xpath("./structure/stoichiometry").text if element.xpath("./structure/stoichiometry").text.present?
            reaction_record.right_reaction_elements.create!(:metabolite_id => metabolite.id, :stoichiometry => sto, :altext => metabolite.name)
          end
        end
        
        reaction_record.export = export_reaction
        reaction_record.comment = comment_reaction
        reaction_record.save!
        
        # add reaction enzymes
        reaction_enzyme = reaction_record.reaction_enzymes.where(protein_id: protein_record.id)
        if reaction_enzyme.present? == false
          reaction_record.reaction_enzymes.create!(protein_id: protein_record.id)
        end
      end
      
      
      # fill in transport related tables
      proteins.xpath("./transports/transport").each do |transport|
        # assume transport table empty
        transport_record = protein_record.transports.new(:altext => transport.xpath("./text").text)
        transport_record.save!
       
        # fill in transport_metabolites table
        transport.xpath("./elements/element").each do |element|
          if element.xpath("./structure/inchi").text.present?
            metabolite = Metabolite.find_by_moldb_inchi(element.xpath("./structure/inchi").text)
            if metabolite.nil? && old_metabolites[element.xpath("./structure/inchi").text].present?
              metabolite = Metabolite.find_by_hmdb_id(old_metabolites[element.xpath("./structure/inchi").text])
            end
            
            if metabolite.nil? && MetaboliteNameMap.find_by_inchi(element.xpath("./structure/inchi").text).present?
              metabolite = MetaboliteNameMap.find_by_inchi(element.xpath("./structure/inchi").text).metabolite
            end
            
            if metabolite.present? == false
              # cannot find metabolite
              abort "CANNOT FIND transport METABOLITE " + element.xpath("./name").text + " " + element.xpath("./structure/inchi").text
            end
            
            if transport_record.transport_metabolites.where(:metabolite_id => metabolite.id).present? == false
              transport_record.transport_metabolites.create!(:metabolite_id => metabolite.id, :altext => element.xpath("./name").text)
            end
          else
            transport_record.transport_metabolites.create!(:altext => element.xpath("./name").text)
          end
          
          
        end
      end
         
      
    end
  end
  
  # helper method to print conflicted metabolites
  def resolve_conflict(meta)
    record = Metabolite.find_by_name(meta.name)
    puts "* NAME = #{meta.name}"
    puts "* R #{record.moldb_inchi}"
    puts "* M #{meta.moldb_inchi}"
  end
  
  # print all XML tags to STDOUT 
  def print_all_markup(doc)
    doc.xpath("//records").each do |r|
      print_all_node(r, "")
    end
    @uni.keys.each do |k|
      puts k
    end
  end
  
  # helper function to print MXL tags
  def print_all_node(node, path)
    cur_name = node.name
    
    if node.children.empty?
      if node.text.blank?
        
      else
        if cur_name == "text"
          @uni[path] = 1
        else
          puts "error"
        end
        
      end
    else
      node.children.each do |c|
        print_all_node(c, "#{path}#{cur_name} > ")
      end
    end
  end
end