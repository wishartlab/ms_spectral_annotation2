namespace :nmr_spectra do
  desc "Get HMDB NMR Spectra from moldb"
  task :text => [:environment] do
    
    metabolites = Metabolite.exported.where("id > 0")
    metabolites.each do |m|
      m.spectra.each do |s|
          s.documents.each do |d|
            if /List of chemical shift/.match(d.description) && d.class.to_s == "Specdb::NmrOneD::Document" && s.nucleus == "1H"
              File.open("public/nmr_one_shifts/" + m.hmdb_id + ".txt", "w") do |saved_file|
 		 # the following "open" is provided by open-uri
  		open(d.url, "r") do |read_file|
    		  saved_file.write(read_file.read)
                  saved_file.close
  		end
                #break
  	      end
              #File.open("public/nmr_one_shifts/" + m.hmdb_id + ".sdf", "w") do |sdf_file|
             #   sdf_file.write(m.to_sdf)
             #   sdf_file.close
             # end
	    end
	  end
      end
    end
  end
end

