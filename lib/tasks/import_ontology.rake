namespace :import_ontology do
  task :categories => [:environment] do

    CSV.foreach("ontology_categories.csv", :headers => true) do |line|
      name = line["category"].humanize
      level = line["level"].to_i
      parent = line["parent"]
      definition = line["definition"]

      if !category = OntologyTerm.find_by(term: name)
        category = OntologyTerm.new
        category.term = name
        category.level = level
      end
      if parent_category = OntologyTerm.find_by(term: parent)
        category.parent_id = parent_category.id
      end
      category.save!
      category.definition = definition
      category.save!

    end
  end

  task :term => [:environment] do

    missing = Array.new
    CSV.foreach("full_ontology_22-SepNK.csv", :headers => true, :encoding => 'windows-1251:utf-8') do |line|
        
      # puts line

      if line["level4"]
        categories = line["level4"].humanize
      elsif line["level3"]
        categories = line["level3"].humanize
      elsif line["level2"]
        categories = line["level2"].humanize
      else
        categories = line["level1"].humanize
      end

      term = line["TERM"].humanize
      definition = line["DEFINITION"]
      if line["SYNONYMS"]
        synonyms = line["SYNONYMS"].split("; ")
      else
        synonyms = Array.new
      end
      external_link = line["DB_SYNONYMS"]
      source = line["SOURCE"]
      comment = line["COMMENT"]
      curator = line["CURATOR"]

      # puts line

      if line["PARENT TERMS"]
        parent_terms = line["PARENT TERMS"].split("; ")
      else
        parent_terms = Array.new
      end

      related_terms = Array.new
      if line["ASSOCIATED PROCESS"]
        line["ASSOCIATED PROCESS"].split("; ").each do |r|
          related_terms.push(r)
        end
      end
      if line["ASSOCIATED ROLE"]
        line["ASSOCIATED ROLE"].split("; ").each do |r|
          related_terms.push(r)
        end
      end
      if line["ASSOCIATED PHYSIOLOGICAL EFFECT"]
        line["ASSOCIATED PHYSIOLOGICAL EFFECT"].split("; ").each do |r|
          related_terms.push(r)
        end
      end
      if line["ASSOCIATED BIOLOGICAL LOCATION"]
        line["ASSOCIATED BIOLOGICAL LOCATION"].split("; ").each do |r|
          related_terms.push(r)
        end
      end

      categories.split("; ").each do |category|
        category = category.humanize
        next if term == category

        if !ont_cat = OntologyTerm.find_by(term: category)
          # puts "category does not exist"
          # puts category
          missing.push(category) if !missing.include?(category)

          next
        end

        if !ont_term = OntologyTerm.find_by(term: term)
          ont_term = OntologyTerm.new
        end
        ont_term.term = term
        ont_term.definition = definition
        ont_term.external_id = external_link
        ont_term.external_source = source
        ont_term.comment = comment
        ont_term.curator = curator

        ont_term.parent_id = ont_cat.id
        ont_term.level = ont_cat.level + 1
        
        ont_term.save!

        # if !link = OntologyTermCategory.find_by(ontology_term_id: ont_term.id, ontology_category_id: ont_cat.id)
        #   link = OntologyTermCategory.new
        #   link.ontology_term_id = ont_term.id
        #   link.ontology_category_id = ont_cat.id

        #   link.save!
        # end
      end
    end

    puts missing
  end

  task :links => [:environment] do

    log_file = "ontology.log"
    Dir.glob("ontology_mappings_for_upload/Other/*.csv").each do |file|
      # next if file != "ontology_mappings_for_upload/Other/ripped_data_mappings_v2.csv"
      puts file
      ontology_log = File.open(log_file, "a")
      ontology_log.puts(file)
      CSV.foreach(file, :headers => true, :encoding => 'windows-1251:utf-8') do |line|

        hmdb_id = line["HMDB_id"]
        name = line["External_name"] ||= ""
        term = line["Term"]
        external_link = line["External Identifiers"]
        reference = line["Reference"] ||= line["References"]
        source = line["Source"]
        # external_id = line[""]
        # z_score = line["Zscore"]

        next if !term
        term = term.humanize

        if !ont_term = OntologyTerm.find_by(term: term)
          if !ont_term = OntologyTerm.find_by(term: term[0..-2])
            if !ont_syn = OntologySynonym.find_by(synonym: term)
              ont_syn = OntologySynonym.find_by(synonym: term[0..-2])
            end
            if ont_syn
              ont_term = ont_syn.ontology_term
              ontology_log.puts("SYNONYM" + term + " "+ont_term.term)
              puts "SYNONYM" + term + " "+ont_term.term
            end
          end
        end
        if !ont_term
          ontology_log.puts("NOT FOUND: "+term)
          puts term
          next
        end

        # ont_term_categories = OntologyTermCategory.where(ontology_term_id: ont_term.id)
        if !metabolite = Metabolite.find_by(hmdb_id: hmdb_id)
          metabolite = Metabolite.find_by(name: name)
        end
        if !metabolite = Metabolite.find_by(hmdb_id: hmdb_id)
          begin
            while hmdb_id.length < 11
              hmdb_id.insert(4, "0")
            end
          rescue
            puts "No HMDB_ID for #{name} #{term}"
          end
        end
        if !metabolite = Metabolite.find_by(hmdb_id: hmdb_id)
          puts "Metabolite does NOT exist #{hmdb_id} #{name} #{term}"
          next
        end

        # ont_term_categories.each do |cat|

          if !ont_term_met = MetaboliteOntologyTerm.find_by(metabolite_id: metabolite.id, ontology_term_id: ont_term.id)
            ont_term_met = MetaboliteOntologyTerm.new
          end
          ont_term_met.metabolite_id = metabolite.id
          ont_term_met.ontology_term_id = ont_term.id
          ont_term_met.export = 1
          ont_term_met.save!

          pmid = nil
          site = nil

          if reference
            if reference.include?("pubmed")
              begin
                pmid = reference.split(":")[1].strip.to_i
              rescue
                ontology_log.puts("Cannot parse reference: "+reference)
              end
            elsif reference.include?("www")
              site = reference.split(".")[1].humanize
            end
          end
          if pmid.blank? && source
            if source.include?("www")
              reference = source
              site = reference.split(".")[1].humanize
            elsif source.include?("HMDB")
              pmid = 29140435
            elsif source.include?("Classyfire")
              pmid = 27867422
            end
          end

          if !pmid.blank?
            article = CiteThis::Article.where(pubmed_id: pmid).first_or_create!
            if !ont_term_met.articles.include?(article)
              ont_term_met.articles << article
              ont_term_met.save!
            end
          # elsif !site.blank?
          #   if !elink = CiteThis::ExternalLink.find_by(url: reference)
          #     elink = CiteThis::ExternalLink.new
          #     elink.url = reference
          #     elink.name = site
          #     elink.save!
          #   end
          #   if !ont_term_met.external_links.include?(elink)
          #     ont_term_met.external_links << elink
          #     ont_term_met.save!
          #   end
          end

        # end
      end
      ontology_log.close
    end
  end

  task :delete_links => [:environment] do
    terms = ["Poison","Death"]
    terms.each do |t|
      ont_term = OntologyTerm.find_by(term: t)
      # ont_cats = OntologyTermCategory.where(ontology_term_id: ont_term.id)

      melatonin_links = MetaboliteOntologyTerm.where(ontology_term_id: ont_term.id)
      melatonin_links.each do |m|
        m.destroy!
      end
    end
  end

  task :links_lipids => [:environment] do

    log_file = "ontology.log"
    Dir.glob("ontology_mappings_for_upload/Lipids/*").each do |file|
      ontology_log = File.open(log_file, "a")
      puts file
      ontology_log.puts(file)
      CSV.foreach(file, :headers => true, :encoding => 'windows-1251:utf-8') do |line|

        begin
          hmdb_id = line["HMDB_id"]
          term = line["Location"].split("\\")[-1].humanize
          source = line["Source"]
          reference = line["Reference"] ||= line["References"]
        rescue Exception => e
          puts("Cannot parse: "+line + ": "+e)
          ontology_log.puts("Cannot parse: "+line + ": "+e)
          next
        end

        term = term.humanize
        if !ont_term = OntologyTerm.find_by(term: term)
          if !ont_term = OntologyTerm.find_by(term: term[0..-2])
            puts term
            ontology_log.puts(term)
            next
          end
        end
        # ont_term_categories = OntologyTermCategory.where(ontology_term_id: ont_term.id)
        if !metabolite = Metabolite.find_by(hmdb_id: hmdb_id)
          begin
            while hmdb_id.length < 11
              hmdb_id.insert(4, "0")
            end
          rescue
            puts "No HMDB_ID for #{term}"
          end
        end
        if !metabolite = Metabolite.find_by(hmdb_id: hmdb_id)
          puts "Metabolite does NOT exist #{hmdb_id}"
          next
        end

        # ont_term_categories.each do |cat|

          if ont_term.term == "Extracellular"
            if ont_term_met = MetaboliteOntologyTerm.find_by(metabolite_id: metabolite.id, ontology_term_id: ont_term.id)
              ont_term_met.destroy!
            end
            # next
          end

          if !ont_term_met = MetaboliteOntologyTerm.find_by(metabolite_id: metabolite.id, ontology_term_id: ont_term.id)
            ont_term_met = MetaboliteOntologyTerm.new
          end
          ont_term_met.metabolite_id = metabolite.id
          ont_term_met.ontology_term_id = ont_term.id
          ont_term_met.export = 1
          ont_term_met.save!

          pmid = nil
          site = nil

          if reference
            if reference.include?("pubmed")
              begin
                pmid = reference.split(":")[1].strip.to_i
              rescue
                ontology_log.puts("Cannot parse reference: "+reference)
              end
            elsif reference.include?("www")
              site = reference.split(".")[1].humanize
            end
          end
          if pmid.blank? && source
            if source.include?("www")
              reference = source
              site = reference.split(".")[1].humanize
            elsif source.include?("HMDB")
              pmid = 29140435
            elsif source.include?("Classyfire")
              pmid = 27867422
            end
          end

          if !pmid.blank?
            article = CiteThis::Article.where(pubmed_id: pmid).first_or_create!
            if !ont_term_met.articles.include?(article)
              ont_term_met.articles << article
              ont_term_met.save!
            end
          elsif !site.blank?
            if !elink = CiteThis::ExternalLink.find_by(url: reference)
              elink = CiteThis::ExternalLink.new
              elink.url = reference
              elink.name = site
              elink.save!
            end
            if !ont_term_met.external_links.include?(elink)
              ont_term_met.external_links << elink
              ont_term_met.save!
            end
          end
        # end
      end
      ontology_log.close
    end
  end

  task :update => [:environment] do

    missing = Array.new
    files = ["Export_file_24-Sep_1_1.csv", "Export_file_25-Sep.csv", "Export_file_27-Sep.csv", "Export_file_NK_02-Oct1.1.csv", "Export_file_02-Oct_Hasan.csv", "Export_file_03-Oct_Hasan.csv", "Export_file_NK_03-Oct_after_Hasan_Only.csv", "Export_file_02-Dec_Ana.csv"]

    files.each do |file|
      CSV.foreach(file, :headers => true, :encoding => 'windows-1251:utf-8') do |line|
        next if !line["TERM"]
        action = line["action required"]

        if line["level4"]
          categories = line["level4"].humanize
        elsif line["level3"]
          categories = line["level3"].humanize
        elsif line["level2"]
          categories = line["level2"].humanize
        else
          categories = line["level1"].humanize
        end

        term = line["TERM"].humanize
        definition = line["DEFINITION"]
        if line["SYNONYMS"]
          synonyms = line["SYNONYMS"].split("; ")
        else
          synonyms = Array.new
        end
        external_link = line["DB_SYNONYMS"]
        # source = line["SOURCE"]
        source = line["ORIGINAL_DEFINITION_SOURCE"]
        comment = line["COMMENT"]
        curator = line["CURATOR"]
        
        categories.split("; ").each do |category|
          category = category.humanize
          if action == "DELETE"
            if !ont_term = OntologyTerm.find_by(term: term)
              next
            elsif ont_term.parent.term != category
              next
            else
              ont_term.destroy!
            end
            # end
            # if !ont_cat = OntologyTerm.find_by(name: category)
            #   next
            # end
            # if link = OntologyTermCategory.find_by(ontology_term_id: ont_term.id, ontology_category_id: ont_cat.id)
            #   metabolite_links = MetaboliteOntologyTerm.where(ontology_term_category_id: link.id)
            #   metabolite_links.each do |m|
            #     m.destroy!
            #   end
            #   link.destroy!
            # end
            # if !link = OntologyTermCategory.find_by(ontology_term_id: ont_term.id)
            #   ont_term.destroy!
            # end
          else
            if !ont_cat = OntologyTerm.find_by(term: category)
              # puts "category does not exist"
              # puts category
              missing.push(category) if !missing.include?(category)

              next
            end

            if !ont_term = OntologyTerm.find_by(term: term)
              ont_term = OntologyTerm.new
            end
            ont_term.term = term
            ont_term.definition = definition
            ont_term.external_id = external_link
            ont_term.external_source = source
            ont_term.comment = comment
            ont_term.curator = curator
            ont_term.parent_id = ont_cat.id
            ont_term.level = ont_cat.level + 1

            ont_term.save!

            # if !link = OntologyTermCategory.find_by(ontology_term_id: ont_term.id, ontology_category_id: ont_cat.id)
            #   link = OntologyTermCategory.new
            #   link.ontology_term_id = ont_term.id
            #   link.ontology_category_id = ont_cat.id

            #   link.save!
            # end
          end
        end
      end
    end
    puts missing

  end

  task :pathways => [:environment] do
    pathways = Pathway.all
    pathways.each do |pathway|
      metabolites = PathwayLink.where(pathway_id: pathway.id, element_type: "Metabolite")

      if pathway.name.include?("Cardiolipin Biosynthesis")
        ont_path = Pathway.find_by(name: "Cardiolipin Biosynthesis")
      elsif pathway.name.include?("De Novo Triacylglycerol Biosynthesis")
        ont_path = Pathway.find_by(name: "De Novo Triacylglycerol Biosynthesis")
      elsif pathway.name.include?("Phosphatidylcholine Biosynthesis")
        ont_path = Pathway.find_by(name: "Phosphatidylcholine Biosynthesis")
      elsif pathway.name.include?("Phosphatidylethanolamine Biosynthesis")
        ont_path = Pathway.find_by(name: "Phosphatidylethanolamine Biosynthesis")
      else
        ont_path = pathway
      end

      puts ont_path.name

      category = OntologyTerm.find_by(term: "Biochemical pathway")
      if !ont_term = OntologyTerm.find_by(term: ont_path.name)
        
        ont_term = OntologyTerm.new
      end
      ont_term.term = ont_path.name
      if ont_path.smpdb_description.present?
        ont_term.definition = ont_path.smpdb_description.split(".")[1..2].join(".")+"."
      elsif ont_path.description.present?
        ont_term.definition = ont_path.description.split(".")[1..2].join(".")+"."
      end
      # ont_term.external_id = external_link
      if ont_path.smpdb_description.present?
        ont_term.external_source = "SMPDB"
      else
        ont_term.external_source = "HMDB"
      end
      # ont_term.comment = comment
      ont_term.curator = "Ana"

      ont_term.parent_id = category.id
      ont_term.level = category.level + 1
      ont_term.save!

      # end

      # if !link = OntologyTermCategory.find_by(ontology_term_id: ont_term.id, ontology_category_id: category.id)
      #   link = OntologyTermCategory.new
      #   link.ontology_term_id = ont_term.id
      #   link.ontology_category_id = category.id
      #   link.save!
      # end
      
      metabolites.each do |met|
        if !Metabolite.find_by(id: met.element_id)
          puts "NOT a metabolite: "+met.element_id.to_s
          next
        end
        if !met_link = MetaboliteOntologyTerm.find_by(ontology_term_id: ont_term.id, metabolite_id: met.element_id)
          met_link = MetaboliteOntologyTerm.new
          met_link.ontology_term_id = ont_term.id
          met_link.metabolite_id = met.element_id
          met_link.save!
        end
      end

    end
  end

  task :delete_terms => [:environment] do
    terms = OntologyTerm.where('id in (select ontology_term_id from ontology_term_categories where ontology_category_id in (select id from ontology_categories where name = "Biochemical pathway")) and (term like "%disease%" or term like "%deficiency%" or term like "%syndrome%" or term like "%uria%" or term like "%emia%")')
    terms.each do |t|
      puts t.term
      # t_cats = OntologyTermCategory.where(ontology_term_id: t.id)
      # t_cats.each do |c|
      t_mets = MetaboliteOntologyTerm.where(ontology_term_id: t.id)
      t_mets.each do |m|
        m.destroy!
      end
      # c.destroy!
      # end
      # t.destroy!
    end

  end

  task :synonyms => [:environment] do
    CSV.foreach("ontology_mappings_for_upload/new_terms_synonyms_locations.csv", :headers => true) do |line|
      term = line["Term"]
      synonyms = line["Synonyms"]

      if !ontology_term = OntologyTerm.find_by(term: term.humanize)
        puts term.humanize
        next
      end
      if synonyms
        all_terms = synonyms.split(";")
        all_terms.each do |syn|
          syn.strip!
          if syn[-1] == "."
            syn = syn[0..-2]
          end
          term_synonym = OntologySynonym.find_or_create_by(ontology_term_id: ontology_term.id, synonym: syn.humanize)
        end

      end
    end

  end
end
