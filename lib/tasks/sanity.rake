require 'csv'
namespace :sanity do
  desc "Sanity check metabolites"
  task :metabolites => [:environment] do
    
    # csv_string = CSV.generate do |csv|
      Metabolite.find_each do |metabolite|
        next if metabolite.valid?
        base_row = [ metabolite.hmdb_id, metabolite.name ]
        metabolite.errors.full_messages.each do |msg|
          puts (base_row + [ msg ]).to_csv
        end
        if !metabolite.link_set.valid?
          metabolite.link_set.errors.full_messages.each do |msg|
            puts (base_row + [ msg ]).to_csv
          end
        end
        if !metabolite.experimental_property_set.valid?
          metabolite.experimental_property_set.errors.full_messages.each do |msg|
            puts (base_row + [ msg ]).to_csv
          end
        end
      end
    # end

    # puts csv_string
  end

  task :fix_articles => [:environment] do
    articles = CiteThis::ArticleReferencing.find_by_sql("select id, article_id, referencer_id, referencer_type, count(*) as n, group_concat(id) AS ids from `cite_this_article_referencings` group by article_id, referencer_id, referencer_type having n >1;")
    articles.each do |ar|
      id = ar.ids.split(/,/).last
      puts "Deleting #{id}"
      CiteThis::ArticleReferencing.destroy(id)
    end
  end

  task :fix_textbooks => [:environment] do
    CiteThis::TextbookReferencing.all.each do |tbr|
      next if CiteThis::Textbook.where(id: tbr.textbook_id).exists?
      puts "Can't find #{tbr.textbook_id}"
      tbr.destroy
    end
  end
end