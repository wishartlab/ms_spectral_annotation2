require 'zip'
require 'builder'
require 'admin_mailer'
require 'wishart/task_support/zipper'

namespace :hmdb do
  namespace :export do
    include Wishart::TaskSupport::Zipper

    DOWNLOAD_PATH = 'public/system/downloads/current'.freeze
    SEQUENCES_PATH = "#{DOWNLOAD_PATH}/sequences".freeze

    desc "Ensure the correct paths exist for export"
    task paths: [:environment] do
      [DOWNLOAD_PATH, SEQUENCES_PATH].each do |path|
        FileUtils.makedirs(path) unless File.exists?(path)
      end
    end

    desc "Run all export tasks"
    task all: [:paths, :xml, :structures, :sequences, :biofluid_structures]

    desc 'Export all compounds and proteins to xml format'
    task xml: ['xml:proteins','xml:metabolites','xml:biofluid_metabolites']

    namespace :xml do
      desc 'Export proteins to xml'
      task proteins: [:environment] do
        proteins_scope = Protein.exported.includes(
          :accession_numbers, :enzyme_classes, :gene_sequence,
          :polypeptide_sequence, :pfams, :go_classes, :pathways,
          :articles, :textbooks, :external_links, :subcellular_locations, :synonyms,
          metabolite_protein_links: [:metabolite, :articles, :textbooks, :external_links]
        )
        export_models_as_xml proteins_scope, "hmdb_proteins", batch_size: 5
      end

      desc 'Export metabolites to xml'
      task metabolites: [:environment] do
        metabolites_scope = Metabolite.exported.includes(
          :accession_numbers, :synonyms, :metabolite_cellular_locations,
          :metabolite_protein_links, :proteins, :link_set,
          :tissues, :ontology, :pathways,
          :articles, :textbooks, :external_links,
          diseases: [:articles, :textbooks, :external_links],
          normal_concentrations: [:articles, :textbooks, :external_links],
          abnormal_concentrations: [:articles, :textbooks, :external_links]
        )
        export_models_as_xml metabolites_scope, "hmdb_metabolites", batch_size: 20
      end

      desc 'Export biofluid metabolites to xml'
      task biofluid_metabolites: [:environment] do
        export_biofluid_metabolites_xml('serum')
        export_biofluid_metabolites_xml('urine')
        export_biofluid_metabolites_xml('csf')
        export_biofluid_metabolites_xml('feces')
        export_biofluid_metabolites_xml('saliva')
        export_biofluid_metabolites_xml('sweat')
      end

      desc 'Export biofluid metabolites to xml for the given biofluid type'
      # Exports metabolites for the given biofluid type.
      # Invoke like this:
      #   bundle exec rake hmdb:export:xml:biofluid_metabolites_set['serum']
      # Allowed biofluid codes:
      #   serum, urine, csf, feces, saliva, sweat
      task :biofluid_metabolites_set , [:biofluid_abbrev] => [:environment] do |t, args|
        export_biofluid_metabolites_xml(args.biofluid_abbrev)
      end

      def export_biofluid_metabolites_xml(biofluid_abbrev)
        # Convert abbreviation to the name used in the database
        biofluid_name = ''
        if biofluid_abbrev == 'serum'
          biofluid_name = 'Blood'
        elsif biofluid_abbrev == 'urine'
          biofluid_name = 'Urine'
        elsif biofluid_abbrev == 'csf'
          biofluid_name = 'Cerebrospinal Fluid (CSF)'
        elsif biofluid_abbrev == 'feces'
          biofluid_name = 'Feces'
        elsif biofluid_abbrev == 'saliva'
          biofluid_name = 'Saliva'
        elsif biofluid_abbrev == 'sweat'
          biofluid_name = 'Sweat'
        else
          puts 'Error: unrecognized biofluid.'
          exit
        end

        metabolites_scope = Metabolite.exported.in_biofluid(biofluid_name).distinct.includes(
          :accession_numbers, :synonyms, :metabolite_cellular_locations,
          :metabolite_protein_links, :proteins, :link_set,
          :tissues, :ontology, :pathways,
          :articles, :textbooks, :external_links,
          diseases: [:articles, :textbooks, :external_links],
          normal_concentrations: [:articles, :textbooks, :external_links],
          abnormal_concentrations: [:articles, :textbooks, :external_links]
        )
        export_models_as_xml metabolites_scope, "#{biofluid_abbrev}_metabolites", batch_size: 20
      end
    end

    desc "Revoked molecules should be unexported"
    task revoke_unexport: [:environment] do
      Revocation.all.each do |r|
        m = Metabolite.find_by(hmdb_id: r.hmdb_id)
        m.export_to_hmdb = false if !m.nil?
      end
    end

    desc "Export structures (SDF)"
    task structures: [:environment] do
      mailer = AdminMailer.build "hmdb",
        subject: "SDF structures export",
        message: "SDF structures export from rake task."

      begin
        outfile = Tempfile.new('structures', encoding: 'UTF-8')

        puts "Building SDF file"
        progress = ProgressBar.new(Metabolite.exported.count) if Rails.env.development?

        # Will give the compounds in order of id asc (which is what we want)
        Metabolite.exported.includes(:synonyms).find_each do |compound|
          progress.increment! if Rails.env.development?
          next unless compound.has_structure?

          # Wrap the helper due to network connection
          # issues
          retries = [3, 5]
          begin
            structure = compound.to_sdf
          # When the host is unreachable, retry with a backoff strategy
          rescue Errno::EHOSTUNREACH => e
            if delay = retries.shift # will be nil if the list is empty
              sleep delay
              retry # backs up to just after the "begin"
            else
              raise # with no args re-raises original error
            end
          # When the file is not found, skip it
          rescue OpenURI::HTTPError => e
            next
          end
          outfile.write(structure) unless structure.blank?
        end

        write_zip_file("#{DOWNLOAD_PATH}/structures.zip",
                       "structures.sdf",
                       outfile)
        mailer.notify_success!
      rescue => e
        raise if Rails.env.development?
        mailer.notify_error! e
      end
    end


    desc 'Export biofluid_structures to sdf'
    task biofluid_structures: [:environment] do
      export_biofluid_structures_sdf('serum')
      export_biofluid_structures_sdf('urine')
      export_biofluid_structures_sdf('csf')
      export_biofluid_structures_sdf('feces')
      export_biofluid_structures_sdf('saliva')
      export_biofluid_structures_sdf('sweat')
    end

    def export_biofluid_structures_sdf(biofluid_abbrev)
      biofluid_name = ''
      if biofluid_abbrev == 'serum'
        biofluid_name = 'Blood'
      elsif biofluid_abbrev == 'urine'
        biofluid_name = 'Urine'
      elsif biofluid_abbrev == 'csf'
        biofluid_name = 'Cerebrospinal Fluid (CSF)'
      elsif biofluid_abbrev == 'feces'
        biofluid_name = 'Feces'
      elsif biofluid_abbrev == 'saliva'
        biofluid_name = 'Saliva'
      elsif biofluid_abbrev == 'sweat'
        biofluid_name = 'Sweat'
      else
        puts 'Error: unrecognized biofluid.'
        exit
      end
      metabolites_scope = Metabolite.exported.in_biofluid(biofluid_name).distinct.includes(:synonyms)
      export_models_as_sdf metabolites_scope, "#{biofluid_abbrev}_metabolites_structures", batch_size: 20
    end


    desc "Export molecule sequences (FASTA)"
    task sequences: [:environment] do
      mailer = AdminMailer.build "hmdb",
        subject: "Sequence export",
        message: "Sequence export from rake task."

      begin
        proteins_fasta = Tempfile.new('proteins', encoding: 'UTF-8')
        genes_fasta = Tempfile.new('genes', encoding: 'UTF-8')

        Protein.includes(:polypeptide_sequence, :gene_sequence).exported.find_each do |molecule|
          if molecule.polypeptide_sequence.present?
            proteins_fasta << molecule.polypeptide_sequence.fasta_sequence(80, "#{molecule.hmdbp_id} #{molecule.name}") << "\n"
          end
          if molecule.gene_sequence.present?
            genes_fasta << molecule.gene_sequence.fasta_sequence(80, "#{molecule.hmdbp_id} #{molecule.name}") << "\n"
          end
        end

        write_zip_file("#{SEQUENCES_PATH}/protein.fasta.zip", "protein.fasta", proteins_fasta)
        write_zip_file("#{SEQUENCES_PATH}/gene.fasta.zip", "gene.fasta", genes_fasta)
      rescue => e
        mailer.notify_error! e
        raise unless Rails.env.production?
      end

      mailer.notify_success!
    end
  end
end

def export_models_as_xml(scope, filename, batch_size: 100)
  puts "Packing #{scope.name}"
  mailer = AdminMailer.build "hmdb",
    subject: "XML export: #{scope.name}",
    message: "XML export of #{scope.name} from rake task."

  outfile = Tempfile.new(filename, encoding: 'UTF-8')

  begin
    puts "\tExporting"
    outfile.write('<?xml version="1.0" encoding="UTF-8"?>')
    outfile.write("\n");
    outfile.write('<hmdb xmlns="http://www.hmdb.ca">')
    outfile.write("\n");
    scope.find_each(batch_size: batch_size) do |item|
      puts item.to_param

      # Wrap the fetch due to network connection issues
      retries = [3, 5]
      begin
        # Strip XML header before outputting
        if m = (/^(<\?xml [^>]+> *\n?)(.*)/m).match(item.to_xml)
          outfile.write(m[2])
        end
      rescue Errno::EHOSTUNREACH => e
        if delay = retries.shift # will be nil if the list is empty
          sleep delay
          retry # backs up to just after the "begin"
        else
          raise # with no args re-raises original error
        end
      end
    end

    outfile.write('</hmdb>');
    outfile.write("\n");

    puts "\tZipping"
    write_zip_file("#{DOWNLOAD_PATH}/#{filename}.zip",
                   "#{filename}.xml",
                   outfile)
    mailer.notify_success!
  rescue => e
    mailer.notify_error! e
    raise unless Rails.env.production?
  end
end

def export_models_as_sdf(scope, filename, batch_size: 100)
  puts "Packing #{scope.name}"
  mailer = AdminMailer.build "hmdb",
    subject: "SDF export: #{scope.name}",
    message: "SDF export of #{scope.name} from rake task."

  outfile = Tempfile.new(filename, encoding: 'UTF-8')
  puts "Building SDF file"
  progress = ProgressBar.new(scope.count) if Rails.env.development?

  begin
    scope.find_each(batch_size: batch_size) do |compound|
      progress.increment! if Rails.env.development?
      next unless compound.has_structure?

      # Wrap the fetch due to network connection issues
      retries = [3, 5]
      begin
        structure = compound.to_sdf
        # When the host is unreachable, retry with a backoff strategy
      rescue Errno::EHOSTUNREACH => e
        if delay = retries.shift # will be nil if the list is empty
          sleep delay
          retry # backs up to just after the "begin"
        else
          raise # with no args re-raises original error
        end
      # When the file is not found, skip it 
      rescue OpenURI::HTTPError => e
        next
      end
      outfile.write(structure) unless structure.blank?
    end

    puts "\tZipping"
    write_zip_file("#{DOWNLOAD_PATH}/#{filename}.zip",
                   "#{filename}.sdf",
                   outfile)
    mailer.notify_success!
  rescue => e
    mailer.notify_error! e
    raise unless Rails.env.production?
  end
end