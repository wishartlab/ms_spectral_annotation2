require 'net/http'
require 'open-uri'

namespace :msds do
  desc "Grab MSDS files from Science Lab"
  task :sciencelab => [:environment] do
    uri = 'http://www.sciencelab.com/msdsList.php'
    response = Net::HTTP.get_response(URI.parse(uri))
    page = response.body
    page.scan(/<a href=\"(.*?)\">(.*?)\s*MSDS\s*<\/a>/) do |url, name|
      m = Metabolite.exported.find_by_name(name)
      next if m.blank?
      puts "Updating MSDS for #{m.hmdb_id}"

      file = if File.exists?("tmp/msds/sciencelab/#{m.hmdb_id}.pdf")
        File.open("tmp/msds/sciencelab/#{m.hmdb_id}.pdf")
      else
        # puts "Found MSDS for #{name}!"
        url = "http://www.sciencelab.com/#{url}"
        File.open("tmp/msds/sciencelab/#{m.hmdb_id}.pdf", "wb") do |saved_file|
          open(url, 'rb') do |read_file|
            saved_file.write(read_file.read)
          end
        end
        File.open("tmp/msds/sciencelab/#{m.hmdb_id}.pdf")
      end

      m.msds = file
      m.save(:validate => false)
    end
  end
end



