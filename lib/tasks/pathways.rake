namespace :pathways do

  desc "Update pathway links to smpdb with a given list of pathways"
  task :update_from_list => [:environment] do
    new_pathways = 0
    current_m_links = PathwayLink.where(element_type: "Metabolite").count
    current_p_links = PathwayLink.where(element_type: "Protein").count
    smpdb_ids = File.read("data/pathways_to_import.txt").split("\n")
    smpdb_ids.each do |smpdb_id|

      pathway = get_json("http://smpdb.ca/pathways/get_smpdb_pathway?smpdb_id=#{smpdb_id}")
      if !pathway.any?
        puts "NO RESULT: #{smpdb_id}"
        exit
      end

      next if !pathway["species"]
      next if !pathway["species"].downcase.include?("homo sapien")
      next if !pathway["compounds"]

      hmdb_pathway = Pathway.where(smpdb_id: pathway["smpdb_id"]).take
      if hmdb_pathway.blank?
        hmdb_pathway = Pathway.where(pathwhiz_id: pathway["pw_id"]).take
      end

      # Check if pathway exists by name
      if hmdb_pathway.blank?
        hmdb_pathway = Pathway.where('lower(name) = ?', pathway["name"].downcase).where(smpdb_id: "").take
      end

      # Create pathway if needed
      if hmdb_pathway.blank?
        hmdb_pathway = Pathway.create!(name: pathway["name"])
        new_pathways = new_pathways + 1
      end

      hmdb_pathway.update_attribute(:smpdb_id, pathway["smpdb_id"])
      hmdb_pathway.update_attribute(:pathwhiz_id, pathway["pw_id"])
      hmdb_pathway.update_attribute(:name, pathway["name"])
      hmdb_pathway.update_attribute(:smpdb_description, pathway["description"])
      puts hmdb_pathway.smpdb_id
      puts hmdb_pathway.pathwhiz_id

      pathway["compounds"].each do |inchikey|

        next if !metabolite = Metabolite.find_by(moldb_inchikey: inchikey)
        # puts metabolite.hmdb_id
        metabolite_link = PathwayLink.where(pathway_id: hmdb_pathway.id,
                          element_type: "Metabolite",
                          element_id: metabolite.id).first_or_create

        if !metabolite_link
          puts "NO LINK: #{metabolite.hmdb_id}, #{smpdb_id}"
          exit
        end

        metabolite.touch
      end

      pathway["proteins"].each do |uniprot_id|

        next if !protein = Protein.find_by(uniprot_id: uniprot_id)
        # puts protein.uniprot_id
        protein_link = PathwayLink.where(pathway_id: hmdb_pathway.id,
                          element_type: "Protein",
                          element_id: protein.id).first_or_create

        if !protein_link
          puts "NO LINK: #{protein_link.uniprot_id}, #{smpdb_id}"
          exit
        end

        protein.touch
      end

    end
    puts "NEW PATHWAYS: " + new_pathways.to_s
    puts "NEW METABOLITE PATHWAY LINKS: " + (PathwayLink.where(element_type: "Metabolite").count - current_m_links).to_s
    puts "NEW PROTEIN PATHWAY LINKS: " + (PathwayLink.where(element_type: "Protein").count - current_p_links).to_s
  end

  # Query SMPDB to find the associated pathways for all exported 
  # metabolites and proteins, and add the necessary pathways and
  # pathway links to HMDB. NOTE: This takes about half a day, due
  # to the number of metabolites/proteins.
  desc "Update pathway links to smpdb"
  task :update => [:environment] do
    # Keep track of how many objects we add
    new_pathways = 0
    current_m_links = PathwayLink.where(element_type: "Metabolite").count
    current_p_links = PathwayLink.where(element_type: "Protein").count

    # For all exported metabolites with inchikeys
    # Metabolite.exported.where("moldb_inchikey <> ''").each do |metabolite|
    Metabolite.where(moldb_inchikey: "GNGACRATGGDKBX-UHFFFAOYSA-N").each do |metabolite|
    # Metabolite.where(moldb_inchikey: "AWUCVROLDVIAJX-GSVOUGTGSA-N").each do |metabolite|
      puts metabolite.hmdb_id
      # Get pathways for this metabolite
      # pathways = get_json("http://smpdb.ca/compounds/get_smpdb_pathways?inchikey=#{metabolite.moldb_inchikey}")
      pathway_smpdb_ids = get_json("http://smpdb.ca/compounds/get_smpdb_pathway_ids?inchikey=#{metabolite.moldb_inchikey}")

      puts "TOTAL Pathways: #{pathway_smpdb_ids.count}"

      pathway_smpdb_ids.each do |path_id|
        smpdb_id = path_id["smpdb_id"]
        next if smpdb_id.split("SMP")[1].to_f<38691

        # puts smpdb_id
        pathway = get_json("http://smpdb.ca/pathways/get_smpdb_pathway?smpdb_id=#{smpdb_id}")
        if !pathway.any?
          puts "NO RESULT: #{smpdb_id}"
          exit
        end

        next if !pathway["species"]
        next if !pathway["species"].downcase.include?("homo sapien")

        hmdb_pathway = Pathway.where(smpdb_id: pathway["smpdb_id"]).take
        if hmdb_pathway.blank?
          hmdb_pathway = Pathway.where(pathwhiz_id: pathway["pw_id"]).take
        end

        # Check if pathway exists by name
        if hmdb_pathway.blank?
          hmdb_pathway = Pathway.where('lower(name) = ?', pathway["name"].downcase).where(smpdb_id: "").take
        end

        # Create pathway if needed
        if hmdb_pathway.blank?
          hmdb_pathway = Pathway.create!(name: pathway["name"])
          new_pathways = new_pathways + 1
        end

        hmdb_pathway.update_attribute(:smpdb_id, pathway["smpdb_id"])
        hmdb_pathway.update_attribute(:pathwhiz_id, pathway["pw_id"])
        hmdb_pathway.update_attribute(:name, pathway["name"])
        hmdb_pathway.update_attribute(:smpdb_description, pathway["description"])
        puts hmdb_pathway.smpdb_id
        puts hmdb_pathway.pathwhiz_id

        # Check if pathway link exists, create if needed
        metabolite_link = PathwayLink.where(pathway_id: hmdb_pathway.id,
                          element_type: "Metabolite",
                          element_id: metabolite.id).first_or_create
        if !metabolite_link
          puts "NO LINK: #{metabolite.hmdb_id}, #{smpdb_id}"
          exit
        end
      end
      
      metabolite.touch
    end

    # For all exported metabolites with inchikeys
    # Protein.exported.where("uniprot_id <> ''").each do |protein|
    #   puts protein.hmdbp_id
    #   # Get pathways for this protein
    #   pathways = get_json("http://smpdb.ca/proteins/get_smpdb_pathways?uniprot_id=#{protein.uniprot_id}")

    #   pathways.each do |pathway|
    #     next if !pathway["species"].downcase.include?("homo sapien")
    #     # Check if pathway exists by smpdb
    #     hmdb_pathway = Pathway.where(smpdb_id: pathway["smpdb_id"]).take

    #     # Check if pathway exists by name
    #     if hmdb_pathway.blank?
    #       hmdb_pathway = Pathway.where('lower(name) = ?', pathway["name"].downcase).where(smpdb_id: "").take
    #       if hmdb_pathway.present?
    #         hmdb_pathway.update_attribute(:smpdb, pathway["smpdb_id"])
    #         hmdb_pathway.update_attribute(:name, pathway["name"])
    #         puts "UPDATE PATHWAY " + hmdb_pathway.name
    #       end
    #     end

    #     # Create pathway if needed
    #     if hmdb_pathway.blank?
    #       hmdb_pathway = Pathway.create!(smpdb_id: pathway["smpdb_id"],
    #                                     name: pathway["name"])
    #       puts "ADD PATHWAY " + hmdb_pathway.smpdb_id
    #       new_pathways = new_pathways + 1
    #     end

    #     hmdb_pathway.update_attribute(:pathwhiz_id, pathway["pw_id"])
    #     hmdb_pathway.update_attribute(:smpdb_description, pathway["description"])

    #     # Check if pathway link exists, create if needed
    #     PathwayLink.where(pathway_id: hmdb_pathway.id,
    #                       element_type: "Protein",
    #                       element_id: protein.id).first_or_create
    #   end

    #   protein.touch
    # end

    puts "NEW PATHWAYS: " + new_pathways.to_s
    puts "NEW METABOLITE PATHWAY LINKS: " + (PathwayLink.where(element_type: "Metabolite").count - current_m_links).to_s
    puts "NEW PROTEIN PATHWAY LINKS: " + (PathwayLink.where(element_type: "Protein").count - current_p_links).to_s

  end

  # Query SMPDB to find the associated pathways for a single 
  # metabolite, and add the necessary pathways and
  # pathway links to HMDB.
  desc "Update pathways links to smpdb"
  task :update_one_metabolite, [:id] => [:environment] do |t, args|
    # Keep track of how many objects we add
    new_pathways = 0
    current_m_links = PathwayLink.where(element_type: "Metabolite").count

    metabolite = Metabolite.find_by_hmdb_id(args.id)
    puts metabolite.hmdb_id
    # Get pathways for this metabolite
    pathways = get_json("http://smpdb.ca/compounds/get_smpdb_pathways?inchikey=#{metabolite.moldb_inchikey}")

    pathways.each do |pathway|
      # Check if pathway exists by smpdb_id
      hmdb_pathway = Pathway.where(smpdb_id: pathway["smpdb_id"]).take

      # Check if pathway exists by name
      if hmdb_pathway.blank?
        hmdb_pathway = Pathway.where('lower(name) = ?', pathway["name"].downcase).where(smpdb_id: "").take
        if hmdb_pathway.present?
          hmdb_pathway.update_attribute(:smpdb, pathway["smpdb_id"])
          hmdb_pathway.update_attribute(:name, pathway["name"])
          puts "UPDATE PATHWAY " + hmdb_pathway.name
        end
      end

      # Create pathway if needed
      if hmdb_pathway.blank?
        hmdb_pathway = Pathway.create!(smpdb_id: pathway["smpdb_id"],
                                      name: pathway["name"])
        puts "ADD PATHWAY " + hmdb_pathway.smpdb_id
        new_pathways = new_pathways + 1
      end

      # Check if pathway link exists, create if needed
      PathwayLink.where(pathway_id: hmdb_pathway.id,
                        element_type: "Metabolite",
                        element_id: metabolite.id).first_or_create
    end

    metabolite.touch

    puts "NEW PATHWAYS: " + new_pathways.to_s
    puts "NEW METABOLITE PATHWAY LINKS: " + (PathwayLink.where(element_type: "Metabolite").count - current_m_links).to_s

  end

  # Grab json data from a url
  def get_json(url)
    begin
      return JSON.load(open(url))
    rescue
      return {}
    end
  end

end

