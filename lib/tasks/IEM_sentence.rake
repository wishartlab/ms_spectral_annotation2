require "synonym_cleaner"

namespace :IEM_sentence do
	desc "if a metabolite is associated with an IEM(s), add a sentence saying so in the description!"
	task :add => [:environment] do
		hash_count = 0
    met_with_iem = Hash.new { |h, k| h[k] = [] }
    met_with_name = Hash.new
		CSV.open('public/IEM_and_biomarkers_for_Hasan.tsv', 'r', :col_sep => "\t", :quote_char => "\x00").each do | row |
			if row.empty? || row.nil? || row=="" || row.blank?
				next
			else
				hash_count+= 1
				id = row[0].strip
      	met_name = row[1].strip
        disease = row[2].strip
        met_with_iem[id].push(disease)
        met_with_name[id] = met_name
        puts "iteration number: #{hash_count.to_s}"
      end
    end

    puts "hash size: #{met_with_iem.size.to_s}"
    puts "Should be the same as this: #{met_with_name.size.to_s}"
    puts "--------------------------------"
    sleep(2)
    puts "...now sentences will start generating..."
    sleep(2)

    outfile = File.open('public/metabolites_desc_associated_with_IEM.tsv', "w")
    start = "is found to be associated with"
    one = "which is an inborn error of metabolism"
    many = "which are inborn errors of metabolism"
    iteration_count = 0
    met_with_iem.each do |key, value|
      iteration_count+= 1
      is_mentioned_once = false
      is_mentioned_twice = false
      m = Metabolite.find_by_hmdb_id(key)
      if !m.nil? || !m.empty?
        words = ["inborn error of metabolism", "disease", "inborn errors", "deficiency",
         "disorder", "genetic error", "inherited"]
        if words.any? { |word| m.description.downcase.include?(word) }
          is_mentioned_once = true
        end
        downcased = value.map(&:downcase)
        m.description.downcase.split.each do |word|
          if downcased.include? word.strip
            is_mentioned_twice = true
          end
        end
        m_name = SynonymCleaner.capitalize(met_with_name[key])
        if met_with_iem[key].size == 1
          new_sentence = "#{m_name} #{start} #{met_with_iem[key].to_sentence}, #{one}."
        else
          new_sentence = "#{m_name} #{start} #{met_with_iem[key].to_sentence}, #{many}."
        end
        new_desc = m.description + " " + new_sentence
        if is_mentioned_once || is_mentioned_twice
          puts "#{iteration_count}- #{key}; yes"
          outfile.write("#{key}\t#{new_desc}\n")	
      	else
          puts "#{iteration_count}- #{key}; no"
          outfile.write("#{key}\t#{new_desc}\n")
          m.description = new_desc
          m.save!
      	end
      else
        puts "#{key} was not found!"
      end
    end
    outfile.close
  end
end