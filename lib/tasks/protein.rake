namespace :protein do
  desc "Import Protein - Metabolite associations"
  task :associations,[:filename] => [:environment] do |t,args|
    ActiveRecord::Base.transaction do
      CSV.foreach(args.filename) do |row| 
        hmdbs = row[1].split(";")
        uniprot_id = row[0]
        puts uniprot_id
        protein = if Protein.find_by_uniprot_id(uniprot_id).nil?
          p = create_by_uniprot_id(uniprot_id)
          p.save!
          p
        else
          Protein.find_by_uniprot_id(uniprot_id)
        end

        hmdbs.each do |hmdb_id|
          metabolite = Metabolite.find_by_hmdb_id(hmdb_id)
          protein.metabolites << metabolite if protein.metabolites.where(:id => metabolite.id).empty?
        end
        protein.save!
      end
    end
  end

  desc "Remove Non-Human Proteins"
  task :remove_non_human => [:environment] do
    # DataWrangler.configure do |config|
    #   config.dalli_client = Dalli::Client.new('127.0.0.1:11211')
    #   config.cache_dir = '/Users/tjewison/scratch/test_data-wrangler/cache'
    #   config.chemspider_token = "b3302c5e-7908-4e8b-8708-f1ba0102b303"
    # end

    Protein.exported.each do |p|
      # puts "Processing #{p.uniprot_id}"
      # uniprot = DataWrangler::Model::UniprotProtein.new(p.uniprot_id)
      if p.organism.to_s.strip != "Homo_sapiens"
        puts "#{p.hmdbp_id} Revoked Non-Human protein"
        p.revoke!("Non-Human protein")
        # p.export = false
        # p.save!
      end
    end
  end

  desc "Remove Non-Human Proteins"
  task :remove_with_no_metabolites => [:environment] do
    Protein.exported.each do |p|
      if p.metabolites.count == 0
        puts "#{p.hmdbp_id} Revoked no associated metabolites"
        p.revoke!("Revoked as protein has no associated metabolites.")
      end
    end
  end  

  desc "Check for outdated Uniprot ids"
  task :outdated => [:environment] do
    DataWrangler.configure do |config|
      config.dalli_client = Dalli::Client.new('127.0.0.1:11211')
      config.cache_dir = '/Users/tjewison/scratch/test_data-wrangler/cache'
      config.chemspider_token = "b3302c5e-7908-4e8b-8708-f1ba0102b303"
    end
    Protein.exported.each do |p|
      # puts "checking #{p.uniprot_id}"
      u = DataWrangler::Uniprot.get_by_id(p.uniprot_id)
      if u.uniprot_id.nil?
        p.revoke!("Revoked due to outdated associated UniProt entry (#{p.uniprot_id})")
        puts "#{p.hmdbp_id} Revoked OUTDATED UNIPROT #{p.uniprot_id}"
      elsif p.uniprot_id != u.uniprot_id
        puts "#{p.hmdbp_id} #{p.uniprot_id} update to #{u.uniprot_id}"
        p.uniprot_id = u.uniprot_id
        p.save!
      end
    end
  end

  desc "Merge Phase 1"
  task :merge_1 => [:environment] do
    Protein.exported.select("DISTINCT uniprot_id").each do |p|

      proteins = Protein.exported.where(:uniprot_id => p.uniprot_id).order(:id)
      next if proteins.count == 1
      keep = proteins.shift
      proteins.each do |delete|
        # puts "MERGING #{keep.hmdbp_id} with #{delete.hmdbp_id}"
        merge_protein(keep,delete)
      end
    end
  end
end

def merge_protein(keep, delete)
  Protein.transaction do
    keep.name                  = delete.name if keep.name.blank?
    keep.general_function      = delete.general_function if keep.general_function.blank?
    keep.gene_name             = delete.gene_name if keep.gene_name.blank?
    keep.specific_function     = delete.specific_function if keep.specific_function.blank?
    keep.reactions             = delete.reactions if keep.reactions.blank?
    keep.signal_regions        = delete.signal_regions if keep.signal_regions.blank?
    keep.transmembrane_regions = delete.transmembrane_regions if keep.transmembrane_regions.blank?
    keep.pdb_id                = delete.pdb_id if keep.pdb_id.blank?
    keep.genbank_gene_id       = delete.genbank_gene_id if keep.genbank_gene_id.blank?
    keep.genbank_protein_id    = delete.genbank_protein_id if keep.genbank_protein_id.blank?
    keep.genecard_id           = delete.genecard_id if keep.genecard_id.blank?
    keep.chromosome_location   = delete.chromosome_location if keep.chromosome_location.blank?
    keep.locus                 = delete.locus if keep.locus.blank?
    keep.geneatlas_id          = delete.geneatlas_id if keep.geneatlas_id.blank?
    keep.hgnc_id               = delete.hgnc_id if keep.hgnc_id.blank?

    keep.polypeptide_sequence = delete.polypeptide_sequence if keep.polypeptide_sequence.blank?
    keep.gene_sequence = delete.gene_sequence if keep.gene_sequence.blank?

    delete.pathways.each do |p|
      keep.pathways << p if keep.pathways.where(:id => p.id).empty?
    end

    delete.synonyms.each do |syn|
      keep.synonyms << ProteinSynonym.new(:synonym => syn.synonym) if keep.synonyms.where(:synonym => syn.synonym).empty?
    end

    delete.enzyme_classes.each do |ec|
      keep.enzyme_classes << EnzymeClass.new(:ec => ec.ec) if keep.enzyme_classes.where(:ec => ec.ec).empty?
    end

    delete.articles.each do |ref|
      keep.articles << ref if keep.articles.where(:id => ref.id).empty?
    end

    delete.textbooks.each do |ref|
      keep.textbooks << ref if keep.textbooks.where(:id => ref.id).empty?
    end
    delete.pfams.each do |pfam|
      keep.pfams << pfam if keep.pfams.where(:id => pfam.id).empty?
    end

    delete.subcellular_locations.each do |sl|
      keep.subcellular_locations << sl if keep.subcellular_locations.where(:id => sl.id).empty?
    end

    delete.go_classes.each do |go|
      keep.go_classes << go if keep.go_classes.where(:id => go.id).empty?
    end

    delete.metabolite_protein_links.each do |mpl|
      keep_mpl = keep.metabolite_protein_links.where(:metabolite_id => mpl.metabolite_id).first
      if keep_mpl.nil?
        keep_mpl = MetaboliteProteinLink.new(:metabolite_id => mpl.metabolite_id, :protein_id => keep.id)
        keep_mpl.save!
      end

      mpl.articles.each do |ref|
        keep_mpl.articles << ref if keep_mpl.articles.where(:id => ref.id).empty?
      end

      mpl.textbooks.each do |ref|
        keep_mpl.textbooks << ref if keep_mpl.textbooks.where(:id => ref.id).empty?
      end
    end

    keep.accession_numbers << AccessionNumber.new(:number => delete.hmdbp_id)
    keep.save!

    if !delete.destroy
      raise "Could not delete merged entry #{delete.hmdbp_id}"
    end

    puts "#{delete.hmdbp_id} merged into #{keep.hmdbp_id}"
  end
end