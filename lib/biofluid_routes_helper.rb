class ActionDispatch::Routing::Mapper
  def draw_biofluid_routes(*sub_applications)
    constraints = Regexp.new sub_applications.map(&:to_s).join("|")
    Rails.application.routes.draw do
      # We need this as an alternative route for grabbing the concentration
      # info for the modal details view (clicking on 'details' link).
      # AJAX requests can't go cross-domain, so we need to duplicate the
      # route here:
      get 'biofluids/:biofluid/concentrations/:id' => "concentrations#show",
        as: "biofluid_concentration"

      scope "/biofluids/:biofluid", module: "biofluids", constraints: {biofluid: constraints} do
        # Simple html page actions
        get "" => "simple#home", as: "biofluid_home"

        # Resource routes
        resources :metabolites, only: :index, as: :biofluid_metabolites

        get "/concentrations" => "biofluid#concentrations",
          as: "biofluid_concentrations"
        get "/diseases" => "biofluid#diseases",
          as: "biofluid_diseases"
        get "/statistics" => "biofluid#stats",
          as: "biofluid_stats"
        get "/downloads" => "biofluid#downloads",
          as: "biofluid_downloads"
        
        # get "/biofluids/:biofluid/public/system/*other" => "biofluid#download_link"
        
      end
      
      #get "/biofluids/:biofluid/public/system/*other" => "biofluid#download_link"
      
    end
  end
end
