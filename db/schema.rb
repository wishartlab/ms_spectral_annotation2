# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180302193110) do

  create_table "metabolites", force: :cascade do |t|
    t.string   "database_id", limit: 255,                null: false
    t.string   "name",        limit: 255,                null: false
    t.integer  "source_id",   limit: 4,                  null: false
    t.boolean  "exported",                default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "metabolites", ["database_id", "source_id"], name: "index_metabolites_on_database_id_and_source_id", unique: true, using: :btree
  add_index "metabolites", ["database_id"], name: "index_metabolites_on_database_id", using: :btree
  add_index "metabolites", ["source_id"], name: "index_metabolites_on_source_id", using: :btree

  create_table "sources", force: :cascade do |t|
    t.string "organism", limit: 255, null: false
    t.string "database", limit: 255
  end

  add_index "sources", ["organism"], name: "index_sources_on_organism", using: :btree

  create_table "wishart_notices", force: :cascade do |t|
    t.text     "content",    limit: 65535,                 null: false
    t.boolean  "display",                  default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_foreign_key "metabolites", "sources"
end
