class ChangeSources < ActiveRecord::Migration
  def change
    add_column :sources, :database, :string
    rename_column :sources, :name, :organism
    add_index :sources, :organism
  end
end
