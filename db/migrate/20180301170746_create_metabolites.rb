class CreateMetabolites < ActiveRecord::Migration
  def change
    create_table :metabolites do |t|
      t.string :database_id, null: false, index: true
      t.string :name, null: false
      t.references :source, index: true, null: false
      t.boolean :exported, default: true
      t.timestamps
    end
    add_foreign_key :metabolites, :sources, on_delete: :restrict
    add_index :metabolites, [:database_id, :source_id], unique: true
  end
end
