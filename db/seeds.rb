# Sources
sources_list = [
  ['Arabidopsis thaliana', 'SMPDB'],
  ['Caenorhabditis elegans', 'SMPDB'],
  ['Danio rerio', 'SMPDB'],
  ['Drosophila melanogaster', 'SMPDB'],
  ['Escherichia coli', 'M2MDB'],
  ['Homo sapiens', 'HMDB'],
  ['Mus musculus', 'HMDB'],
  ['Rattus norvegicus', 'HMDB'],
  ['Pseudomonas aeruginosa', 'SMPDB'],
  ['Saccharomyces cerevisiae', 'YMDB'],
  ['Homo sapiens, Amniotic Fluid', 'HMDB'],
  ['Homo sapiens, Bile', 'HMDB'],
  ['Homo sapiens, Blood', 'HMDB'],
  ['Homo sapiens, Breast Milk', 'HMDB'],
  ['Homo sapiens, Cellular Cytoplasm', 'HMDB'],
  ['Homo sapiens, Cerebrospinal Fluid (CSF)', 'HMDB'],
  ['Homo sapiens, Feces', 'HMDB'],
  ['Homo sapiens, Saliva', 'HMDB'],
  ['Homo sapiens, Sweat', 'HMDB'],
  ['Homo sapiens, Urine', 'HMDB']
]

sources_list.each do |organism, database|
  Source.create(organism: organism, database: database)
end

# Metabolites (for testing mass 175.01)
metabolites_list = [
  ['HMDB0000292', 'Xanthine', 6],
  ['M2MDB000121', 'Xanthine', 5],
  ['PW_C000198', 'Xanthine', 1],
  ['YMDB00263', 'Xanthine', 10]
]

metabolites_list.each do |database_id, name, source_id|
  Metabolite.create(database_id: database_id, name: name, source_id: source_id)
end
