require 'test_helper'

class SimpleControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
  end

  test "should get about" do
    get :about
    assert_response :success
  end

  test "should get release_notes" do
    get :release_notes
    assert_response :success
  end

  test "should get citing" do
    get :citing
    assert_response :success
  end

  test "should get statistics" do
    get :statistics
    assert_response :success
  end

  test "should get sources" do
    get :sources
    assert_response :success
  end

  test "should get help" do
    get :help
    assert_response :success
  end

  test "should get databases" do
    get :databases
    assert_response :success
  end

  test "should get contact" do
    get :contact
    assert_response :success
  end

end
