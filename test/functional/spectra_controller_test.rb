require 'test_helper'

class SpectraControllerTest < ActionController::TestCase
  test "should get show" do
    get :show
    assert_response :success
  end

  test "should get fids" do
    get :fids
    assert_response :success
  end

  test "should get conditions" do
    get :conditions
    assert_response :success
  end

end
